cd ./Instruments/DtUtilities
ms CREATE -f ./Desktop.DtUtilities.EmailClient.Chars
ms CREATE -f ./Desktop.DtUtilities.WinEditor.Chars
ms CREATE -f ./Desktop.DtUtilities.ImageEditor.Chars
ms CREATE -f ./Desktop.DtUtilities.ImageViewer.Chars
ms CREATE -f ./Desktop.DtUtilities.MediaViewer.Chars
ms CREATE -f ./Desktop.DtUtilities.VectorEditor.Chars
ms CREATE -f ./Desktop.DtUtilities.WebBrowser.Chars
ms CREATE -f ./Desktop.DtUtilities.FileBrowser.Chars

cd ../Window
ms CREATE -f ./AutoWindowPlacement.Logical
ms CREATE -f ./MoveOpaqueWindow.Logical
ms CREATE -f ./ToolchestHorizontal.Logical
ms CREATE -f ./KeyboardFocus.Logical
ms CREATE -f ./OutlineThickness.Gauge
ms CREATE -f ./WindowManagerAccent.Color

cd ../Settings
ms CREATE -f ./ShowLaunchEffect.Logical
ms CREATE -f ./DesktopAccent.Color

cd ../UserInterface
ms CREATE -f ./ModernLookAndFeel.Logical

cd ../Colors
ms CREATE -f ./UserInterfaceAccent.Color
ms CREATE -f ./SgiScheme.Chars

cd ../DtSounds
ms CREATE -f ./DesktopSounds.Logical

cd ../Text
ms CREATE -f ./SmoothText.Logical

cd ../Background
ms CREATE -f ./DarkBackground.Logical

cd ..

ms CREATE -A name=Desktop.WinEditor.Nano, stereotype=Chars,default=nano
ms CREATE -A name=Desktop.WinEditor.XNEdit, stereotype=Chars,default=xnedit
ms CREATE -A name=Desktop.WinEditor.GEdit, stereotype=Chars,default=gedit
ms CREATE -A name=Desktop.WinEditor.Sublime, stereotype=Chars,default=subl


echo "Done"


