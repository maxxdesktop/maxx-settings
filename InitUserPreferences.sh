# DtUtilities
msettings RESET Desktop.DtUtilities.EmailClient
msettings RESET Desktop.DtUtilities.WinEditor
msettings RESET Desktop.DtUtilities.ImageEditor
msettings RESET Desktop.DtUtilities.ImageViewer
msettings RESET Desktop.DtUtilities.MediaViewer
msettings RESET Desktop.DtUtilities.VectorEditor
msettings RESET Desktop.DtUtilities.WebBrowser
msettings RESET Desktop.DtUtilities.FileBrowser

# Desktop.Window
msettings RESET Desktop.Window.AutoWindowPlacement
msettings RESET Desktop.Window.MoveOpaqueWindow
msettings RESET Desktop.Window.ToolchestHorizontal
msettings RESET Desktop.Window.KeyboardFocus
msettings RESET Desktop.Window.OutlineThickness
msettings RESET Desktop.Window.WindowManagerAccent

# Desktop.Settings
msettings RESET Desktop.Settings.DesktopAccent
msettings RESET Desktop.Settings.ShowLaunchEffect

# Desktop.UserInterface
msettings RESET Desktop.UserInterface.ModernLookAndFeel

# Desktop.Colors
msettings RESET Desktop.Colors.UserInterfaceAccent
msettings RESET Desktop.Colors.SgiScheme

# Desktop.DtSounds
msettings RESET Desktop.DtSounds.DesktopSounds

# Text
msettings RESET Desktop.Text.SmoothText

# Background
msettings RESET Desktop.Background.DarkBackground

msettings RESET Desktop.WinEditor.Nano
msettings RESET Desktop.WinEditor.XNEdit
msettings RESET Desktop.WinEditor.GEdit
msettings RESET Desktop.WinEditor.Sublime

echo "done"
