# Welcome to MaXX-Settings

MaXX Settings is a dynamic configuration management subsystem designed from the ground up with simplicity in mind while not sacrificing flexibility and extensibility. MaXX Settings comes with its own CLI interface allowing simple management, automation via scripting, inline-query and easy application integration. MaXX Settings also provides Java and C++ binding making it super easy to integrate within most modern applications. MaXX Settings allow the definition of System wide setting, we call them *Instruments*, and user’s overridables called *User Preferences*.

 
To learn more about MaXX Settings, visit our [main-site](https://docs.maxxinteractive.com/books/maxx-desktop-frameworks/page/maxx-settings)
## Technical Specification
Refer to [MaXX Settings Architecture & Technical Specifications](https://docs.google.com/document/d/1RrAgtpF-lkds02tPuA8SbvSEbO9Xv2GgSlbJcCwlji8)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

## Prerequisites

**Ensure JDK 8 u251 minimum is present**
```bash
$ java -version
```
Which returns:
```
java version "1.8.0_251"
Java(TM) SE Runtime Environment (build 1.8.0_251-b03)
Java HotSpot(TM) 64-Bit Server VM (build 25.251-b03, mixed mode)
```


**Ensure Apache Maven 3.6.3 is installed.**
```bash
$ mvn --version
```
Which returns:
```bash
Apache Maven 3.6.3 (138edd61fd100ec658bfa2d307c43b76940a5d7d; 2017-10-18T03:58:13-04:00)
Maven home: /opt/java/apache-maven
Java version: 1.8.0_251, vendor: Oracle Corporation
Java home: /opt/java/jdk1.8.0_251
Default locale: en_US, platform encoding: UTF-8
OS name: "linux", version: "4.18.13-100.fc27.x86_64", arch: "amd64", family: "unix"
```
**Install GRAALVM and Native-Image in order to build a native image of MaXX Settings CLI**

- Refer to [GRAAL VM](https://www.graalvm.org/docs/getting-started/)
- Refer to [GRAAL VM - Native Image](https://www.graalvm.org/reference-manual/native-image/)
---
## Checkout the code
```bash
$ git clone https://gitlab.com/maxxdesktop/maxx-settings.git
```

## Build the project

```bash
$ cd maxx-settings
$ mvn clean install -DskipTests=true // or false
```

## Generating JavaDoc
```bash
$ mvn javadoc:javadoc
```

## Browse MaXX Settings JavaDoc
```bash
$ $WEBBROWSER ./target/site/apidocs/index.html 
```

## Build native image
*Successful build outputs a native-image (binary)named **desktop-msettings** in bin/*
```bash
$ ./mk-image.sh

// test the native-image   

$ ldd ./bin/desktop-msettings
        linux-vdso.so.1 (0x00007ffec4667000)
        libpthread.so.0 => /lib64/libpthread.so.0 (0x00007f076234b000)
        libdl.so.2 => /lib64/libdl.so.2 (0x00007f0762147000)
        libz.so.1 => /lib64/libz.so.1 (0x00007f0761f30000)
        librt.so.1 => /lib64/librt.so.1 (0x00007f0761d28000)
        libc.so.6 => /lib64/libc.so.6 (0x00007f0761965000)
        /lib64/ld-linux-x86-64.so.2 (0x00007f076256b000)

$ ./bin/msettings -v
MaXX Settings CLI version 1.0
```

## Build everything, including native image
```bash
$ ./build.sh
```
---
## To run Tests

### Run the SchemaTest app
```bash
$ java -cp target/desktop-msettings.jar com.maxxinteractive.msettings.tests.SchemaTest
```

###To run the InstrumentTest app  
```bash
$ java -cp target/desktop-msettings.jar com.maxxinteractive.msettings.tests.InstrumentTest
```

### Run the Test app
```bash
$ java -cp target/desktop-msettings.jar com.maxxinteractive.msettings.tests.Test
```
---
## To run MaXX Settings (in JVM mode)

### Run MaXX Settings in Standard-mode  - print version
```bash
$ java -jar target/desktop-msettings.jar msettings -v

$ ./run.sh msettings -v      // using the convinient wrapper
```

### Run MaXX Settings in Standard-mode - print help
```bash
$ java -jar target/desktop-msettings.jar msettings -h
```

### Run MaXX Settings in Admin-mode - print Hashed directory
```bash
$ su -
$ java -jar target/desktop-msettings.jar ms H Desktop.Mouse.Acceleration

- Desktop.Mouse.Acceleration = /3b/2a/d4/d6
```

## To run MaXX Settings local (native mode)
*Assuming you have already built the native image*

### Run MaXX Settings in Standard-mode  - print version
```bash
$ ./bin/msettings -v
```

### Run MaXX Settings in Standard-mode - print help
```bash
$ .bin/msettings -h
```

### Run MaXX Settings in Admin-mode - print Hashed directory
*Must be root or superuser to run in Admin-mode*
```bash
$ su -
$ ./bin/ms H Desktop.Mouse.Acceleration

- Desktop.Mouse.Acceleration = /3b/2a/d4/d6
```
---
## Install MaXX Settings CLI
*Installation location: /opt/MaXX*
```bash
$ su - 
$ ./install.sh
$ exit
```
or
```bash
$ sudo ./install.sh
```

## Test your new MaXX Settings CLI Installation
*Assuming you are running in a MaXX Desktop Session, just checking :)*
```bash
$ msettings -h
```
---
**Congratulations, you are all set and ready to go.**