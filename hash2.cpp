#include <iostream>
#include <string>
#include <ctime>
#include <chrono>

//  g++ -O3  -std=c++11 hash2.cpp -o hash2 -L/usr/lib64 -lstdc++ -lm

int hashCode(const std::string &);

int main()
{
  std::string str = "Desktop.Settings.DisplayApplicationErrors";
  
  auto start = std::chrono::steady_clock::now();
  std::size_t hash = hashCode(str);

  auto end = std::chrono::steady_clock::now();
  auto elapsed = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);
  std::cout << "It took me " << elapsed.count() << " nanoseconds." << std::endl;

  int mask = 255;
  int firstDir = hash & mask;
  int secondDir = (hash >> 8) & mask;
  int thirdDir = (hash >> 8 >> 8) & mask;
  int forthDir = (hash >> 8 >> 8 >> 8) & mask; 
  
  char filepath[50];std::sprintf( filepath, "%02x/%02x/%02x/%02x", firstDir, secondDir, thirdDir, forthDir );
  std::cout << str << " hash=" << hash <<  " path=" <<  filepath << std::endl;
}


/** 
 Fast/Dead Simple String Hashing fonction compatible with Java hashCode()
*/
int hashCode(const std::string &value) {
	auto p = 31;
	auto h = 0;
	for (char c : value) {
		h = p * h + c;
	}
    return h;
}
