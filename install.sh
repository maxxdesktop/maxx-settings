#!/bin/bash

if [[ "`id -u`" -ne 0 ]];
  then echo "Installing MaXX Settings requires root/superuser level, aborting."
  exit
fi
. /opt/MaXX/etc/system.runtime

echo "Installing MaXX Settings CLI in $MAXX_HOME."
echo " "
set -v
mkdir -p $MAXX_BIN
install ./bin/* $MAXX_BIN/
mkdir -p $MAXX_HOME/share/doc
cp ./doc/* $MAXX_HOME/share/doc/
mkdir -p $MAXX_HOME/java
cp ./target/desktop-msettings.jar $MAXX_HOME/java/
set +v
echo " "
echo "Installing MaXX Settings CLI Completed."
