package com.maxxinteractive.msettings;

/**
 * Enum for supported ColorSpace
 */
public enum ColorSpace {
    RGB255,
    RGB100,
    YUV,
    HSL,
    CMYK

}
