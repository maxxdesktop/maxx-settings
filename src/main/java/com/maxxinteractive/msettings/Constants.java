package com.maxxinteractive.msettings;

public class Constants {

	// Schema
	public static final String ATTRIBUTE_VERSION    = "version";
	public static final String ATTRIBUTE_UUID       = "uuid";
	public static final String ATTRIBUTE_STEREOTYPE = "stereotype";
	public static final String ATTRIBUTE_NAME       = "name";

	// Instrument
	public static final String ATTRIBUTE_DEFAULT = "default";
	public static final String ATTRIBUTE_VALUE   = "value";

	// Choice
	public static final String ATTRIBUTE_TYPE   = "type";
	public static final String ATTRIBUTE_CATALOG  = "catalog";
	public static final String ATTRIBUTE_OPTION = "option[%d]";

	// Chars
	public static final String ATTRIBUTE_ENCODING   = "encoding";
	public static final String ATTRIBUTE_MAX_LENGTH = "maxLength";

	//Gauge
	public static final String ATTRIBUTE_MINIMUM = "minimum";
	public static final String ATTRIBUTE_MAXIMUM = "maximum";
	public static final String ATTRIBUTE_SCALE   = "scale";

	// Color
	public static final String ATTRIBUTE_COLOR_SPACE	= "colorSpace";
	public static final String ATTRIBUTE_ALPHA			= "alpha";

	// Image
	public static final String ATTRIBUTE_FILE_PATH	= "filePath";
	public static final String ATTRIBUTE_DIMENSION	= "dimension";
	public static final String ATTRIBUTE_CROP		= "crop";
	public static final String ATTRIBUTE_RESIZE_TO	= "resizeTo";

	// Typeface
	public static final String ATTRIBUTE_FONT		= "font";
	public static final String ATTRIBUTE_SIZE		= "size";
	public static final String ATTRIBUTE_STYLE 		= "style";
	public static final String ATTRIBUTE_WEIGHT 	= "weight";
	public static final String ATTRIBUTE_SLANT 		= "slant";

	// Command
	public static final String ATTRIBUTE_EXEC_NAME			= "execName";
	public static final String ATTRIBUTE_EXEC_PATH			= "execPath";
	public static final String ATTRIBUTE_EXEC_PARAMS		= "execParams";
	public static final String ATTRIBUTE_ENV_BINARY_PATH	= "envBinaryPath";
	public static final String ATTRIBUTE_ENV_LIBRARY_PATH	= "envLibraryPath";
	public static final String ATTRIBUTE_GEOMETRY			= "geometry";

	// Application
	public static final String ATTRIBUTE_OPEN_COMMAND	= "openCommand";
	public static final String ATTRIBUTE_VIEW_COMMAND	= "viewCommand";
	public static final String ATTRIBUTE_EDIT_COMMAND 	= "editCommand";
	public static final String ATTRIBUTE_PRINT_COMMAND 	= "printCommand";

	// MSettings Paths
	public static final String MSETTINGS_HOME_SYSTEM 	= "/opt/MaXX/share/msettings";
	public static String MSETTINGS_HOME_USER 			= ".maxxdesktop/msettings";

	public static final String MSETTINGS_INSTRUMENTS_HOME = MSETTINGS_HOME_SYSTEM + "/Instruments";
	public static  String MSETTINGS_USER_PREFERENCES_HOME = getUserHome() + "/Preferences";
	public static final String CHOICES 				= "Choices";
	public static final String CATALOGS				= "Catalogs";
	public static final String APPLICATIONS			= "Applications";
	public static final String FILETYPES			= "FileTypes";

	public static final String VERSION_10 			= "1.0";
	public static final String VERSION_11 			= "1.1";

	public static final String[] EMPTY_ARRAY 		= {};
	public static final String ENCODING_UTF8 		= "UTF-8";
	public static final String ENCODING_UTF16		= "UTF-16";

	public static final String LOG_MESSAGE_MISSING_ATTRIBUTE = "Attribute is missing";
	public static final String LOG_MESSAGE_NULL_ATTRIBUTE_SET_DEFAULT = "Attribute received NULL values, setting to default values";
	public static final String LOG_MESSAGE_NULL_ATTRIBUTE_IGNORE = "Attribute received NULL values, will ignore assignation";
	public static final String LOG_MESSAGE_MISSING_EXTENSION = "Attribute Extension is missing";

	public static final String LOG_MESSAGE_OPTION_INDEX_NOT_FOUND_INT_ATTRIBUTE = "Attribute for option[%d] was not found within the range of supported option's id";
	public static final String LOG_MESSAGE_OUT_OF_RANGE_INT_ATTRIBUTE = "Attribute with values '%d' was out of range, value is set to '%d'";
	public static final String LOG_MESSAGE_OUT_OF_RANGE_FLOAT_ATTRIBUTE = "Attribute with values '%.2f' was out of range, value is set to '%.2f'";

	public static String getUserHome() {
		String home = System.getProperty("user.home");
		if(home == null || home.isEmpty()) {
			home = System.getenv("HOME");
		}
		return home + "/"+ MSETTINGS_HOME_USER;
	}
}