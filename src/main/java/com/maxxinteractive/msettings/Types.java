package com.maxxinteractive.msettings;

/**
 * Attribute values types
 */
public enum Types {

	UUID,
	String,
	Integer, 
	Float,
	Boolean,
	/*	to remove
	Stereotype,
  	Dimension,
    Location,
     Geometry,*/
	NULL
}
