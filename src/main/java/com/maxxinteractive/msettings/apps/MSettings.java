package com.maxxinteractive.msettings.apps;

import com.maxxinteractive.msettings.cli.CLICommand;
import com.maxxinteractive.msettings.cli.CLIContext;
import com.maxxinteractive.msettings.cli.CLIParser;
import com.maxxinteractive.msettings.cli.ICLICommand;
import com.maxxinteractive.msettings.cli.commands.*;
import com.maxxinteractive.msettings.commons.Logger;
import com.maxxinteractive.msettings.utils.FileSystemHelper;
import com.maxxinteractive.msettings.utils.HasherUtility;
import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.util.Properties;

public class MSettings implements Runnable {

    // CMD
    private CLICommand cliCommand = CLICommand.UNKNOWN;

    // CLI Parser
    CLIParser cliParser;

    //CLI Context
    CLIContext cliContext;

    public static void main(String[] args){

        MSettings msettings = new MSettings(args);

        msettings.run();
    }

    public MSettings(String[] args){

        cliParser = new CLIParser(args);
        Logger.debug("Starting session...");
    }

    public void run() {

        ICLICommand cli = null;
        // 1.  Parse command line params
        boolean status = cliParser.parseCommandLine();

        // 2.  Handle Parsing status
        if(!status){
            Logger.warning("Parsing Failed.");
            terminate(1);
        }
        Logger.debug("Parsing complete.");

        // 3. Get CLIContext
        cliContext = cliParser.getContext();

        // 4. Get CLI Command
        cliCommand = cliContext.getCliCommand();

        /////////////////////////////////////////////
        // Hashing Utility
        //
        // ms HASH -n Desktop.Mouse.Acceleration
        // msettings H -n Desktop.Mouse.Acceleration

        if(cliCommand == CLICommand.HASH) {
            Logger.debug("HASH CLI Storage Location Utility");
            StringBuilder builder = new StringBuilder(64);
            for(String name : cliContext.getNames()){
                String hash = HasherUtility.generateDirectoryHash(name.toLowerCase());
                builder.append(String.format("- %s = %s\n",name, hash));
            }
            System.out.println(builder.toString().trim());
            terminate(0);
        }

        /////////////////////////////////////////////
        // INIT Database
        //
        //  ms INIT -f | --force
        //  ms I

        if(cliCommand == CLICommand.INIT){
            Logger.debug("INIT CLI starting...");
            cli = new InitializeDatabase(cliContext);
            boolean result = cli.runCommand();
            Logger.debug("INIT CLI success="+result);
            terminate(result?0:1);
        }

        /////////////////////////////////////////////
        // LIST Instrument
        //
        // ms LIST
        // msettings LIST

        if(cliCommand == CLICommand.LIST) {
            Logger.debug("LIST CLI starting...");
            cli = new ListInstrument(cliContext);
            boolean result = cli.runCommand();
            Logger.debug("LIST CLI success="+result);
            terminate(result ? 0 : 1);
        }

        /////////////////////////////////////////////
        // CREATE Instrument
        //
        // ms CREATE -f ./DesktopMouse_Acceleration.Gauge
        // ms C      -f ./DesktopMouse_Acceleration.Gauge
        // ms C      -A name=Desktop.Mouse.Acceleration,stereotype=Gauge,minimum=1,maximum=5,scale=1,default=2

        if(cliCommand == CLICommand.CREATE) {
            Logger.debug("CREATE CLI starting...");
            cli = new CreateInstrument(cliContext);
            boolean result = cli.runCommand();
            Logger.debug("CREATE CLI success="+result);
            terminate(result ? 0 : 1);
        }

        /////////////////////////////////////////////
        // UPDATE Instrument
        //
        // ms UPDATE -f ./DesktopMouse_Acceleration.Gauge
        // ms U      -f ./DesktopMouse_Acceleration.Gauge

        if(cliCommand == CLICommand.UPDATE){
          //  Logger.debug("UPDATE CLI Starting...");
            Logger.warning("UPDATE CLI Feature is not yet implemented.");
         //   Logger.debug("UPDATE CLI success="+false);
            terminate(1);
        }

        /////////////////////////////////////////////
        // GET Instrument
        //
        // ms GET -n Desktop.Mouse.LeftHand
        // ms G Desktop.Mouse.LeftHand
        // ms Desktop.Mouse.LeftHand
        //
        // -x for long format output

        if(cliCommand == CLICommand.GET) {
            Logger.debug("GET CLI starting...");
            cli = new GetUserPreference(cliContext);
            boolean result = cli.runCommand();
            Logger.debug("GET CLI success="+result);
            terminate(result ? 0 : 1);
        }

        /////////////////////////////////////////////
        // SET Instrument
        //
        // ms SET -n Desktop.Mouse.LeftHand=true
        // ms S -n Desktop.Mouse.LeftHand=false

        if(cliCommand == CLICommand.SET) {
            Logger.debug("GET CLI starting...");
            cli = new SetUserPreference(cliContext);
            boolean result = cli.runCommand();
            Logger.debug("GET CLI success="+result);
            terminate(result ? 0 : 1);
        }

        /////////////////////////////////////////////
        // RESET Instrument
        //
        // ms RESET -n Desktop.Mouse.LeftHand
        // ms R -n Desktop.Mouse.LeftHand

        if(cliCommand == CLICommand.RESET){
            Logger.debug("RESET CLI starting...");
            cli = new ResetUserPreference(cliContext);
            boolean result = cli.runCommand();
            Logger.debug("RESET CLI success="+result);
            terminate(result ? 0 : 1);
        }
    }

    public static void terminate(int status){
        if(status == 0) {
            Logger.debug("Closing session, execution was successful.");
        }else {
            Logger.warning("Closing session, execution failed. Consults the log file for detail.");
            if(Logger.isSilentMode()){
                System.err.println("failed");
            }
        }
        System.exit(status);
    }

    private static final String VERSION = "MaXX Settings CLI version %s ";
    private static final String version = "1.0";

    public static void printVersion(){
        System.out.printf((VERSION) + "%n", version);
    }
}