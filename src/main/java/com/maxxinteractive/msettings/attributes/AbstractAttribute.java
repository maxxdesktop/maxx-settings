package com.maxxinteractive.msettings.attributes;

import com.maxxinteractive.msettings.Types;
import com.maxxinteractive.msettings.attributes.values.IAttributeValue;

import java.util.Objects;

/**
 * @author emasson
 * Abstraction of a Schema's key=values pair.  aka an Attribute
 */
public abstract class AbstractAttribute implements IAttribute{

	protected String name;
	protected IAttributeValue value;

	/**
	 * Attribute field's name
	 * @return name of the attributes
	 */
	@Override
	public final String getName() {
		return name;
	}

	/**
	 * Attribute's values Type.  meaning it's a String, Bool, Integer, etcsc
	 * @return type
	 */
	@Override
	public final Types getType() {
		return value.getType();
	}

	public final IAttributeValue getValue() {
		return value;
	}

	public String getValueAsString() {
		return value.getValueAsString();
	}

	public boolean getValueAsBoolean() {
		return value.getValueAsBoolean();
	}

	public int getValueAsInteger() {
		return value.getValueAsInteger();
	}

	public float getValueAsFloat() {
		return value.getValueAsFloat();
	}

	public final String serialize () {
		return name + "=" + getValueAsString();
	}

	@Override
	public boolean equals (Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		AbstractAttribute that = (AbstractAttribute) o;
		return Objects.equals(name, that.name) &&
				value.getType() == that.getType();
	}

	@Override
	public int hashCode () {
		return Objects.hash(name, value.getType());
	}

	@Override
	public String toString () {
		return name+"="+value.toString();
	}

	@Override
	public boolean isNotPresent() {
		return false;
	}
}
