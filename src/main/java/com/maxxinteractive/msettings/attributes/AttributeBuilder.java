package com.maxxinteractive.msettings.attributes;

import com.maxxinteractive.msettings.Types;
import com.maxxinteractive.msettings.attributes.values.*;
import com.maxxinteractive.msettings.commons.Logger;
import com.maxxinteractive.msettings.stereotype.Stereotypes;

import java.util.UUID;

public class AttributeBuilder {

	// NullNotPresent Attribute
	public static IAttribute buildNullNotPresent(String name, String value){
		return new NullNotPresentAttribute(name);
	}

	// String Attribute
	public static IAttribute buildString(String name, String value){
		return new StringAttribute(name, new StringValue(value));
	}

	// UUID Attribute
	public static IAttribute buildUUID(String name, UUID value){
		return new UUIDAttribute(name, new UUIDValue(value) );
	}

	public static IAttribute buildUUID(String name, String value){
		return new UUIDAttribute(name, value);
	}

	// Boolean Attribute
	public static IAttribute buildBoolean(String name, boolean value){
		return new BooleanAttribute(name, new BooleanValue(value));
	}

	public static IAttribute buildBoolean(String name, Boolean value){
		return new BooleanAttribute(name, new BooleanValue(value));
	}

	public static IAttribute buildBoolean(String name, String value){
		return new BooleanAttribute(name, value);
	}

	// Integer Attribute
	public static IAttribute buildInteger(String name, int value){
		return new IntegerAttribute(name, new IntegerValue(value));
	}

	public static IAttribute buildInteger(String name, String value){
		return new IntegerAttribute(name, value);
	}

	public static IAttribute buildInteger(String name, Integer value){
		return new IntegerAttribute(name, new IntegerValue(value));
	}

	// Float Attribute
	public static IAttribute buildFloat(String name, float value){
		return new FloatAttribute(name, new FloatValue(value));
	}

	public static IAttribute buildFloat(String name, String value){
		return new FloatAttribute(name, value);
	}

	public static IAttribute buildFloat(String name, Float value){
		return new FloatAttribute(name, new FloatValue(value));
	}

	/**
	 * Utility method to create a new instance of IAttribute based on the corresponding Stereotype and a Attribute Value.
	 * @param name attribute name
	 * @param val IAttributeValue
	 * @param stereotype type of Schema, for validation purpose
	 * @return a new IAttribute or null if it failed.
	 */
	public static IAttribute buildWithAttributeValue(String name, IAttributeValue val, Stereotypes stereotype){
		IAttribute att = null;
		Types type = val.getType();
		switch  (type) {
			case String: {
				if (stereotype == Stereotypes.Chars || stereotype == Stereotypes.Dimension ||
						stereotype == Stereotypes.Choice ||
						stereotype == Stereotypes.Location || stereotype== Stereotypes.Geometry ||
						stereotype== Stereotypes.Command || stereotype== Stereotypes.Application ||
						stereotype== Stereotypes.Typeface || stereotype== Stereotypes.Color){
					att = new StringAttribute(name, (StringValue) val);
				}
				break;
			}
			case Integer: {
				if (stereotype == Stereotypes.Number) {
					att = new IntegerAttribute(name, (IntegerValue) val);
				}
				break;
			}
			case Float: {
				if (stereotype == Stereotypes.Decimal || stereotype == Stereotypes.Gauge || stereotype== Stereotypes.Typeface || stereotype== Stereotypes.Color) {
					att = new FloatAttribute(name, (FloatValue) val);
				}
				break;
			}
			case Boolean: {
				if (stereotype == Stereotypes.Logical) {
					att = new BooleanAttribute(name, (BooleanValue) val);
				}
				break;
			}
			default:
				Logger.error (new Exception("No Match value with Stereotytpe for AttributeValue."));
		}
		return att;
	}
}
