package com.maxxinteractive.msettings.attributes;

import com.maxxinteractive.msettings.attributes.values.BooleanValue;

public class BooleanAttribute extends AbstractAttribute {

	public BooleanAttribute (String name, boolean value) {
		this.name = name;
		this.value = new BooleanValue(value);
	}

	public BooleanAttribute (String name, String value) {
		this.name = name;
		Boolean tmp = null;
		tmp = Boolean.parseBoolean(value);
		this.value = new BooleanValue(tmp);
	}

	public BooleanAttribute (String name, BooleanValue value) {
		this.name = name;
		this.value = value;
	}

}
