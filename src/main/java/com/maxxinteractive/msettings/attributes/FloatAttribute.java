package com.maxxinteractive.msettings.attributes;

import com.maxxinteractive.msettings.attributes.values.FloatValue;

public class FloatAttribute extends AbstractAttribute {

	public FloatAttribute (String name, float value) {
		this.name = name;
		this.value = new FloatValue(value);
	}

	public FloatAttribute (String name, String value) {
		this.name = name;
		Float tmp = null;
		tmp = Float.parseFloat(value);
		this.value = new FloatValue(tmp);
	}

	public FloatAttribute (String name, FloatValue value) {
		this.name = name;
		this.value = value;
	}


}
