package com.maxxinteractive.msettings.attributes;

import com.maxxinteractive.msettings.Types;
import com.maxxinteractive.msettings.attributes.values.IAttributeValue;

public interface IAttribute {

	String getName();

	Types getType();

	IAttributeValue getValue();

	String getValueAsString();
	boolean getValueAsBoolean();
	int getValueAsInteger();
	float getValueAsFloat();

	boolean isNotPresent();
}
