package com.maxxinteractive.msettings.attributes;

import com.maxxinteractive.msettings.attributes.values.IntegerValue;

public class IntegerAttribute extends AbstractAttribute {

	public IntegerAttribute (String name, int value) {
		this.name = name;
		this.value = new IntegerValue(value);
	}

	public IntegerAttribute (String name, String value) {
		this.name = name;
		Integer tmp = null;
		tmp = Integer.parseInt(value);
		this.value = new IntegerValue(tmp);
	}

	public IntegerAttribute (String name, IntegerValue value) {
		this.name = name;
		this.value = value;
	}
}