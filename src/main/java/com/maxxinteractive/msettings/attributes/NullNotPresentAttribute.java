package com.maxxinteractive.msettings.attributes;

import com.maxxinteractive.msettings.attributes.values.NullValue;

public class NullNotPresentAttribute extends AbstractAttribute{

    public NullNotPresentAttribute(String name) {
        this.name = name;
        this.value = new NullValue();
    }

    @Override
    public boolean isNotPresent() {
        return true;
    }

    @Override
    public String toString () {
        return name+"=null";
    }

}
