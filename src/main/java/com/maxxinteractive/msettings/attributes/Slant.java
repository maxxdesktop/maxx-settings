package com.maxxinteractive.msettings.attributes;
/**
 * MaXX Settings Typeface Slant values
 */
public enum Slant {
    Roman,      // 0
    Italic,     // 100
    Oblique;    // 110
}
