package com.maxxinteractive.msettings.attributes;

import com.maxxinteractive.msettings.attributes.values.StringValue;

public class StringAttribute extends AbstractAttribute {

	public StringAttribute (String name, String value) {
		this.name = name;
		this.value = new StringValue(value==null?"":value);
	}

	public StringAttribute (String name, StringValue value) {
		this.name = name;
		this.value = value;
	}
}

