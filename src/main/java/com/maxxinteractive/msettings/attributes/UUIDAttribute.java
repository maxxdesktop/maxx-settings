package com.maxxinteractive.msettings.attributes;

import com.maxxinteractive.msettings.attributes.values.UUIDValue;

import java.util.UUID;

public class UUIDAttribute extends AbstractAttribute{

	public UUIDAttribute (String name, String value) {
		this(name, value==null?UUID.randomUUID():UUID.fromString(value));
	}

	public UUIDAttribute (String name, UUID value) {
		this.name = name;
		this.value = new UUIDValue(value==null?UUID.randomUUID():value);
	}

	public UUIDAttribute (String name, UUIDValue value) {
		this.name = name;
		this.value = value;
	}

}
