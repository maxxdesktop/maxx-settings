package com.maxxinteractive.msettings.attributes;

/**
 * MaXX Settings Typeface Weight values
 */
public enum Weight {
    Light,      // 0
    Medium,     // 100
    Demibold,   // 180
    Bold,       // 200
    Black;      // 210M
}
