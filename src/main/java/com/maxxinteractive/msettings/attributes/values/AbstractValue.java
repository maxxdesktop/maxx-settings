package com.maxxinteractive.msettings.attributes.values;

import com.maxxinteractive.msettings.Types;

/**
 * @author emasson
 * Abstraction of a Value. This class is used for Attribute's values
 */
public class AbstractValue implements  IAttributeValue {

	protected Types type;
	protected Object value;

	@Override
	public final Types getType () {
		return type;
	}

	public Object getValue() {
		return null;
	}

	public String getValueAsString() {
		return null;
	}
	public boolean getValueAsBoolean() {
		return false;
	}

	public int getValueAsInteger() {
		return 0;
	}

	public float getValueAsFloat() {
		return 0f;
	}

	public String toString(){
		return getValueAsString();
	}
}
