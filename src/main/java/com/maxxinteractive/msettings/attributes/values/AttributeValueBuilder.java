package com.maxxinteractive.msettings.attributes.values;

import com.maxxinteractive.msettings.commons.Logger;
import com.maxxinteractive.msettings.stereotype.Stereotypes;

public class AttributeValueBuilder {

    /**
     * Utility method to create a new instance of IAttributeValue based on the corresponding Stereotype and a String Value.
     * @param value raw value in String form
     * @param stereotype type of Schema, for validation purpose
     * @return a new IAttribute or null if it failed.
     */
    public static IAttributeValue builtWithStringValue(String value, Stereotypes stereotype){
        IAttributeValue att = null;
        try {
            if (stereotype == Stereotypes.Chars || stereotype == Stereotypes.Dimension ||
                    stereotype == Stereotypes.Location || stereotype == Stereotypes.Geometry ||
                    stereotype == Stereotypes.Command || stereotype == Stereotypes.Application ||
                    stereotype == Stereotypes.Typeface || stereotype == Stereotypes.Color) {
                att = new StringValue(value);
            } else if (stereotype == Stereotypes.Number || stereotype == Stereotypes.Choice) {
                att = new IntegerValue(IntegerValue.getValueFromString(value));
            } else if (stereotype == Stereotypes.Decimal || stereotype == Stereotypes.Gauge) {
                att = new FloatValue(FloatValue.getValueFromString(value));
            } else if (stereotype == Stereotypes.Logical) {
                att = new BooleanValue(BooleanValue.getValueFromString(value));
            }
        }
        catch(Exception e){
            Logger.error(e);
            return att;
        }
        if (att == null) {

            Logger.error (new Exception("No Match AttributeValue with Stereotytpe "+ stereotype.name() + " for value="+value));
        }
        return  att;
    }
}
