package com.maxxinteractive.msettings.attributes.values;

import com.maxxinteractive.msettings.Types;

public class BooleanValue extends AbstractValue{

	protected Boolean value ;

	public BooleanValue (boolean value) {
		this(new Boolean(value));
	}

	public BooleanValue (Boolean value) {
		type = Types.Boolean;
		this.value = value;
	}

	public static boolean getValueFromString(String value){
		return Boolean.valueOf(value);
	}

	public Boolean getValue(){
		return value;
	}

	@Override
	public String getValueAsString () {
		return value.toString();
	}

	public boolean getValueAsBoolean () {
		return value.booleanValue();
	}

	public int getValueAsInteger () {
		return value.booleanValue()?1:0;
	}

	public float getValueAsFloat () {
		return value.booleanValue()?1f:0f;
	}
}
