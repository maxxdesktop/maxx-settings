package com.maxxinteractive.msettings.attributes.values;

import com.maxxinteractive.msettings.Types;

public class FloatValue extends AbstractValue {

	protected Float value;

	public FloatValue (float value) {
		this(new Float(value));
	}

	public FloatValue (Float value) {
		type = Types.Float;
		this.value = new Float(value);
	}

	public Float getValue(){
		return value;
	}

	public static Float getValueFromString(String value){
		return Float.valueOf(value);
	}

	@Override
	public String getValueAsString () {
		return value.toString();
	}

	@Override
	public boolean getValueAsBoolean () {
		return value.intValue() == 0 ? true : false;
	}

	@Override
	public int getValueAsInteger () {
		return value.intValue();
	}

	@Override
	public float getValueAsFloat () {
		return value.floatValue();
	}
}
