package com.maxxinteractive.msettings.attributes.values;
import com.maxxinteractive.msettings.Types;

/**
 * Attribute value abstraction.
 * @author emasson
 */
public interface IAttributeValue {

	public Types getType();

	public Object getValue();

	public String getValueAsString();
	public boolean getValueAsBoolean();
	public int getValueAsInteger();
	public float getValueAsFloat();

}
