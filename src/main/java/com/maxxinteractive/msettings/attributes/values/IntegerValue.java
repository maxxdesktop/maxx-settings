package com.maxxinteractive.msettings.attributes.values;

import com.maxxinteractive.msettings.Types;

public class IntegerValue extends AbstractValue {

	protected Integer value;

	public IntegerValue (int value) {
		this(new Integer(value));
	}

	public IntegerValue (Integer value) {
		type = Types.Integer;
		this.value = new Integer(value);
	}

	public static Integer getValueFromString(String value){
		return Integer.parseInt(value);
	}

	public Integer getValue(){
		return value;
	}

	@Override
	public String getValueAsString () {
		return value.toString();
	}

	@Override
	public boolean getValueAsBoolean () {
		return value.intValue() == 0 ? true : false;
	}

	@Override
	public int getValueAsInteger () {
		return value.intValue();
	}

	@Override
	public float getValueAsFloat () {
		return value.floatValue();
	}
}
