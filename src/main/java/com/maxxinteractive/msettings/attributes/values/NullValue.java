package com.maxxinteractive.msettings.attributes.values;

import com.maxxinteractive.msettings.Types;

public class NullValue extends AbstractValue {

    public NullValue() {
        type = Types.NULL;
    }

}
