package com.maxxinteractive.msettings.attributes.values;

import com.maxxinteractive.msettings.Types;

public class StringValue extends AbstractValue{

	protected String value;

	public StringValue (String value) {
		type = Types.String;
		this.value = value==null?"":value;
	}

	public String getValue() {
		return value;
	}

	@Override
	public String getValueAsString () {
		return value;
	}

	@Override
	public boolean getValueAsBoolean () {
		return false;
	}


	@Override
	public int getValueAsInteger () {

		return 0;
	}

	@Override
	public float getValueAsFloat () {
		return 0;
	}
}
