package com.maxxinteractive.msettings.attributes.values;

import com.maxxinteractive.msettings.Types;

import java.util.UUID;

public class UUIDValue extends AbstractValue {

	protected UUID value;

	public UUIDValue (UUID value) {
		type = Types.UUID;
		this.value = value==null?UUID.randomUUID():value;
	}

	public UUID getValue(){
		return value;
	}

	@Override
	public String getValueAsString () {
		return value.toString();
	}

	@Override
	public boolean getValueAsBoolean () {
		return false;
	}

	@Override
	public int getValueAsInteger () {
		return 0;
	}

	@Override
	public float getValueAsFloat () {
		return 0f;
	}
}
