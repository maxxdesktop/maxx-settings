package com.maxxinteractive.msettings.cli;

import com.maxxinteractive.msettings.attributes.IAttribute;
import com.maxxinteractive.msettings.cli.commands.CLIUserPreference;
import com.maxxinteractive.msettings.commons.Logger;
import com.maxxinteractive.msettings.database.Database;
import com.maxxinteractive.msettings.database.IDatabase;
import com.maxxinteractive.msettings.instruments.Instrument;
import com.maxxinteractive.msettings.instruments.InstrumentBuilder;
import com.maxxinteractive.msettings.instruments.InstrumentParser;
import com.maxxinteractive.msettings.instruments.choices.Catalog;
import com.maxxinteractive.msettings.instruments.choices.Choice;
import com.maxxinteractive.msettings.preferences.IUserPreference;
import com.maxxinteractive.msettings.stereotype.Stereotypes;
import com.maxxinteractive.msettings.utils.FileSystemInstrumentHelper;
import com.maxxinteractive.msettings.utils.HasherUtility;
import com.maxxinteractive.msettings.utils.KeyValuePair;
import org.jetbrains.annotations.NotNull;

import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.Optional;

import static com.maxxinteractive.msettings.Constants.*;
import static com.maxxinteractive.msettings.database.IndexEntry.PATH_LENGTH;
import static com.maxxinteractive.msettings.database.IndexEntry.UUID_LENGTH;

public abstract class AbstractCommand implements ICLICommand {

    protected CLIContext cliContext = null;
    protected boolean continueOnFailure;
    protected IUserPreference instrument = null;
    protected KeyValuePair userPref = null;

    protected Optional<String> chunk = Optional.empty();

    protected AbstractCommand(@NotNull CLIContext context){
        cliContext = context;
    }

    public boolean isContinueOnFailure() {
        return continueOnFailure;
    }

    public void setContinueOnFailure(boolean continueOnFailure) {
        this.continueOnFailure = continueOnFailure;
    }

    public boolean runCommand() {

        if(cliContext.isUsingName()) {
            Iterator<String> iterator = cliContext.getNames().iterator();
            while (iterator.hasNext()) {
                String name = iterator.next();
                boolean status = doInstrumentByName(name);
                if(!status && !continueOnFailure){
                    return false;
                }

                if (cliContext.isLongFormat() && iterator.hasNext()) {
                    System.out.println("-----");
                }
            }
        }else {
            Iterator<String> iterator = cliContext.getUuids().iterator();
            while (iterator.hasNext()) {
                String uuid = iterator.next();
                boolean status = doInstrumentByUUID(uuid);
                if(!status && !continueOnFailure){
                    return false;
                }
                if (cliContext.isLongFormat() && iterator.hasNext()) {
                    System.out.println("-----");
                }
            }
        }
        return true;
    }

    protected void reset(){

    }

    /**
     * Extract Schema from Instrument name
     * @param name instrument
     * @return schema name
     */
    public Optional<String> getSchema(@NotNull String name){
        String schema = null;
        int index = name.lastIndexOf(".");
        if (index > -1) {
            schema = name.substring(index + 1);
        } else {
            Logger.error(new Exception("Unable to extract Schema name from input parameters."));
            return Optional.empty();
        }
        return Optional.of(schema);
    }

    /**
     * Try to fetch DB Record with a hashed directory and Schema name
     * @param hash   for ex:  /a4/e2/ff/1q
     * @param schema for ex:  Acceleration
     * @return {@code Optional<String>}
     */
    protected Optional<String> fetchRecordByHashAndName(@NotNull String hash, @NotNull String schema){

        IDatabase db = Database.getInstance();
        if (db.open()) {
            chunk = db.findByHashFilename(hash + schema);
            db.closeDB();
        }else {
            Logger.error(new Exception("Unable to open the Database."));
            return Optional.empty();
        }
        if(!chunk.isPresent()) {
            Logger.error(new Exception("Unable to locate Instrument DB record. hash="+hash+" schema="+schema));
        }
        return chunk;
    }

    protected Optional<String> fetchRecordByUUID(@NotNull String uuid) {

        IDatabase db = Database.getInstance();
        if (db.open()) {
            chunk = db.findByUUID(uuid);
            db.closeDB();
        } else {
            Logger.error(new Exception("Unable to open the Database."));
            return Optional.empty();
        }

        if (!chunk.isPresent()) {
            Logger.error(new Exception("Unable to locate Instrument DB record."));
        }
        return chunk;
    }

    protected String getHashFromChunk(@NotNull String chunk){
        return chunk.substring(UUID_LENGTH, UUID_LENGTH + PATH_LENGTH);
    }

    protected String getSchemaFromChunk(@NotNull String chunk){
        return chunk.substring(UUID_LENGTH+PATH_LENGTH).trim();
    }

    /**
     * Fetch Instrument and UserPreference KeyValuePair
     * @param hash directory
     * @param schemaFile name of the Instrument
     * @return an {@code Optional<CLIUserPreference>}
     */
    protected Optional<CLIUserPreference> doFetch(@NotNull String hash, @NotNull String schemaFile){

        ////////////////////////////////
        // 1. Fetch System wide Instrument
        Optional<IUserPreference> instrumentOutput = fetchInstrument(hash,schemaFile);
        if (!instrumentOutput.isPresent()) {
            return Optional.empty();
        }

        Optional<KeyValuePair> userPrefOutput = fetchUserPreference(hash,schemaFile);
        if (!userPrefOutput.isPresent()) {
            return Optional.empty();
        }

        IUserPreference instrument = instrumentOutput.get();
        KeyValuePair userPref = userPrefOutput.get();
        CLIUserPreference cliUserPreference = new CLIUserPreference(instrument, userPref);
        cliUserPreference.setHash(hash);
        cliUserPreference.setSchemaFile(schemaFile);

        ////////////////////////////////
        // 2. Resolver (optional step)
        String resolverString = userPref.getValue();
        if( cliContext.isResolver()){
            cliUserPreference = resolver(cliUserPreference);
        }

        return Optional.of(cliUserPreference);
    }

    /**
     * Recursive UserPreference Value Resolver
     * @param cliUserPreference input
     * @return resolved output
     */
    protected CLIUserPreference resolver(@NotNull CLIUserPreference cliUserPreference){

        IUserPreference instrument = cliUserPreference.getInstrument();
        KeyValuePair keyValuePair = cliUserPreference.getUserPref();

        // Resolver is only supported for Chars, Choice and Command Stereotypes
        if( instrument.getStereotype() != Stereotypes.Choice &&
            instrument.getStereotype() != Stereotypes.Chars  &&
            instrument.getStereotype() != Stereotypes.Command) {
            return cliUserPreference;
        }

        String unresolvedValue = null;
        // Fetch text value before attempting a resolution.
        if( instrument.getStereotype() == Stereotypes.Choice){
            String index = keyValuePair.getValue();
            Choice choice = (Choice)instrument;
            unresolvedValue = choice.getOption(index);
        }else {
            unresolvedValue = keyValuePair.getValue();
        }

        // Check if value contains an Instrument's reference
        if(!unresolvedValue.startsWith("@")){
            Logger.info("Resolver: Not required for '"+instrument.getStereotype().name() +"' Instrument with value="+unresolvedValue);
            return cliUserPreference;
        }
        Logger.info("Resolver: YES '"+instrument.getStereotype().name() +"' Instrument with value="+unresolvedValue);

        // Resolve Instrument's reference
        while(unresolvedValue.startsWith("@")) {

            Logger.info("Resolver: LOOP Instrument with value="+unresolvedValue);
            //strip '@' from value
            String instrumentRefName = unresolvedValue.substring(1);

            String schema = null;
            String hash = null;
            String schemaFile;

            ////////////////////////////////
            // 1. Get Schema name
            Optional<String> schemaOpt = getSchema(instrumentRefName);
            if (!schemaOpt.isPresent()){
                return cliUserPreference;
            }
            schema = schemaOpt.get();

            ////////////////////////////////
            // 2. Calc hash based on Instrument name + toLowerCase()
            hash = HasherUtility.generateDirectoryHash(instrumentRefName.toLowerCase());

            ////////////////////////////////
            // 3. Lookup for matching record and extract schemaFile
            Optional<String> chunk = fetchRecordByHashAndName(hash,schema);
            if(!chunk.isPresent()) {
                return cliUserPreference;
            }

            // extract schemaFile then load the InstrumentFile = chunk.substring (UUID_LENGTH+PATH_LENGTH).trim();
            schemaFile = getSchemaFromChunk(chunk.get());
            if(schemaFile.isEmpty()){
                Logger.error(new Exception("Unable to extract required information from the input parameters."));
                return cliUserPreference;
            }

            // fetch UserPreference
            Optional<CLIUserPreference> resolvedUserPrefOpt = doFetch(hash,schemaFile);
            if (!resolvedUserPrefOpt.isPresent()) {
                Logger.error(new Exception("Failed to resolve : "+ instrumentRefName));
                return cliUserPreference;
            }

            // fetch new 'resolved' CLIUserPreference
            cliUserPreference = resolvedUserPrefOpt.get();

            // fetch new unresolved value
            unresolvedValue = cliUserPreference.getUserPref().getValue();

            // Check if value contains an Instrument's reference
            if(!unresolvedValue.startsWith("@")){
                Logger.info("Resolver: Not required for '"+cliUserPreference.getInstrument().getStereotype().name() +"' Instrument with value="+unresolvedValue+ " EXIT") ;
                return cliUserPreference;
            }


        } //while loop

        return cliUserPreference;
    }



    /**
     * Common Logic that Fetch a System wide Instrument as anIUserPreference
     * @param hash to use for the operation
     * @param schemaFile to use for the operation
     * @return resulting IUserPreference
     */
    protected Optional<IUserPreference> fetchInstrument(@NotNull String hash, @NotNull String schemaFile) {

        ////////////////////////////////
        // 1. Load Instrument as List<KeyValuePair>
        Path path;
        try {
            path = FileSystemInstrumentHelper.buildPath(MSETTINGS_INSTRUMENTS_HOME, hash, schemaFile);
        }catch (InvalidPathException e){
            return Optional.empty();
        }
        String filename = path.toFile().getAbsolutePath();

        InstrumentParser parser = new InstrumentParser(filename);
        if(!parser.parse()){
            return Optional.empty();
        }

        ////////////////////////////////
        // 2. Build IUserPreference
        InstrumentBuilder instrumentBuilder = new InstrumentBuilder(parser.getAttributes());
        Optional<IUserPreference> instrumentOpt = instrumentBuilder.build();

        if(!instrumentOpt.isPresent()){
            Logger.error(new Exception("Unable to build IUserPreference with : "+schemaFile));
            return Optional.empty();
        }

        IUserPreference instrument = instrumentOpt.get();

        return instrumentOpt;
    }

    protected Optional<KeyValuePair> fetchUserPreference(@NotNull String hash, @NotNull String schemaFile) {
        return FileSystemInstrumentHelper.loadUserPreference(hash,schemaFile);
    }

    /**
     * Load the requested Catalog
     * @param catalogName name
     * @return found Catalog
     */
    protected Optional<Catalog> fetchCatalog(@NotNull String catalogName) {
        ////////////////////////////////
        // 1. Load Catalog as List<KeyValuePair>
        Path path;
        try {
            path = Paths.get(MSETTINGS_INSTRUMENTS_HOME  + "/" + CATALOGS+ "/"+ catalogName+".Catalog");
        }catch (InvalidPathException e){
            return Optional.empty();
        }
        String filename = path.toFile().getAbsolutePath();

        InstrumentParser parser = new InstrumentParser(filename);
        if(!parser.parse()){
            return Optional.empty();
        }

        ////////////////////////////////
        // 2. Build Catalog
        Catalog catalog =  new Catalog(parser.getAttributes());

        return Optional.of(catalog);
    }

}
