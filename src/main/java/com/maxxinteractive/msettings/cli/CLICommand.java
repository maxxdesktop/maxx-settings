package com.maxxinteractive.msettings.cli;

public enum CLICommand {

    INIT("INIT"),
    CREATE("CREATE"),
    UPDATE("UPDATE"),
    LIST("LIST"),
    HASH("HASH"),
    SET("SET"),
    GET("GET"),
    RESET("RESET"),
    UNKNOWN("UNKNOWN");

    private String command ;

    CLICommand(String cmd){
        command = cmd;
    }

    final static char ADMIN_CMD_INIT        = 'I';
    final static char ADMIN_CMD_CREATE      = 'C';
    final static char ADMIN_CMD_UPDATE      = 'U';
    final static char ADMIN_CMD_HASH        = 'H';


    public final boolean isAdmin(){
        if(command == null || command.isEmpty()){
            return false;
        }

        char cmd = command.charAt(0);
        if(cmd == ADMIN_CMD_INIT || cmd == ADMIN_CMD_CREATE || cmd == ADMIN_CMD_UPDATE || cmd == ADMIN_CMD_HASH){
            return true;
        }
        return false;
    }

    public static CLICommand getCommand(String command){
        CLICommand cliCommand = CLICommand.UNKNOWN;

        if(command.equals("INIT")) {
            cliCommand = CLICommand.INIT;
        }

        else if(command.equals("CREATE")) {
            cliCommand = CLICommand.CREATE;
        }

        else if(command.equals("UPDATE")) {
            cliCommand = CLICommand.UPDATE;
        }

        else if(command.equals("H") || command.equals("HASH")) {
            cliCommand = CLICommand.HASH;
        }

        else if(command.equals("L") || command.equals("LIST")) {
            cliCommand = CLICommand.LIST;
        }

        else if(command.equals("S") || command.equals("SET")) {
            cliCommand = CLICommand.SET;
        }

        else if(command.equals("G") || command.equals("GET")) {
            cliCommand = CLICommand.GET;
        }

        if (command.equals("R") || command.equals("RESET")) {
            cliCommand = CLICommand.RESET;
        }

        return cliCommand;
    }
}
