package com.maxxinteractive.msettings.cli;

import com.maxxinteractive.msettings.commons.Logger;
import com.maxxinteractive.msettings.utils.KeyValuePair;

import java.util.ArrayList;
import java.util.List;

/**
 * Context that contains all CLI related information.
 */
public class CLIContext {

    // CMD
    private CLICommand cliCommand = CLICommand.UNKNOWN;

    // modes
    private String mode;
    private boolean adminMode =false;

    public final static String USER_MODE = "msettings";
    public final static String ADMIN_MODE = "ms";

    // options
    private boolean caseSensitive = true;
    private boolean silentModeOff = false;
    private boolean debugMode = false;

    //params
    private boolean force = false;
    private boolean valueOnly = true;
    private boolean keyValue = false;
    private boolean longFormat = false;
    private boolean resolver = false;

    private boolean usingName = true; // default lookup mode, otherwise using uuids

    // List of names, uuid and filenames passed via the command line
    private List<String> uuids = new ArrayList<>(16);
    private List<String> names = new ArrayList<>(16);
    private List<KeyValuePair> keyValuePairs = new ArrayList<>(16);
    private List<String> filenames = new ArrayList<>(16);

    public CLICommand getCliCommand() {
        return cliCommand;
    }

    public void setCliCommand(CLICommand cliCommand) {
        this.cliCommand = cliCommand;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public boolean isAdminMode() {
        return adminMode;
    }

    public void setAdminMode(boolean adminMode) {
        this.adminMode = adminMode;
    }

    public boolean isCaseSensitive() {
        return caseSensitive;
    }

    public void setCaseSensitive(boolean caseSensitive) {
        this.caseSensitive = caseSensitive;
    }

    public boolean isSilentModeOff() {
        return silentModeOff;
    }

    public void setSilentModeOff(boolean silentModeOff) {
        this.silentModeOff = silentModeOff;
        Logger.setSilentMode(!silentModeOff);
    }

    public boolean isDebugMode() {
        return debugMode;
    }

    public void setDebugMode(boolean debugMode) {
        this.debugMode = debugMode;
        Logger.setDebugMode(debugMode);
    }

    public boolean isResolver() {
        return resolver;
    }

    public void setResolver(boolean resolver) {
        this.resolver = resolver;
    }

    public boolean isForce() {
        return force;
    }

    public void setForce(boolean force) {
        this.force = force;
    }

    public boolean isValueOnly() {
        return valueOnly;
    }

    public void setValueOnly(boolean valueOnly) {
        this.valueOnly = valueOnly;
    }

    public boolean isKeyValue() {
        return keyValue;
    }

    public void setKeyValue(boolean keyValue) {
        this.keyValue = keyValue;
    }

    public boolean isLongFormat() {
        return longFormat;
    }

    public void setLongFormat(boolean longFormat) {
        this.longFormat = longFormat;
    }

    public List<String> getUuids() {
        return uuids;
    }

    public void setUuids(List<String> uuids) {
        this.uuids = uuids;
    }

    public List<String> getNames() {
        return names;
    }

    public void setNames(List<String> names) {
        this.names = names;
    }


    public boolean isUsingName() {
        return usingName;
    }

    public void setUsingName(boolean usingName) {
        this.usingName = usingName;
    }

    public List<KeyValuePair> getKeyValuePairs() {
        return keyValuePairs;
    }

    public void setKeyValuePairs(List<KeyValuePair> keyValuePairs) {
        this.keyValuePairs = keyValuePairs;
    }

    public boolean hasFilenames(){
        if(filenames.isEmpty()){
            return false;
        }
        return true;
    }

    public List<String> getFilenames() {
        return filenames;
    }

    public void setFilenames(List<String> filenames) {
        this.filenames = filenames;
    }

}
