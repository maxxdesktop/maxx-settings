package com.maxxinteractive.msettings.cli;

import com.maxxinteractive.msettings.apps.MSettings;
import com.maxxinteractive.msettings.commons.Logger;
import com.maxxinteractive.msettings.utils.FileSystemHelper;
import com.maxxinteractive.msettings.utils.KeyValuePair;
import com.maxxinteractive.msettings.utils.KeyValuePairUtils;
import org.jetbrains.annotations.NotNull;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.StringTokenizer;

public class CLIParser {

    //TODO add constants and refactor logging messages
    final static String ERROR_BAD_CLI      = "Bad CLI syntax, something went wrong. Re-run the command with : -h or --help for assistance.";
    final static String ERROR_BAD_CLI_UUID  = "Bad CLI syntax, something is wrong in the 'uuid' param(s). Re-run the command with : -h or --help for assistance.";
    final static String ERROR_BAD_CLI_NAME  = "Bad CLI syntax, something is wrong in the 'name' param(s). Re-run the command with : -h or --help for assistance.";
    final static String ERROR_BAD_CLI_FILENAME  = "Bad CLI syntax, something is wrong in the 'filename' param(s). Re-run the command with : -h or --help for assistance.";
    final static String ERROR_BAD_CLI_KEY_VALUES  = "Bad CLI syntax, something is wrong in the 'key=value' param(s). Re-run the command with : -h or --help for assistance.";


    final static String OPT_FORCE          = "-F";
    final static String OPT_LONG_FORCE     = "--force";

    final static String OPT_DETAIL          = "-X";
    final static String OPT_LONG_DETAIL     = "--expand-detail";

    final static String OPT_VAL_ONLY         = "-x";
    final static String OPT_LONG_VAL_ONLY    = "--value-only";

    final static String OPT_KEY_VAL         = "-K";
    final static String OPT_LONG_KEY_VAL    = "--key-value";

    final static String OPT_NAME            = "-n";
    final static String OPT_LONG_NAME       = "--name=";

    final static String OPT_UUID            = "-u";
    final static String OPT_LONG_UUID       = "--uuid=";

    final static String OPT_SILENT_OFF      = "-s";
    final static String OPT_LONG_SILENT_OFF = "--silent-mode-off";

    final static String OPT_DEBUG           = "-D";
    final static String OPT_LONG_DEBUG      = "--debug-mode-on";

    final static String OPT_USER            = "-p";
    final static String OPT_LONG_USER       = "--user-preference";

    final static String OPT_RESOLV          = "-R";
    final static String OPT_LONG_RESOLV     = "--resolver";

    final static String OPT_ATTRIBUTES_KEY_VALUE = "-A";
    final static String OPT_FILENAME        = "-f";
    final static String OPT_LONG_FILENAME   = "--filename=";

    //CLI Context
    CLIContext cliContext;

    // processed console CLI params
    private String arguments;
    final private String[] commandLineArgs;

    private boolean hasCommand;

    /**
     * Construct an instance of CLIParser with the command-line String[] args,from main()
     * @param commandLineArgs to use for the parsing
     */
    public CLIParser(String[] commandLineArgs) {
        this.commandLineArgs = commandLineArgs;
        cliContext = new CLIContext();
        loadProperties();
    }

    private void loadProperties(){
        String home = FileSystemHelper.getHome();

        try {

           InputStream output = new FileInputStream(home+"/.msettingsrc");

            Properties prop = new Properties();
            // Load properties to project root folder
            prop.load(output);

            // set the properties value
            cliContext.setDebugMode(getPropValue( "debug-mode-on", false));
            cliContext.setSilentModeOff(getPropValue( "silent-mode-off", false));
            cliContext.setCaseSensitive(getPropValue( "no-case", false));

            cliContext.setLongFormat(getPropValue( "expand-detail", false));
            cliContext.setValueOnly(getPropValue( "value-only", true));
            cliContext.setKeyValue(getPropValue( "key-value", false));

        } catch (IOException  io) {
//            io.printStackTrace();
        }
    }

    private boolean getPropValue(@NotNull String name, boolean defaultValue){
        String home = FileSystemHelper.getHome();

        boolean result = defaultValue;
        try (InputStream output = new FileInputStream(home+"/.msettingsrc")) {

            Properties prop = new Properties();
            // Load properties to project root folder
            prop.load(output);

            String value = prop.getProperty(name,defaultValue?"false":"true");

            if(value.equalsIgnoreCase("true")){
                result = true;
            }
        } catch (IOException io) {
            //io.printStackTrace();
        }

        return result;
    }

    /**
     * Return the Context of the current CLI interaction
     * @return the CLIContext that was built during parsing
     */
    public CLIContext getContext(){
        return cliContext;
    }

    /**
     * Parse the command line arguments
     * @return parsing status, true if successful
     */
    public boolean parseCommandLine() {

        int len = commandLineArgs.length;
        for (int i = 0; i < len; i++) {
            String arg = commandLineArgs[i];
//            System.out.println("Arg: "+ arg);
            if (cliContext.getMode() == null) {
                if (processModes(arg)) {
                    continue;
                }
            }

            if (processOptions(arg)) {
                continue;
            }

            if (!hasCommand) {
                if (getCommand(arg)) {
                    continue;
                }
            }

            //////////////////////////////////////
            //
            // from this point on, it must be params

            // --force  option
            if (OPT_LONG_FORCE.equals(arg)) {
                cliContext.setForce(true);
                continue;
            }

            // -D or --debug-mode  option
            if (OPT_DEBUG.equals(arg) || OPT_LONG_DEBUG.equals(arg)) {
                cliContext.setDebugMode(true);
                continue;
            }

            // -R or --resolver  option
            if (OPT_RESOLV.equals(arg) || OPT_LONG_RESOLV.equals(arg)) {
                cliContext.setResolver(true);
                continue;
            }

            // -S or --silent-mode-off  option
            if (OPT_SILENT_OFF.equals(arg) || OPT_LONG_SILENT_OFF.equals(arg)) {
                cliContext.setSilentModeOff(false);
                continue;
            }

            // -X or --expand-detail  option
            if (OPT_DETAIL.equals(arg) || OPT_LONG_DETAIL.equals(arg)) {
                cliContext.setLongFormat(true);
                cliContext.setValueOnly(false);
                cliContext.setKeyValue(false);
                continue;
            }

            // -x or --value-only  option
            if (OPT_VAL_ONLY.equals(arg) || OPT_LONG_VAL_ONLY.equals(arg)) {
                cliContext.setValueOnly(true);
                cliContext.setLongFormat(false);
                cliContext.setKeyValue(false);
                continue;
            }

            // -K or --key-value  option
            if (OPT_KEY_VAL.equals(arg) || OPT_LONG_KEY_VAL.equals(arg)) {
                cliContext.setValueOnly(false);
                cliContext.setLongFormat(false);
                cliContext.setKeyValue(true);
                continue;
            }

            // --name=
            if (arg.startsWith(OPT_LONG_NAME)) {
                // crop --name
                cliContext.setUsingName(true);
                String acc = accumulateArgument(i);
                arguments = acc.substring(OPT_LONG_NAME.length());
                break;
            }

            // -A ...
            if (OPT_ATTRIBUTES_KEY_VALUE.equals(arg)) {
                cliContext.setUsingName(false);
                String acc = accumulateArgument(i);
                arguments = acc.substring(OPT_ATTRIBUTES_KEY_VALUE.length());
                processKeyValuePairs(arguments);
                break;
            }

            // -n ...
            if (OPT_NAME.equals(arg)) {
                cliContext.setUsingName(false);
                String acc = accumulateArgument(i);
                arguments = acc.substring(OPT_NAME.length());
                processNames(arguments);
                break;
            }

            // -uuid=
            if (arg.startsWith(OPT_LONG_UUID)) {
                cliContext.setUsingName(false);
                String acc = accumulateArgument(i);
                arguments = acc.substring(OPT_LONG_UUID.length());
                break;
            }

            // -u ...
            if (OPT_UUID.equals(arg)) {
                cliContext.setUsingName(false);
                String acc = accumulateArgument(i);
                arguments = acc.substring(OPT_UUID.length());
                processUUIDs(arguments);
                break;
            }

            // -f ...
            if (OPT_FILENAME.equals(arg)) {
                if (cliContext.getCliCommand() == CLICommand.CREATE || cliContext.getCliCommand() == CLICommand.UPDATE) {
                    String acc = accumulateArgument(i);
                    arguments = acc.substring(OPT_FILENAME.length());
                    cliContext.setUsingName(false);
                    processFilename(arguments);
                    break;
                }
            }

            // must be short form, no CMD no param. Just name(s)
            arguments = accumulateArgument(i);
            processNames(arguments);
            break;
        } // iteration over String[] args  end

        if (cliContext.getCliCommand() == CLICommand.UNKNOWN) {
            Logger.error(new Exception("Bad CLI syntax, Re-run the command with : -h or --help for assistance."));
            return false;
        }

        if(Logger.isDebugMode()) {
            StringBuilder builder = new StringBuilder(1024);
            builder.append("Debug mode on :)");
            builder.append("\nCase Sensitive : " + cliContext.isCaseSensitive())
                    .append("\nCMD : " + cliContext.getCliCommand())
                    .append("\nValue Only : " + cliContext.isValueOnly())
                    .append("\nKey Value : " + cliContext.isKeyValue())
                    .append("\nLong Format : " + cliContext.isLongFormat())
                    .append("\nnames=" + cliContext.getNames())
                    .append("\nuuids=" + cliContext.getUuids())
                    .append("\nfilename=" + cliContext.getFilenames());
            Logger.debug(builder.toString());
        }

        // Post Processing based on the CLI Command
        if(cliContext.getCliCommand() == CLICommand.HASH) {
            processNames(arguments);
        }
        return true;
    }

    /**
     * Return a clean, trimmed and whitespace free konkat of arguments from specific index.
     * @param index current Args[] index
     * @return accumulated arguments
     */
    protected String accumulateArgument(int index) {
        StringBuilder accumulator = new StringBuilder();

        if(index == commandLineArgs.length) {
            Logger.error(new Exception(ERROR_BAD_CLI));
            MSettings.terminate(-1);
        }

        for (int t=index; t < commandLineArgs.length; t++){
            accumulator.append(commandLineArgs[t]);
            accumulator.append(" ");
        }
        String result = accumulator.toString().trim();
        result = result.replace(" ", "");
        return result;
    }

    /**
     * Extract Instrument name from the comma separated String and store the result in {@code names}
     * @param arg comma separated list of Instrument name
     */
    private void processNames(@NotNull String arg){
        if(arg.isEmpty()) {
            Logger.error(new Exception(ERROR_BAD_CLI_NAME));
            MSettings.terminate(-1);
        }
        StringTokenizer stk = new StringTokenizer(arg.trim(),",");
        List<String> names = new ArrayList<>(stk.countTokens());
        while(stk.hasMoreElements()){
            names.add( stk.nextToken());
        }
        cliContext.setNames(names);
    }

    private void processFilename(@NotNull String arg){
        if(arg.isEmpty()) {
            Logger.error(new Exception(ERROR_BAD_CLI_FILENAME));
            MSettings.terminate(-1);
        }
        StringTokenizer stk = new StringTokenizer(arg.trim(),",");
        List<String> filenames = new ArrayList<>(stk.countTokens());
        while(stk.hasMoreElements()){
            filenames.add( stk.nextToken());
        }
        cliContext.setFilenames(filenames);
    }

    private void processKeyValuePairs(@NotNull String arg){
        if(arg.isEmpty()) {
            Logger.setSilentMode(false);
            Logger.error(new Exception(ERROR_BAD_CLI_KEY_VALUES));
            MSettings.terminate(-1);
        }
        List<KeyValuePair> keyValuePairs = KeyValuePairUtils.processKeyValuePairs(arg);
        cliContext.setKeyValuePairs(keyValuePairs);
    }

    /**
     * Extract Instrument UUID from the comma separated String and store the result in {@code uuids}
     * @param arg comma separated list of Instrument UUID
     */
    private void processUUIDs(@NotNull String arg){
        if(arg.isEmpty()) {
            Logger.setSilentMode(false);
            Logger.error(new Exception(ERROR_BAD_CLI_UUID));
            MSettings.terminate(-1);
        }
        StringTokenizer stk = new StringTokenizer(arg.trim(),",");
        List<String> uuids = new ArrayList<>(stk.countTokens());
        while(stk.hasMoreElements()){
            uuids.add(stk.nextToken());
        }
        cliContext.setUuids(uuids);
    }

    /**
     * Extract CLI Command
     * @param arg ingress info
     * @return result of the processing. True if CLI Command was found, or carry on in the parsing.
     */
    protected boolean getCommand(String arg) {
        if(hasCommand) {
            return true;
        }

        CLICommand cli = CLICommand.getCommand(arg);
        if(cli == CLICommand.UNKNOWN ){
            // we are in export mode and GET is defaulted, unless another CMD is provided
            if(cliContext.isAdminMode()){
                return false;
            }
        }else {
            cliContext.setCliCommand(cli);
            hasCommand = true;
            return true;
        }
        return false;
    }

    /**
     * Extract options
     * @param arg ingress info
     * @return result of the processing. True if success and can carry on in the parsing.
     */
    protected boolean processOptions (String arg){
        if(arg.equals("-h") || arg.equals("--help")){
            MSettings.printVersion();
            printUsage();
            MSettings.terminate(0);
        }
        if(arg.equals("-v") || arg.equals("--version")){
            MSettings.printVersion();
            MSettings.terminate(0);
        }
        if(arg.equals("-i") || arg.equals("--no-case")){
            cliContext.setCaseSensitive(false);
            return true;
        }
        return false;
    }

    /**
     * Extract Mode - Admin or User mode
     * @param arg ingress info
     * @return result of the processing. True if success and can carry on in the parsing.
     */
    protected boolean processModes(String arg){
        if(CLIContext.USER_MODE.equals(arg)){
            cliContext.setAdminMode(false);
            cliContext.setMode(arg);

            // default GET
            cliContext.setCliCommand(CLICommand.GET);
            return true;
        }
        if(CLIContext.ADMIN_MODE.equals(arg)){
            cliContext.setAdminMode(true);
            cliContext.setMode(arg);
            return true;
        }
        Logger.error(new Exception("Bad CLI syntax, missing MODE."));
        MSettings.terminate(-1);

        return false;
    }

    final String  usage = ""+
            "CLI Options\n\r"+
            "    -h,          --help                Print the help information.. \n\r" +
            "    -v,          --version             Print the version information.\n\r"+
            "    -D,          --debug-mode-on       Enable debug mode. Extra DEBUG information will be printed out in the console.\n\r"+
            "    -s,          --silent-mode-off     Turn OFF silent mode. This allows normal verbose outputs in the console. \n\r"+
            "    -i,          --no-case             Disable case sensitivity when searching by name.\n\r" +
            "    -F,          --force               Force the initialization over an existing MaXX Settings environment. This will erase everything.\n\r" +

            "Administrative Commands\n\r" +
            "     INIT                  Initialize MaXX Settings Database, indexes and storage. Can be used with --force param. Use with caution.\n\r" +
            "     CREATE                Create a new System wide Instrument.\n\r"+
            "     HASH                  Generate a Hashed Storage Location for the given Instrument name.\n\r"+
            "     UPDATE                Update a System Wide Instrument Schema definition.\n\r" +
            "     LIST                  List all Installed Instruments.\n\r" +

            "Standard Commands\n\r" +
            "     S,          SET       Set User Preference Instrument value. \n\r" +
            "     G,          GET       Get User Preference Instrument value.\n\r"+
            "     R,          RESET     Reset User Preference Instrument value with its default value.\n\r" +
            "     L,          LIST      List all Installed Instruments.\n\r" +
            "     H,          HASH      Generate a Hashed Storage Location for the given Instrument name.\n\r"+

            "Parameters\n\r" +
            "    -u UUID      --uuid=UUID           Single Instrument UUID and its values\n\r" +
            "    -u UUID,UUID,...                   Comma-separated list of Instrument UUIDs.\n\r" +
            "    -n name      --name=NAME           Single Instrument name and its values.\n\r" +
            "    -n name,name,...                   Comma-separated list of Instrument names.\n\r" +
            "    -f filename  --filename=FILENAME   Single filename.\n\r" +
            "    -f filename,filename,...           Comma-separated list of filenames.\n\r" +
            "    -F key=value,key=value,...         Comma-separated list of key=value pair to pass to the CREATE Command.\n\r" +
            "    -X,          --expand-detail       Detailed output format, print all attributes in Key=Value pair for every requested Instrument.\n\r" +
            "    -x,          --value-only          Output only the Value for every requested Instrument. This is the DEFAULT settings.\n\r" +
            "    -K,          --key-value           Output in key=value format for every requested Instrument.\n\r" +
            "    -R,          --resolver            Resolve value attribute before output. Resolver allow the resolution of Choice option\n\r"+
            "                                       index into text and/or to lookup and resolve any reference to another Instrument's name with the '@' prefix as its attribute's value.\n\r";

    public void printUsage() {
        System.out.println(usage);
    }
}
