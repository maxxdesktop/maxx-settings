package com.maxxinteractive.msettings.cli;

import org.jetbrains.annotations.NotNull;

public interface ICLICommand {
    /**
     * Run a CLI Command and orchestrate the various phase of the processing
     * @return true if successful or false if failure
     */
    boolean runCommand();

    boolean isContinueOnFailure();
    void setContinueOnFailure(boolean value);

    boolean doInstrumentByName(@NotNull String name);
    boolean doInstrumentByUUID(@NotNull String name);


}
