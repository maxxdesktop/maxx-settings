package com.maxxinteractive.msettings.cli.commands;

import com.maxxinteractive.msettings.preferences.IUserPreference;
import com.maxxinteractive.msettings.utils.KeyValuePair;

public class CLIUserPreference {
    private IUserPreference instrument = null;
    private KeyValuePair userPref = null;
    private String chunk  = null;
    private String hash  = null;
    private String schemaFile  = null;

    public CLIUserPreference(String chunk, String hash, String schemaFile) {
        this.chunk = chunk;
        this.hash = hash;
        this.schemaFile = schemaFile;
    }

    public CLIUserPreference(IUserPreference instrument, KeyValuePair userPref) {
        this.instrument = instrument;
        this.userPref = userPref;
    }

    public CLIUserPreference(String hash, String schemaFile) {
        this.hash = hash;
        this.schemaFile = schemaFile;
    }

    public IUserPreference getInstrument() {
        return instrument;
    }

    public void setInstrument(IUserPreference instrument) {
        this.instrument = instrument;
    }

    public String getChunk() {
        return chunk;
    }

    public void setChunk(String chunk) {
        this.chunk = chunk;
    }

    public KeyValuePair getUserPref() {
        return userPref;
    }

    public void setUserPref(KeyValuePair userPref) {
        this.userPref = userPref;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getSchemaFile() {
        return schemaFile;
    }

    public void setSchemaFile(String schemaFile) {
        this.schemaFile = schemaFile;
    }
}
