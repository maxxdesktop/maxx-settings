package com.maxxinteractive.msettings.cli.commands;

import com.maxxinteractive.msettings.attributes.IAttribute;
import com.maxxinteractive.msettings.cli.AbstractCommand;
import com.maxxinteractive.msettings.cli.CLIContext;
import com.maxxinteractive.msettings.commons.Logger;
import com.maxxinteractive.msettings.database.Database;
import com.maxxinteractive.msettings.database.IDatabase;
import com.maxxinteractive.msettings.database.IndexEntry;
import com.maxxinteractive.msettings.instruments.InstrumentBuilder;
import com.maxxinteractive.msettings.instruments.InstrumentParser;
import com.maxxinteractive.msettings.preferences.IUserPreference;
import com.maxxinteractive.msettings.utils.AttributeMapUtils;
import com.maxxinteractive.msettings.utils.FileSystemInstrumentHelper;
import com.maxxinteractive.msettings.utils.HasherUtility;
import org.jetbrains.annotations.NotNull;

import java.util.Map;
import java.util.Optional;

/**
 * Create System Wide Instrument.<br>
 */

public class CreateInstrument extends AbstractCommand{

    public CreateInstrument(CLIContext context) {
        super(context);
    }

    @Override
    public boolean runCommand() {
        if(!cliContext.isAdminMode()){
            Logger.error(new Exception("Instrument Creation not allowed in User Preference Mode."));
            return false;
        }

        if(cliContext.hasFilenames()){
            for(String filename : cliContext.getFilenames()){

                InstrumentParser parser = new InstrumentParser(filename);
                if(!parser.parse()){
                    return false;
                }
                InstrumentBuilder builder = new InstrumentBuilder(parser.getAttributes());
                Optional<IUserPreference> iUserPreference = builder.build();
                if(!iUserPreference.isPresent()){
                    return false;
                }

               boolean status = cliCreateInstrument(iUserPreference.get());
               if(!status) {
                   Logger.error(new Exception("Bad data format for : "+ filename));
                   if(!continueOnFailure) {
                       return false;
                   }
                }
            }
        } else if(!cliContext.getKeyValuePairs().isEmpty()) {
            Optional<Map<String, IAttribute>> attributesOpt = AttributeMapUtils.convert(cliContext.getKeyValuePairs());
            if(!attributesOpt.isPresent()){
                return false;
            }
            InstrumentBuilder builder = new InstrumentBuilder(attributesOpt.get());
            Optional<IUserPreference> iUserPreference = builder.build();
            if(!iUserPreference.isPresent()){
                return false;
            }
            return cliCreateInstrument(iUserPreference.get());
        } else {
            Logger.error(new Exception("Bad CREATE CLI format. Missing -f or -A with params."));
            return false;
        }
        return true;
    }

    /**
     * Create System Wide Instrument.<br>
     * ms CREATE -f ./DesktopMouse_Acceleration.Gauge<br>
     * ms CREATE -A stereotype=Choice,name=Desktop.FileManager.IconSortBy,type=Chars,default=1,option[0]=Name,option[1]=Date<br>
     * @param instrument input
     */
    private boolean cliCreateInstrument(@NotNull IUserPreference instrument ){

        IDatabase db = Database.getInstance();
        if(!db.open()) {
            Logger.error(new Exception("No Database available, operation is cancelled."));
            return false;
        }
        // get content
        String content = instrument.serialize();

        // check if already exists, if not go ahead
        String hashDir = HasherUtility.generateDirectoryHash(instrument.getName().toLowerCase());
        String schemaFile = instrument.getFilename();
        String lookup = (hashDir+schemaFile);
        if(db.lookup(lookup)){
            Logger.setSilentMode(false);
            Logger.warning(String.format("Instrument '%s' is already present, aborting operation.", instrument.getName()));
            db.closeDB();
            return false;
        }else {
            // create Instrument's location storage and write schema file
            boolean outcome = FileSystemInstrumentHelper.writeInstrument(hashDir,schemaFile, content);
            if(outcome) {
                // add IndexEntry
                outcome = db.put(new IndexEntry(instrument.getUUID(), hashDir, schemaFile));
                db.closeDB();
                Logger.info(String.format("Successfully added Instrument '%s' w/ Hashed Directory=%s.", instrument.getName(), hashDir));
                Logger.debug(String.format("Instrument's content\n%s\nEOF", content));
            }
        }
        return true;
    }

    @Override
    public boolean doInstrumentByName(@NotNull String name) {
        return false;
    }

    @Override
    public boolean doInstrumentByUUID(@NotNull String name) {
        return false;
    }
}
