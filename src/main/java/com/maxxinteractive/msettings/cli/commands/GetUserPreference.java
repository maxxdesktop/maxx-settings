package com.maxxinteractive.msettings.cli.commands;

import com.maxxinteractive.msettings.cli.AbstractCommand;
import com.maxxinteractive.msettings.cli.CLIContext;
import com.maxxinteractive.msettings.commons.Logger;
import com.maxxinteractive.msettings.preferences.IUserPreference;
import com.maxxinteractive.msettings.utils.HasherUtility;
import com.maxxinteractive.msettings.utils.KeyValuePair;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;
public class GetUserPreference extends AbstractCommand {

    public GetUserPreference(CLIContext context) {
        super(context);
    }

    /**
     * Get User Preference value by Instrument Name<br>
     * msettings GET -i -n desktop.mouse.lefthanded <br>
     * msettings GET -X -n Desktop.Mouse.Acceleration <br>
     * @param name instrument name
     */
    public boolean doInstrumentByName(@NotNull String name) {

        String schema = null;
        String hash = null;
        String schemaFile;

        ////////////////////////////////
        // 1. Get Schema name
        Optional<String> schemaOpt = getSchema(name);
        if (!schemaOpt.isPresent()){
            return false;
        }
        schema = schemaOpt.get();

        ////////////////////////////////
        // 2. Calc hash based on Instrument name + toLowerCase()
        hash = HasherUtility.generateDirectoryHash(name.toLowerCase());

        ////////////////////////////////
        // 3. Lookup for matching record and extract schemaFile
        Optional<String> chunk = fetchRecordByHashAndName(hash,schema);
        if(!chunk.isPresent()) {
            return false;
        }

        // extract schemaFile then load the InstrumentFile = chunk.substring (UUID_LENGTH+PATH_LENGTH).trim();
        schemaFile = getSchemaFromChunk(chunk.get());
        if(schemaFile.isEmpty()){
            Logger.error(new Exception("Unable to extract required information from the input parameters."));
            return false;

        }

        // Fetch Instrument/UserPreference data
        Optional<CLIUserPreference> cliUserPreferenceOpt = doFetch(hash,schemaFile);
        if(!cliUserPreferenceOpt.isPresent()){
            Logger.error(new Exception("Failed to retrieve Instrument/UserPreference data."));
            return false;
        }

        // Do print
        doPrint(cliUserPreferenceOpt.get());

        return true;
    }

    /**
     * Get User Preference value by Instrument UUID<br>
     * msettings GET -X -u 8f6e1638-91fe-4eae-9876-45a4e6686d74<br>
     * @param uuid instrument uuid
     */
    public boolean doInstrumentByUUID(@NotNull String uuid) {

        String schema = null;
        String hash = null;
        String schemaFile;

        ////////////////////////////////
        // 1.Lookup for matching record and  Read System wide schema
        Optional<String> chunk = fetchRecordByUUID(uuid);
        if(!chunk.isPresent()){
            Logger.error(new Exception("Unable to lookup record information from the input parameters."));
            return false;
        }

        // build Index record from loaded chunk record
        hash = getHashFromChunk(chunk.get());

        // extract schemaFile from chunk record
        schemaFile = getSchemaFromChunk(chunk.get());
        if(schemaFile.isEmpty()){
            Logger.error(new Exception("Unable to extract required information from the input parameters."));
            return false;
        }

        // extract schemaFile then load the InstrumentFile = chunk.substring (UUID_LENGTH+PATH_LENGTH).trim();
        schemaFile = getSchemaFromChunk(chunk.get());
        if(schemaFile.isEmpty()){
            Logger.error(new Exception("Unable to extract required information from the input parameters."));
            return false;

        }

        // Fetch Instrument/UserPreference data
        Optional<CLIUserPreference> cliUserPreferenceOpt = doFetch(hash,schemaFile);
        if(!cliUserPreferenceOpt.isPresent()){
            Logger.error(new Exception("Failed to retrieve Instrument/UserPreference data."));
            return false;
        }

        // Do print
        doPrint(cliUserPreferenceOpt.get());

        return true;
    }

    /**
     * Common Logic that and Prints out the value using different format
     * @param cliUserPreference to use for the operation
     * @return status of the operation
     */
    protected boolean doPrint(@NotNull CLIUserPreference cliUserPreference) {

        IUserPreference instrument = cliUserPreference.getInstrument();
        KeyValuePair userPref = cliUserPreference.getUserPref();

        ////////////////////////////////
        // 1. Output information
        if (cliContext.isLongFormat()) {
            System.out.println(instrument.serialize());
            System.out.println(userPref.serialize());
        } else{
            if (cliContext.isKeyValue()) {
            System.out.println(userPref.serialize());
            } else if (cliContext.isValueOnly()) {
                // print value only
                System.out.println(userPref.getValue());
            }
        }
        return true;
    }
}