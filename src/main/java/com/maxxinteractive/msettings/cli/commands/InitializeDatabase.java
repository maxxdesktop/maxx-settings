package com.maxxinteractive.msettings.cli.commands;

import com.maxxinteractive.msettings.cli.AbstractCommand;
import com.maxxinteractive.msettings.cli.CLIContext;
import com.maxxinteractive.msettings.commons.Logger;
import com.maxxinteractive.msettings.database.Database;
import com.maxxinteractive.msettings.database.IDatabase;
import com.maxxinteractive.msettings.utils.FileSystemHelper;
import org.jetbrains.annotations.NotNull;

import static com.maxxinteractive.msettings.Constants.*;

/**
 * Init MaXX Settings System Wide DB
 */
public class InitializeDatabase extends AbstractCommand {

    public InitializeDatabase(CLIContext context) {
        super(context);
    }

    @Override
    public boolean runCommand() {
        if(!cliContext.isAdminMode()){
            Logger.setSilentMode(false);
            Logger.warning("Database Creation not allowed in Standard mode.");
            return false;
        }

        IDatabase db = Database.getInstance();
        Logger.info("Initializing Database and Metadata...");
        db.createDB(cliContext.isForce());

        // Create System wide or User Preferences supporting folders
        FileSystemHelper.createDirectory(MSETTINGS_INSTRUMENTS_HOME);
        FileSystemHelper.createDirectory(MSETTINGS_HOME_SYSTEM + "/" + FILETYPES);
        return true;
    }

    @Override
    public boolean doInstrumentByName(@NotNull String name) {
        return false;
    }

    @Override
    public boolean doInstrumentByUUID(@NotNull String name) {
        return false;
    }
}