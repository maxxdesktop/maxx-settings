package com.maxxinteractive.msettings.cli.commands;

import com.maxxinteractive.msettings.cli.AbstractCommand;
import com.maxxinteractive.msettings.cli.CLIContext;
import com.maxxinteractive.msettings.commons.Logger;
import com.maxxinteractive.msettings.database.Database;
import com.maxxinteractive.msettings.database.IDatabase;
import com.maxxinteractive.msettings.database.IndexEntry;
import com.maxxinteractive.msettings.utils.FileSystemInstrumentHelper;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.maxxinteractive.msettings.Constants.*;

/**
 * List installed Instruments
 */
public class ListInstrument extends AbstractCommand {

    public ListInstrument(CLIContext context) {
        super(context);
    }

    @Override
    public boolean runCommand() {
        IDatabase db = Database.getInstance();
        if (!db.open()) {
            Logger.error(new Exception("Unable to open the Database."));
            return false;
        }
        int count  = db.count();
        System.out.println(("Total Installed Instruments : "+count));
        Logger.debug(("Total Installed Instruments : "+count));
        List<IndexEntry> entries = db.fetchAll();
        List<String> list = new ArrayList<>(count);
        for(IndexEntry entry : entries){
            Optional<String> instrumentName = FileSystemInstrumentHelper.loadRawInstrument(entry.getPath(),entry.getSchema());
            if (!instrumentName.isPresent()) {
                Logger.warning("Bad DB record, instrument not found. ["+ entry.toString()+"]");
                return false;
            }
            Optional <String> name = FileSystemInstrumentHelper.getAttributeValueFromRawFile(instrumentName.get(),ATTRIBUTE_NAME);
            Optional <String> defaultValue = FileSystemInstrumentHelper.getAttributeValueFromRawFile(instrumentName.get(),ATTRIBUTE_DEFAULT);

            String line = name.get();
            if(cliContext.isKeyValue()){
                line += "="+defaultValue.get();
            }
            list.add(line);
        }
        // sort
        Collections.sort(list);
        for(String tmp : list) {
            System.out.println(tmp);
        }

        return true;
    }

    @Override
    public boolean doInstrumentByName(@NotNull String name) {
        return false;
    }

    @Override
    public boolean doInstrumentByUUID(@NotNull String name) {
        return false;
    }
}