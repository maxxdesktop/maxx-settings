package com.maxxinteractive.msettings.cli.commands;

import com.maxxinteractive.msettings.cli.AbstractCommand;
import com.maxxinteractive.msettings.cli.CLIContext;
import com.maxxinteractive.msettings.commons.Logger;
import com.maxxinteractive.msettings.utils.FileSystemInstrumentHelper;
import com.maxxinteractive.msettings.utils.HasherUtility;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

public class ResetUserPreference extends AbstractCommand {

    public ResetUserPreference(CLIContext context) {
        super(context);
    }

    /**
     * Reset UserPreference value by Instrument Name
     * @param name Instrument
     * msettings RESET Desktop.Mouse.LeftHanded <br>
     */
    public boolean doInstrumentByName(@NotNull String name) {
        String hash = null;
        String schema = null;
        String schemaFile = null;
        String key;
        String value;

        ////////////////////////////////
        // 1. Get Schema name
        Optional<String> schemaOpt = getSchema(name);
        if (!schemaOpt.isPresent()){
            return false;
        }
        schema = schemaOpt.get();

        ////////////////////////////////
        // 2. Calc hash based on Instrument name + toLowerCase()
        hash = HasherUtility.generateDirectoryHash(name.toLowerCase());

        ////////////////////////////////
        // 3. Lookup for matching record and extract schemaFile
        Optional<String> chunk = fetchRecordByHashAndName(hash,schema);
        if(!chunk.isPresent()) {
            return false;
        }

        // extract schemaFile then load the InstrumentFile = chunk.substring (UUID_LENGTH+PATH_LENGTH).trim();
        schemaFile = getSchemaFromChunk(chunk.get());
        if(schemaFile.isEmpty()){
            Logger.error(new Exception("Unable to extract required information from the input parameters, bailing out."));
            return false;
        }

        return doFetchAndReset(name,hash,schemaFile);
    }

    /**
     * Reset UserPreference value by Instrument BY UUID
     * msettings RESET 8f6e1638-91fe-4eae-9876-45a4e6686d74<br>
     * @param uuid Instrument's uuid
     */
    public boolean doInstrumentByUUID(@NotNull String uuid) {
        String hash = null;
        String schemaFile = null;

        ////////////////////////////////
        // 1. Lookup for matching record and  Read System wide schema
        Optional<String> chunk = fetchRecordByUUID(uuid);
        if(!chunk.isPresent()){
            Logger.error(new Exception("Unable to lookup record information from the input parameters, bailing out."));
            return false;
        }

        // build Index record from loaded chunk record
        hash = getHashFromChunk(chunk.get());

        // extract schemaFile from chunk record
        schemaFile = getSchemaFromChunk(chunk.get());
        if(schemaFile.isEmpty()){
            Logger.error(new Exception("Unable to extract required information from the input parameters, bailing out."));
            return false;
        }
        return doFetchAndReset(uuid,hash,schemaFile);
    }

    /**
     * Common Logic that Fetches UserPreference and Resets to the default value
     * @param key key field name
     * @param hash to use for the operation
     * @param schemaFile to use for the operation
     * @return status of the operation
     */
    protected boolean doFetchAndReset(@NotNull String key, @NotNull String hash, @NotNull String schemaFile) {

        ////////////////////////////////
        // 1. Fetch System wide Instrument
        Optional<String> defaultValue = FileSystemInstrumentHelper.getDefaultValue(hash,schemaFile);
        if(!defaultValue.isPresent()){
            Logger.error(new Exception("RESET failed, unable to locate Instrument's"));
            return false;
        }

        ////////////////////////////////
        // 2. Reset UserPreference Instrument
        FileSystemInstrumentHelper.writeUserPreference(hash,schemaFile,defaultValue.get());
        System.out.printf("%s=%s\n", key,defaultValue.get());

        return true;
    }
}
