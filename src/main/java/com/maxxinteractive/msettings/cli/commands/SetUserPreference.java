package com.maxxinteractive.msettings.cli.commands;

import com.maxxinteractive.msettings.attributes.values.AttributeValueBuilder;
import com.maxxinteractive.msettings.attributes.values.IAttributeValue;
import com.maxxinteractive.msettings.cli.AbstractCommand;
import com.maxxinteractive.msettings.cli.CLIContext;
import com.maxxinteractive.msettings.commons.Logger;
import com.maxxinteractive.msettings.preferences.IUserPreference;
import com.maxxinteractive.msettings.utils.FileSystemInstrumentHelper;
import com.maxxinteractive.msettings.utils.HasherUtility;
import com.maxxinteractive.msettings.utils.KeyValuePairUtils;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

public class SetUserPreference extends AbstractCommand {

    public SetUserPreference(CLIContext context) {
        super(context);
    }

    @Override
    public boolean runCommand() {

        if(cliContext.isUsingName()) {
            for(String name : cliContext.getNames()){
                boolean status = cliSetInstrumentName(name);
                if(!status && !continueOnFailure){
                    return false;
                }
            }
        }else  {
            for(String uuid : cliContext.getUuids()){
                boolean status = cliSetInstrumentUUID(uuid);
                if(!status && !continueOnFailure){
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Set User Preference Instrument<br>
     * msettings SET -n Desktop.Mouse.Acceleration=True,Desktop.Mouse.Threshold=7<br>
     * @param name instrument name
     */
    private boolean cliSetInstrumentName(@NotNull String name) {
        String hash = null;
        String schema = null;
        String schemaFile = null;
        Optional<String> key;
        Optional<String> value;

        ////////////////////////////////
        // 1. Parse key and value : 'key' is the Instrument's name and 'value' is the value to Set as User Preference
        key = KeyValuePairUtils.extractKey(name);
        if (!key.isPresent()) {
            Logger.error(new Exception("Unable to extract Instrument's 'name' from input parameter."));
            return false;
        }
        value = KeyValuePairUtils.extractValue(name);
        if (!value.isPresent()) {
            Logger.error(new Exception("Unable to extract Instrument's 'value' from input parameter."));
            return false;
        }

        ////////////////////////////////
        // 2. Get Schema name
        Optional<String> schemaOpt = getSchema(key.get());
        if (!schemaOpt.isPresent()){
            return false;
        }
        schema = schemaOpt.get();

        ////////////////////////////////
        // 3. Calc hash based on Instrument name + toLowerCase()
        hash = HasherUtility.generateDirectoryHash(key.get().toLowerCase());

        ////////////////////////////////
        // 4. Lookup for matching record and extract schemaFile
        Optional<String> chunk = fetchRecordByHashAndName(hash, schema);
        if (!chunk.isPresent()) {
            return false;
        }

        // extract schemaFile then load the InstrumentFile = chunk.substring (UUID_LENGTH+PATH_LENGTH).trim();
        schemaFile = getSchemaFromChunk(chunk.get());
        if (schemaFile.isEmpty()) {
            Logger.error(new Exception("Unable to extract required information from the input parameters."));
            return false;
        }

        return doSetValueAndSave(hash, schemaFile, key.get(),value.get());
    }


    /**
     * Set User Preference Instrument<br>
     *  msettings SET -u 8f6e1638-91fe-4eae-9876-45a4e6686d74=True<br>
     *  @param uuid to use
     */
    private boolean cliSetInstrumentUUID(@NotNull String uuid) {
        String hash = null;
        String schemaFile = null;
        Optional<String> key;
        Optional<String> value;

        ////////////////////////////////
        // 1. Extract schema name and parse key and value

        // parse key and value : key is instrument uuid and value is the value to Set as user preference
        key = KeyValuePairUtils.extractKey(uuid);
        if (!key.isPresent()) {
            Logger.error(new Exception("Unable to extract Instrument's UUID from input parameter."));
            return false;
        }
        value = KeyValuePairUtils.extractValue(uuid);
        if (!value.isPresent()) {
            Logger.error(new Exception("Unable to extract the 'value' attribute from the input parameters."));
            return false;
        }

        ////////////////////////////////
        // 2. Lookup for matching record and  Read System wide schema

        Optional<String> chunk = fetchRecordByUUID(uuid);
        if (!chunk.isPresent()) {
            Logger.error(new Exception("Unable to lookup record information from the input parameters, bailing out."));
            return false;
        }

        // build Index record from loaded chunk record
        hash = getHashFromChunk(chunk.get());

        // extract schemaFile from chunk record
        schemaFile = getSchemaFromChunk(chunk.get());
        if (schemaFile.isEmpty()) {
            Logger.error(new Exception("Unable to extract required information from the input parameters, bailing out."));
            return false;
        }

        return doSetValueAndSave(hash, schemaFile, key.get(), value.get());
    }

    /**
     * Common logic that Fetch Instrument, runs validation and persits UserPreference
     * @param hash to use for the operation
     * @param schemaFile to use for the operation
     * @param key name or uuid
     * @param value to persist
     * @return status of the operation
     */
    protected boolean doSetValueAndSave(@NotNull String hash, @NotNull String schemaFile, @NotNull String key, @NotNull String value ) {

        ////////////////////////////////
        // 1. Run schema validation while attempting to fetch an instance of the Instrument and set its value.
        Optional<IUserPreference> instrument = fetchInstrument(hash,schemaFile);
        if (!instrument.isPresent()) {
            return false;
        }

        IAttributeValue attribute = AttributeValueBuilder.builtWithStringValue(value, instrument.get().getStereotype());
        if(attribute == null){
            Logger.error(new Exception("Oops, bad value.  Dead in the water :("));
            return false;
        }

        ////////////////////////////////
        // 2. Persist User Preference new value
        instrument.get().setValue(attribute);
        FileSystemInstrumentHelper.writeUserPreference(hash, schemaFile, instrument.get().getValue().getValueAsString());

        System.out.println(instrument.get().getName()+"="+instrument.get().getValue().getValueAsString());

        Logger.debug(String.format("Wrote User Preference key=[%s] with value=[%s] hashdir=[%s] schema=[%s]",  key, attribute.getValueAsString(), hash, schemaFile));
        return true;
    }


    @Override
    public boolean doInstrumentByName(@NotNull String name) {
        return false;
    }

    @Override
    public boolean doInstrumentByUUID(@NotNull String name) {
        return false;
    }
}
