package com.maxxinteractive.msettings.commons;

import com.maxxinteractive.msettings.utils.FileSystemHelper;
import com.maxxinteractive.msettings.utils.UUIDGenerator;
import org.jetbrains.annotations.NotNull;

import java.time.LocalDateTime;
import java.util.Random;
import java.util.UUID;

public class Logger {

    public final static String DEBUG = "DEBUG";
    public final static String INFO  = "INFO";
    public final static String WARN  = "WARN";
    public final static String ERROR = "ERROR";

    public final static String LOGFILE = ".msettings.log";

    final static String INFO_MSG = "%s - (%d) %s [%s] %s\n";  // LEVEL [THREAD]: MESSAGE
    final static String ERROR_MSG  = "%s - (%d) %s [%s] ->%s(): %s \n";  // TIMESTAMP - LEVEL [THREAD] CLASS->(METHOD): MESSAGE | ERROR

    // Silent mode allow to use the STDOUT to have a more verbose experience
    private static boolean silentMode = true;
    // Debug mode outputs lots more information into the log file
    private static boolean debugMode = false;

    public static final int SESION_ID = new Random().nextInt(1000000);

    public static boolean isSilentMode() {
        return silentMode;
    }

    public static void setSilentMode(boolean silentMode) {
        Logger.silentMode = silentMode;
    }

    public static boolean isDebugMode() {
        return debugMode;
    }

    public static void setDebugMode(boolean debugMode) {
        Logger.debugMode = debugMode;
    }

    /**
     * Return an ISO 8601 formatted timestamp of now
     * @return formatter now in ISO 8601
     */
    private static String getTimestamp() {
        return LocalDateTime.now().toString();
    }

    public static void debug(@NotNull String message){
        if(debugMode) {
            log(String.format(INFO_MSG, getTimestamp(), SESION_ID, DEBUG, Thread.currentThread().getName(), message));
        }
    }

    public static void info(@NotNull String message){
        log(String.format(INFO_MSG, getTimestamp(), SESION_ID, INFO, Thread.currentThread().getName(), message));
    }

    public static void warning(@NotNull String message){
        log(String.format(INFO_MSG, getTimestamp(), SESION_ID, WARN,Thread.currentThread().getName(), message));
    }

    public static void error(@NotNull Throwable error){
        String className = error.getStackTrace()[0].getClassName();
        String methodName = error.getStackTrace()[0].getMethodName();
        log(String.format(ERROR_MSG, getTimestamp(), SESION_ID, ERROR,Thread.currentThread().getName(), className+"."+methodName,  error.getMessage()));
    }

    public static void error(@NotNull String message, @NotNull Throwable error){

        String methodName = error.getStackTrace()[0].getMethodName();
        String className = error.getStackTrace()[0].getClassName();
        log(String.format(ERROR_MSG, getTimestamp(), SESION_ID, ERROR, Thread.currentThread().getName(), className + "." + methodName, message + " | " + error.getMessage()));
    }

    private static void log(@NotNull String output){
        if(!silentMode) {
            System.out.println(output);
        }
        FileSystemHelper.appendToFile(FileSystemHelper.getHome(), LOGFILE,output);
    }
}
