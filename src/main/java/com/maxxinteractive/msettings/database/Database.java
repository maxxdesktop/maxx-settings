package com.maxxinteractive.msettings.database;

import com.maxxinteractive.msettings.commons.Logger;
import com.maxxinteractive.msettings.utils.FileSystemHelper;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * MaXX Settings System wide Database implementation
 *  @author Eric Masson
 *  @version 1.0
 */
public class Database implements  IDatabase{

    public static final String DEFAULT_DB_DIR = "/Database";
    public static final String DEFAULT_DB_FILENAME = "msettings.db";

    private String maxxSettingsHome;
    private String databaseHome;
    private String filename;
    private Path databaseFullPath;

    private boolean opened = false;
    private String content = null;

    private Integer mutex = new Integer(1168);

    private static IDatabase itself = null;

    /**
     * Singleton implementation - getInstance
     * @return the unique instance of IDatabase.
     */

    public static IDatabase getInstance(){
        if (itself == null){
            itself = new Database();
        }
        return itself;
    }

    /**
     * Create a new instance of Database at the specified location
     */
    protected Database() {
        this.maxxSettingsHome = com.maxxinteractive.msettings.Constants.MSETTINGS_HOME_SYSTEM;
        this.filename = Database.DEFAULT_DB_FILENAME;
        this.databaseHome = this.maxxSettingsHome + DEFAULT_DB_DIR;
        this.databaseFullPath = Paths.get(databaseHome +"/"+filename);
    }

    public Path getDatabaseFullPath() {
        return databaseFullPath;
    }

    /**
     * Create a new System wide Database
     * @param forceCreation overwrite existing database, if present.  Use this option with caution
     * @return true if successful
     */
    public boolean createDB(boolean forceCreation){
        // force console output
        Logger.setSilentMode(false);

        // Create regardless if present or not the directory path
        if(! FileSystemHelper.createDirectory(databaseHome)){
            return false;
        }
        try {
            if(forceCreation){
                // Force CREATE, first let's dump everything...
                if(databaseFullPath.toFile().exists()){
                    Logger.warning("Database file already exist. Will reinitialize and overwrite it.");
                    databaseFullPath.toFile().delete();

                    FileSystemHelper.deleteDirectory(Paths.get( maxxSettingsHome+"/Choices"));
                    FileSystemHelper.deleteDirectory(Paths.get( maxxSettingsHome+"/Instruments"));
                    FileSystemHelper.deleteDirectory(Paths.get( maxxSettingsHome+"/FileTypes"));
                }
            }else {
                if(databaseFullPath.toFile().exists()){
                    Logger.warning("Database file already exist.");
                    return false;
                }
            }
            // Create the directory path for the Database, then the file itself
            FileSystemHelper.createDirectory(databaseHome);
            Files.createFile(databaseFullPath);
            Logger.info("Database successfully created in :"+ databaseHome);
        } catch (IOException ioe) {
            Logger.error(ioe);
            return false;
        }
        return true;
    }

    public boolean open(){
        if(!opened){
            if(databaseFullPath.toFile().canRead() ){
                content = loadDB(databaseFullPath);
                Logger.debug("Database is Ready. Using "+  databaseFullPath.toAbsolutePath());
                opened = true;
            }else {
                Logger.error(new Exception("No Database available."));
                return false;
            }
        }
        return true;
    }

    private String loadDB(@NotNull Path databaseFile) {
        Optional<String> record = FileSystemHelper.fastRead(databaseFullPath);
        if(record.isPresent()) {
            content = record.get();
            Logger.debug(String.format("Database loaded with %d bytes in memory.", content.length()));
        }
        return content;
    }

    public final boolean isOpen(){
        if(!opened){
            Logger.error(new Exception("Database not opened."));
        }
        return opened;
    }

    public void closeDB() {
        if(isOpen()){
          //  System.out.println("MaXX Settings: Database Closed.");
            opened = false;
            content = null;
        }
    }

    public boolean put(@NotNull IndexEntry entry){
        boolean result = false;
        if(isOpen()){
            try {
                // write new index entry
                Files.write(databaseFullPath, entry.toString().getBytes(StandardCharsets.UTF_8), StandardOpenOption.APPEND);

                // reload database
                content = loadDB(databaseFullPath);
                result = true;
            } catch (IOException e) {
                Logger.error(e);
            }
        }
        return result;
    }

    /**
     * Try to fetch DB record from Hash filename
     * @param uuid input
     * @return {@code Optional<String>} fund record
     */
    public final Optional<String> findByUUID(@NotNull String uuid){
        if(isOpen()) {
            int index = content.indexOf(uuid);
            if (index > -1) {
                return Optional.of(content.substring(index, index + IndexEntry.RECORD_LENGTH));
            }
        }
        return Optional.empty();
    }

    /**
     * Try to fetch DB record from Hash filename
     * @param criteria input    ex: /A3/11/DD/FF/Acceleration
     * @return record or NULL String
     */
    public final Optional<String> findByHashFilename(@NotNull String criteria){
        try {
            if (isOpen()) {
                int index = content.indexOf(criteria);
                if (index > -1) {
                    index -= 36;
                    String record = content.substring(index, index + IndexEntry.RECORD_LENGTH);
                    return Optional.of(record);
                }
            }
        }catch (Exception e){
            Logger.error(e);
        }
        return Optional.empty();
    }

    public final boolean lookup(@NotNull String lookup){
        if(isOpen()) {
            int index = content.indexOf(lookup);
            if (index > -1) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Integer count() {
        if(isOpen()) {
            return content.length() / IndexEntry.RECORD_LENGTH;
        }
        return 0;
    }

    @Override
    public List<IndexEntry> fetchAll() {
        List<IndexEntry> entries = new ArrayList<>(16);
        if(isOpen()) {
            int len = content.length();
            int count = count();
            for (int i=0; i< count; i++){
                int index = i*IndexEntry.RECORD_LENGTH;
                int offset = index + IndexEntry.RECORD_LENGTH;
                String chunk = content.substring(index,offset);
                IndexEntry indexEntry = new IndexEntry(chunk);
                entries.add(indexEntry);
            }
        }
        return entries;
    }
}