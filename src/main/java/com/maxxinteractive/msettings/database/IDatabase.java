package com.maxxinteractive.msettings.database;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Optional;

/**
 * MaXX Settings System wide Database specifications
 *  @author Eric Masson
 *  @version 1.0
 */
public interface IDatabase {

    boolean createDB(boolean forceCreation);

    boolean open();

    boolean isOpen();

    void closeDB();

    boolean put(IndexEntry entry);

    Optional<String> findByUUID(@NotNull String uuid);

    Optional<String> findByHashFilename(@NotNull String hashDirectory);

    boolean lookup(@NotNull String lookup);

    List<IndexEntry> fetchAll();

    Integer count();
}
