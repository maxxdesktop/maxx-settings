package com.maxxinteractive.msettings.database;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.UUID;

public final class IndexEntry implements Serializable {

    public static final int UUID_LENGTH=36;
    public static final int PATH_LENGTH=12;
    public static final int SCHEMA_LENGTH=80;
    public static final int RECORD_LENGTH= UUID_LENGTH+PATH_LENGTH+SCHEMA_LENGTH;

    private final String uuid;
    private final String path;
    private final String schema;

    public IndexEntry(@NotNull String rawString){
        rawString = rawString.trim(); // trim possible empty space since it's a fixed with record
        this.uuid = rawString.substring (0,UUID_LENGTH);
        this.path = rawString.substring (UUID_LENGTH , UUID_LENGTH+PATH_LENGTH);
        this.schema = rawString.substring (UUID_LENGTH+PATH_LENGTH);
    }

    public IndexEntry(@NotNull UUID uuid, @NotNull  String path, @NotNull  String schema) {
        this(uuid.toString(), path, schema.trim());
    }

    public IndexEntry(@NotNull final String uuid, @NotNull  String path, @NotNull  String schema) {
        this.uuid = uuid.substring(0, Math.min(uuid.length(), UUID_LENGTH));
        this.path = path.substring(0, Math.min(path.length(), PATH_LENGTH));
        this.schema = String.format("%-" + SCHEMA_LENGTH + "s", schema);
    }

    public final String getUuid() {
        return uuid;
    }

    public final String getSchema() {
        return schema;
    }

    public final String getPath() {
        return path;
    }

    public final boolean isEqual(String uuid){
        return this.uuid.equalsIgnoreCase(uuid);
    }

    public String toString(){
        return String.format("%s%s%s", uuid,path,schema);
    }

}
