package com.maxxinteractive.msettings.instruments;

import com.maxxinteractive.msettings.Constants;
import com.maxxinteractive.msettings.attributes.IAttribute;
import com.maxxinteractive.msettings.attributes.values.IAttributeValue;
import com.maxxinteractive.msettings.attributes.values.IntegerValue;
import com.maxxinteractive.msettings.attributes.values.StringValue;
import com.maxxinteractive.msettings.commons.Logger;
import com.maxxinteractive.msettings.stereotype.Stereotypes;
import org.jetbrains.annotations.NotNull;

import java.util.Map;
import java.util.UUID;

import static com.maxxinteractive.msettings.Constants.*;

/**
 * <strong>Simple Stereotype Instrument used to represent a sequence of characters.</strong><br><br>
 * → The version is set to 1.0, if not specified.<br>
 * → The 'default' value is the string ‘null’, if not specified.<br>
 * → The character 'encoding' attribute is <strong>mandatory</strong> and is set to UTF-8 by default.<br>
 * → The 'maxLength' is <strong>optional</strong> and calculated using (octets/bytes) with the specified encoding. If present it is used as a validation rule
 *   for constraining both 'default' and 'value' attributes to its specified length.<br>
 */
public class Chars extends Instrument{

    public static final String DEFAULT = "null";
    public static final String EMPTY_VALUE = "EMPTY";

    protected static final String[] EXTENSION_ATTRIBUTES = { ATTRIBUTE_ENCODING };
    protected static final String[] OPTIONAL_ATTRIBUTES = { ATTRIBUTE_MAX_LENGTH };

    /**
     * <strong>Create a Chars Instrument</strong>
     * @param uuid unique identifier or {@code null} if auto-generate is requested.
     * @param name name of the Instrument - must be provided. ex: Desktop.Workspace.DesktopName
     */
    public Chars(UUID uuid, @NotNull String name) {
        this(uuid, name, DEFAULT, EMPTY_VALUE);
    }

    /**
     * <strong>Create a Chars Instrument</strong>
     * @param uuid unique identifier or {@code null} if auto-generate is requested.
     * @param name name of the Instrument - must be provided. ex: Desktop.Workspace.DesktopName
     * @param defaultValue for this Instrument - must be provided.
     */
    public Chars(UUID uuid, @NotNull String name, @NotNull String defaultValue) {
        this(uuid, name, defaultValue, EMPTY_VALUE);
    }

    /**
     * <strong>Create a Chars Instrument</strong>
     * @param uuid unique identifier or {@code null} if auto-generate is requested.
     * @param name name of the Instrument - must be provided. ex: Desktop.Workspace.DesktopName
     * @param defaultValue for this Instrument - must be provided.
     * @param value for this Instrument - must be provided.
     */
    public Chars(UUID uuid, @NotNull String name, @NotNull String defaultValue, @NotNull String value) {
        super(uuid, name, Stereotypes.Chars);

        // set Default Encoding
        setEncoding(ENCODING_UTF8);

        setDefault(defaultValue);

        if(!value.equals(EMPTY_VALUE)){
            setValue(value);
        }
    }

    /**
     * <strong>Create a Chars Instrument</strong>
     * @param attributes for this Instrument in form of {@code Map<String, IAttribute>}
     */
    public Chars(@NotNull Map<String, IAttribute> attributes) {
        super(attributes);
        addAttribute(attributes.get(Constants.ATTRIBUTE_DEFAULT));
        addAttribute(attributes.get(ATTRIBUTE_ENCODING));
        addAttribute(attributes.get(ATTRIBUTE_MAX_LENGTH));
        addAttribute(attributes.get(Constants.ATTRIBUTE_VALUE));
    }

    /**
     * Provide the mandatory extension attribute names supported by this Instrument.
     * @return the names or empty if not specified.
     */
    public String[] getExtensions(){
        return EXTENSION_ATTRIBUTES;
    }

    /**
     * Provide the optional attribute names supported by this Instrument
     * @return the names or empty if not specified.
     */
    public String[] getOptionalAttributes (){
        return OPTIONAL_ATTRIBUTES;
    }

    /**
     * Specify the 'default' attribute with a runtime value, a.k.a. an User Preference.
     * Validation : Use 'maxLength', if specified, to crop the input parameter to a maximum length of character.
     * @param inputValue to be set as 'default' attribute.
     * @return itself for convenient setter calls chaining.
     */
    public Chars setDefault(String inputValue){
        if (inputValue == null) {
            inputValue=DEFAULT;
            Logger.warning(buildWarningMessage(ATTRIBUTE_DEFAULT, LOG_MESSAGE_NULL_ATTRIBUTE_SET_DEFAULT));
        }
        int maxLength = getMaxLength();
        if(maxLength > 0){
            inputValue = inputValue.substring(0,maxLength);
        }
        addAttribute(ATTRIBUTE_DEFAULT, new StringValue(inputValue), Stereotypes.Chars);
        return this;
    }

    /**
     * Specify the 'value' attribute with a runtime value, a.k.a. an User Preference.
     * Validation : Use 'maxLength', if specified, to crop the input parameter to a maximum length of character.
     * @param inputValue to be set as 'value' attribute.
     * @return itself for convenient setter calls chaining.
     */
    public Chars setValue(String inputValue){
        if (inputValue == null) {
            inputValue=DEFAULT;
            Logger.warning(buildWarningMessage(ATTRIBUTE_VALUE, LOG_MESSAGE_NULL_ATTRIBUTE_SET_DEFAULT));
        }
        int maxLength = getMaxLength();
        if(maxLength > 0){
            inputValue = inputValue.substring(0,maxLength);
        }
        addAttribute(ATTRIBUTE_VALUE, new StringValue(inputValue), Stereotypes.Chars);
        return this;
    }

    /**
     * Specify the mandatory characters 'encoding' attribute with a runtime value, a.k.a. an User Preference.
     * @param val encoding value to set. The default value is UTF-8.
     * @return itself for convenient setter calls chaining.
     */
    public Chars setEncoding(String val){
        addAttribute(ATTRIBUTE_ENCODING, new StringValue(val), Stereotypes.Chars);
        return this;
    }

    /**
     * Specify the optional 'maxLength' attribute with a runtime value, a.k.a. an User Preference.
     * @param val maximum allowed length for both the 'default' and 'value' attributes. There is no default
     * @return itself for convenient setter calls chaining.
     */
    public Chars setMaxLength(Integer val){
        addAttribute(ATTRIBUTE_MAX_LENGTH, new IntegerValue(val), Stereotypes.Number);
        return this;
    }

    /**
     * Return the characters 'encoding' set for this Instrument. Default is UTF-8
     * @return int, the length
     */
    public String getEncoding() {
        return getAttributeStringValue(ATTRIBUTE_ENCODING, ENCODING_UTF8);
    }

    /**
     * Return in characters the maximum length allowed for 'default' and 'value' attributes for this Instrument. There is no default as this attribute is optional.
     * @return the maximum length if set or 0 if not specified.
     */
    public int getMaxLength() {
        return getAttributeIntegerValue(ATTRIBUTE_MAX_LENGTH, 0);
    }
}