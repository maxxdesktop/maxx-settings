package com.maxxinteractive.msettings.instruments;

import com.maxxinteractive.msettings.Constants;
import com.maxxinteractive.msettings.attributes.IAttribute;
import com.maxxinteractive.msettings.attributes.values.FloatValue;
import com.maxxinteractive.msettings.commons.Logger;
import com.maxxinteractive.msettings.stereotype.Stereotypes;
import org.jetbrains.annotations.NotNull;

import java.util.Map;
import java.util.UUID;

import static com.maxxinteractive.msettings.Constants.LOG_MESSAGE_NULL_ATTRIBUTE_SET_DEFAULT;

/**
 * <strong>Simple Stereotype Instrument used to represent an unsigned numerical value with decimal single precision.</strong><br><br>
 * → The version is set to 1.0, if not specified.<br>
 * → Default value is 0.0, if not specified.<br>
 */
public class Decimal extends Instrument {

	public static final float DEFAULT = 0.0f;
	public static final float EMPTY_VALUE = 12345.67890f;

	/**
	 * <strong>Create a Decimal Instrument</strong>
	 * @param uuid unique identifier or {@code null} if auto-generate is requested.
	 * @param name name of the Instrument - must be provided. ex: Desktop.Mouse.AccelerationFactor
	 */
	public Decimal (UUID uuid, @NotNull String name) {
		this(uuid, name, DEFAULT, EMPTY_VALUE);
	}

	/**
	 * <strong>Create a Decimal Instrument</strong>
	 * @param uuid unique identifier or {@code null} if auto-generate is requested.
	 * @param name name of the Instrument - must be provided. ex: Desktop.Mouse.AccelerationFactor
	 * @param defaultValue for this Instrument - must be provided.
	 */
	public Decimal (UUID uuid, @NotNull String name, @NotNull Float defaultValue) {
		this(uuid, name, defaultValue, EMPTY_VALUE);
	}

	/**
	 * <strong>Create a Decimal Instrument</strong>
	 * @param uuid unique identifier or {@code null} if auto-generate is requested.
	 * @param name name of the Instrument, ex: Desktop.Mouse.AccelerationFactor
	 * @param defaultValue for this Instrument - must be provided.
	 * @param value for this Instrument - must be provided.
	 */
	public Decimal (UUID uuid, @NotNull String name, @NotNull Float defaultValue, @NotNull Float value) {
		super(uuid, name, Stereotypes.Decimal);

		setDefault(defaultValue);

		if(value != EMPTY_VALUE){
			setValue(value);
		}
	}

	/**
	 * <strong>Create a Decimal Instrument</strong>
	 * @param attributes for this Instrument in form of {@code Map<String, IAttribute>}
	 */
	public Decimal(@NotNull Map<String, IAttribute> attributes) {
		super(attributes);
		addAttribute(attributes.get(Constants.ATTRIBUTE_DEFAULT));
		addAttribute(attributes.get(Constants.ATTRIBUTE_VALUE));
	}

	/**
	 * Specify the 'default' attribute with a runtime value, a.k.a. an User Preference.
	 * @param inputValue to be set as 'default' attribute.
	 * @return itself for convenient setter calls chaining.
	 */
	public Decimal setDefault(Float inputValue){
		if (inputValue == null) {
			inputValue = DEFAULT;
			Logger.warning(buildWarningMessage(ATTRIBUTE_DEFAULT, LOG_MESSAGE_NULL_ATTRIBUTE_SET_DEFAULT));
		}
		addAttribute(ATTRIBUTE_DEFAULT, new FloatValue(inputValue), Stereotypes.Decimal);
		return this;
	}

	/**
	 * Specify the 'value' attribute with a runtime value, a.k.a. an User Preference.
	 * @param inputValue to be set as 'value' attribute.
	 * @return itself for convenient setter calls chaining.
	 */
	public Decimal setValue(Float inputValue){
		if (inputValue == null) {
			inputValue = DEFAULT;
			Logger.warning(buildWarningMessage(ATTRIBUTE_VALUE, LOG_MESSAGE_NULL_ATTRIBUTE_SET_DEFAULT));
		}
		addAttribute(ATTRIBUTE_VALUE, new FloatValue(inputValue), Stereotypes.Decimal);
		return this;
	}
}