package com.maxxinteractive.msettings.instruments;

import com.maxxinteractive.msettings.attributes.IAttribute;
import com.maxxinteractive.msettings.schema.ISchema;

public interface IInstrument {

    public ISchema schema = null;
    /**
     * Return the corresponding to the Schema's 'default' Attribute
     * @return the default values IAttribute
     */
    IAttribute getDefault();

    boolean hasExtensions();
    String[] getExtensions();

    boolean hasOptionals();
    String[] getOptionals();

    String getFullQualifiedName();
    String getRelativePath();
}
