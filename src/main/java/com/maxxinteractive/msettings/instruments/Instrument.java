package com.maxxinteractive.msettings.instruments;

import com.maxxinteractive.msettings.Constants;
import com.maxxinteractive.msettings.attributes.AttributeBuilder;
import com.maxxinteractive.msettings.attributes.IAttribute;
import com.maxxinteractive.msettings.attributes.NullNotPresentAttribute;
import com.maxxinteractive.msettings.attributes.values.IAttributeValue;
import com.maxxinteractive.msettings.commons.Logger;
import com.maxxinteractive.msettings.preferences.IUserPreference;
import com.maxxinteractive.msettings.schema.Schema;
import com.maxxinteractive.msettings.stereotype.Stereotypes;
import com.maxxinteractive.msettings.utils.*;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.Map;
import java.util.Objects;
import java.util.StringTokenizer;
import java.util.UUID;

import static com.maxxinteractive.msettings.Constants.*;

/**
 * Base class for all MaXX Settings Instrumentation.
 */
public class  Instrument extends Schema implements IUserPreference
{
	protected String classification=null;
	protected String group=null;
	protected String instrumentName;
	protected int hashCode = 0;
	protected String hashedDirectory = null;

	/**
	 * Create an Instrument. Version will be set to 1.0.
	 * @param uuid String representing unique identifier - must be provided.
	 * @param name name of the Instrument - must be provided. ex: Desktop.Workspace.DesktopName
	 * @param stereotype of the Instrument - must be provided.
	 */
	protected Instrument(@NotNull String uuid, @NotNull String name, @NotNull Stereotypes stereotype) {
		this(UUID.fromString(uuid), name, stereotype, VERSION_10);
	}

	/**
	 * Create an Instrument. Version will be set to 1.0.
	 * @param uuid unique identifier or null if want auto-generate one
	 * @param name name of the Instrument - must be provided. ex: Desktop.Workspace.DesktopName
	 * @param stereotype of the Instrument - must be provided.
	 */
	protected Instrument(UUID uuid, @NotNull String name, @NotNull Stereotypes stereotype) {
		this(uuid, name, stereotype, VERSION_10);
	}

	/**
	 * Create an Instrument
	 * @param uuid unique identifier or null if want auto-generate one.
	 * @param name name of the Instrument - must be provided. ex: Desktop.Workspace.DesktopName
	 * @param version of the Instrument - must be provided.
	 * @param stereotype of the Instrument - must be provided.
	 */
	protected Instrument(UUID uuid, @NotNull String name, @NotNull Stereotypes stereotype, @NotNull String version) {
		super(uuid, name, stereotype, version);
		instrumentName = name;
		// initialize and cache internal values
		initialize();
	}

	/**
	 * Create an Instrument from a Map of String, IAttribute.
	 * This constructor is usually used when parsing Instrument file.
	 * version will be set to '1.0' if not present.
	 * @param attributes map of IAttribute
	 */
	protected Instrument(@NotNull Map<String, IAttribute> attributes){
		super(UUID.fromString(attributes.get(Constants.ATTRIBUTE_UUID).getValueAsString()),
				attributes.get(Constants.ATTRIBUTE_NAME).getValueAsString(),
				Stereotypes.getStereotypeFromString(attributes.get(Constants.ATTRIBUTE_STEREOTYPE).getValueAsString()),
				VERSION_10);

		if(attributes.get(ATTRIBUTE_VERSION) != null){
			addAttribute(attributes.get(ATTRIBUTE_VERSION));
		}

		instrumentName = attributes.get(Constants.ATTRIBUTE_NAME).getValueAsString();
		// initialize and cache internal values
		initialize();
	}

	private void initialize(){
		StringTokenizer st = new StringTokenizer(instrumentName, ".");
		if (st.countTokens() >= 2){
			classification = st.nextToken();
			group = st.nextToken();
		}else {
			System.err.format("error: Failed to initialize Instrument '%s'. Bad name format. Must be like : Desktop.Mouse.Acceleration.\n",instrumentName);
		}
		// calculate hash Code based on the Instrument Name with toLowerCase conversion
		// ex: Desktop.Mouse.Acceleration ->  desktop.mouse.acceleration -> /3b/2a/d4/d6
		hashCode = HasherUtility.calculateHashCode(getName().toLowerCase());
		hashedDirectory = HasherUtility.generateDirectoryHash(hashCode);
	}

	public final boolean hasExtensions(){
		return (this.getExtensions().length == 0);
	}

	public final boolean hasOptionals(){
		return (this.getOptionalAttributes().length == 0);
	}

	/**
	 * Provide a list of Extension Attribute(s) for this Instrument implementation.
	 * Extension attributes(s) are mandatory.
	 * This method MUST be Overridden in the Child class, extension attributes are available.
	 * @return list of Extension Attribute Names
	 */
	public String[] getExtensions(){
		return Constants.EMPTY_ARRAY;
	}

	/**
	 * Provide a list of Optional Attribute(s) for this Instrument implementation.
	 * Optionals attributes(s) are, well, not mandatory.
	 * This method MUST be Overridden in the Child class, optionals attributes are available.
	 * @return list of Optional Attribute Names
	 */
	public String[] getOptionalAttributes (){
		return Constants.EMPTY_ARRAY;
	}

	/**
	 * Flag that indicate if the default values can be updatable via CLI
	 * @return true or false
	 */
	public boolean isDefaultReadOnly(){
		return true;
	}

	/**
	 * Return the corresponding to the Schema's 'default' Attribute
	 * @return the default values IAttribute
	 */
	public IAttribute getDefault() {
		return attributes.get(ATTRIBUTE_DEFAULT);
	}

	/**
	 * Return the corresponding to the Schema's 'values' Attribute
	 * @return the default values IAttribute
	 */
	public IAttribute getValue() {
		IAttribute val = attributes.get(ATTRIBUTE_VALUE);
		if(val == null){
			return new NullNotPresentAttribute("notPresent");
		}
		return val;
	}

	@Override
	public String resolve() {
		IAttribute val = attributes.get(ATTRIBUTE_VALUE);
		if(val == null){
			val = attributes.get(ATTRIBUTE_DEFAULT);
		}
		return val.getValueAsString();
	}

	/**
	 * Reset the Instrument's values to its 'default' value
	 */
	public void resetValue(){
		IAttribute defAttribute = getAttribute(ATTRIBUTE_DEFAULT);
		if(defAttribute == null){
			Logger.warning("No default value found, aborting reset().");
			return;
		}
		IAttribute valAttribute = AttributeBuilder.buildWithAttributeValue(ATTRIBUTE_VALUE,defAttribute.getValue(),getStereotype());
		addAttribute(valAttribute );
	}


	/**
	 * Set the Instrument's  'value' attribute
	 * @param attributeValue value for the 'value' attribute
	 */
	public void setValue(@NotNull IAttributeValue attributeValue){
		addAttribute(ATTRIBUTE_VALUE, attributeValue, getStereotype() );
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Instrument that = (Instrument) o;
		return Objects.equals(getName(), that.getName());
	}

	/**
	 * Serialize the Instrument in key-values pairs.
	 * @return serialized attributes in key=values pairs.
	 */
	@Override
	public String serialize() {
		StringBuilder builder = new StringBuilder();
		builder.append(super.serialize()).append("\r\n");

		IAttribute tmp = getAttribute(ATTRIBUTE_DEFAULT);
		if (tmp != null) {
			builder.append(tmp).append("\r\n");
		}

		tmp = getAttribute(ATTRIBUTE_VALUE);
		if (tmp != null) {
			builder.append(tmp).append("\r\n");
		}

		// Serialize the Extension Attribute(s)
		for(String attr : this.getExtensions()) {
			IAttribute att = getAttribute(attr);
			if (att != null) {
				builder.append(att).append("\r\n");
			}else{
				Logger.error(new Exception(buildErrorMessage(attr, LOG_MESSAGE_MISSING_EXTENSION)));
			}
		}

		// Serialize the Optional Attribute(s)
		for(String attr : this.getOptionalAttributes()) {
			IAttribute att = getAttribute(attr);
			if (att != null) {
				builder.append(att).append("\r\n");
			}
		}
		return builder.toString().trim();
	}

	/**
	 * Provide the hash code calculation of the Instrument's name.
	 * @return hash code
	 */
	public final int getHashCode(){
		return hashCode;
	}

	/**
	 * Return the pre calculate hashed directories structure for this Instrument.
	 * The calculation is based on the hash code values of the
	 * Instrument's FQ Name with a toLowerCase() conversion.<br><br>
	 * This allow both case sensitive and insensitive fast look up.
	 * @return the calculated 4 level hashed directory names
	 */
	public final String getHashedPath(){
		return hashedDirectory;
	}

	/**
	 * Provide the Relative Path in relation to the $MAXX_SETTING/Instruments
	 * root directory for System wide.<br><br>
	 * ex:  The instruments Desktop.Mouse.Threshold of Number stereotype stored
	 *      in $MAXX_SETTINGS/Instruments
	 *      is return: /12/12/58/c6/Threshold.Number as RelativePath
	 *
	 * @return the Instrument's full relative path as a String
	 */
	public final String getRelativePath(){
		return hashedDirectory + File.separator + getFilename();
	}

	/**
	 * Provide the FQ Name of this Instrument.
	 * @return the Fully Qualified Name
	 */
	public final String getFQName(){
		StringBuilder qName = new StringBuilder(128);
		qName.append("/").append(classification).append("/").append(group).append("/")
		 	 .append(getFilename());
		return qName.toString();
	}

	protected final String buildWarningMessage(String attributeName, String message){
		StringBuilder builder = new StringBuilder(128);
		builder.append(getName())
				.append(" '").append(attributeName).append("' ")
				.append(message);
		return builder.toString();
	}

	protected final String buildErrorMessage(String attributeName, String message){
		StringBuilder builder = new StringBuilder(128);
		builder.append(getName())
				.append(" '").append(attributeName).append("' ")
				.append(message);
		return builder.toString();
	}
}
