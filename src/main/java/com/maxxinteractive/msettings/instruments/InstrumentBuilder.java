package com.maxxinteractive.msettings.instruments;

import com.maxxinteractive.msettings.attributes.IAttribute;
import com.maxxinteractive.msettings.commons.Logger;
import com.maxxinteractive.msettings.instruments.choices.Catalog;
import com.maxxinteractive.msettings.instruments.choices.Choice;
import com.maxxinteractive.msettings.instruments.complex.*;
import com.maxxinteractive.msettings.preferences.IUserPreference;
import com.maxxinteractive.msettings.stereotype.Stereotypes;
import com.maxxinteractive.msettings.utils.AttributeMapUtils;
import com.maxxinteractive.msettings.utils.KeyValuePair;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.maxxinteractive.msettings.Constants.*;

public class InstrumentBuilder {

    List<KeyValuePair> keyValues = new ArrayList<>(10);
    Map<String, IAttribute> attributes;
    Stereotypes stereotype;
    String instrumentName;
    boolean ready = false;


    /**
     * Instantiate an InstrumentBuilder with a {@code Map<String, IAttribute>} map
     * @param attributes to use by the builder
     */
    public InstrumentBuilder(@NotNull Map<String, IAttribute> attributes) {
        this.attributes = attributes;
        ready = extractInfoFromMap();
    }

    protected boolean extractInfoFromMap(){

        ready = AttributeMapUtils.normalizeAttributes(attributes);
        if(!ready) {
            Logger.error(new Exception("Unable to load Attributes..."));
            return false;
        }

        IAttribute  attribute = attributes.get(ATTRIBUTE_NAME);
        instrumentName = attribute.getValueAsString();



        attribute = attributes.get(ATTRIBUTE_STEREOTYPE);
        stereotype = Stereotypes.getStereotypeFromString(attribute.getValueAsString());



//        attribute = attributes.get(ATTRIBUTE_UUID);
//        if(attribute == null){
//            Logger.warning("No Attribute 'uuid' found.");
//            attributes.put(ATTRIBUTE_UUID, new UUIDAttribute(ATTRIBUTE_UUID, UUID.randomUUID()) );
//        }


        return true;
    }

    /**
     * Factory to build an User Preference based on the parsed data.
     * @return a valid User Preference or null if it failed.
     */
    public Optional<IUserPreference> build(){
        IUserPreference userPreference ;

        if(!ready){
            Logger.warning("Builder's internal dataset is corrupted or not ready to be used.  Consults the ~/.msettings.log for detail.");
            return Optional.empty();
        }
        // init or clear attributes Map
        if(attributes == null || attributes.isEmpty()){
            Logger.warning("No attributes, can't build anything...");
            return Optional.empty();
        }

        switch (stereotype){
            case Chars: {
                userPreference = new Chars(attributes);
                break;
            }
            case Number: {
                userPreference = new Number(attributes);
                break;
            }
            case Decimal: {
                userPreference = new Decimal(attributes);
                break;
            }
            case Logical: {
                userPreference = new Logical(attributes);
                break;
            }
            case Choice: {
                userPreference = new Choice(attributes);
                Choice choice = (Choice) userPreference;
                String type = choice.getType();
                if(type.equals("Catalog")){
                    Logger.debug("Choice with Catalog : " + choice.getCatalogName());
                    //load Catalog
                    String catalogFilename = choice.getCatalogName()+"."+"Catalog";
                    Logger.debug(catalogFilename);
                    InstrumentParser parser = new InstrumentParser(MSETTINGS_HOME_SYSTEM+"/"+CATALOGS+"/"+catalogFilename);
                    // set Catalog
                    if(!parser.parse()){
                        Logger.error(new Exception("Failed loading Catalog : " + catalogFilename));
                    }else {
                        Catalog catalog = new Catalog(parser.getAttributes());
                        choice.loadCatalog(catalog);
                    }
                }
                break;
            }
            case Gauge: {
                userPreference = new Gauge(attributes);
                break;
            }
            case Command:{
                userPreference =new Command(attributes);
                break;
            }
            case Dimension:{
                userPreference = new Dimension(attributes);
                break;
            }
            case Location: {
                userPreference = new Location(attributes);
                break;
            }
            case Geometry:{
                userPreference = new Geometry(attributes);
                break;
            }
            case Color:{
                userPreference = new Color(attributes);
                break;
            }
            case Typeface: {
                userPreference = new Typeface(attributes);
                break;
            }
            case Image:
            case Application:
            default: {
                Logger.error(new Exception("Failed building an instance of Instrument. Refer to the logs for detail."));
                return Optional.empty();
            }
        }
        return Optional.of(userPreference);
    }
}
