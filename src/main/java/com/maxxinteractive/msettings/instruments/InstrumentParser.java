package com.maxxinteractive.msettings.instruments;

import com.maxxinteractive.msettings.attributes.IAttribute;
import com.maxxinteractive.msettings.commons.Logger;
import com.maxxinteractive.msettings.schema.Schema;
import com.maxxinteractive.msettings.stereotype.Stereotypes;
import com.maxxinteractive.msettings.utils.AttributeMapUtils;
import com.maxxinteractive.msettings.utils.FileSystemInstrumentHelper;
import com.maxxinteractive.msettings.utils.KeyValuePair;
import org.jetbrains.annotations.NotNull;

import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

import static com.maxxinteractive.msettings.Constants.ATTRIBUTE_NAME;
import static com.maxxinteractive.msettings.Constants.ATTRIBUTE_STEREOTYPE;

/**
 * Instrument Parser
 */
public class InstrumentParser {

    List<KeyValuePair> keyValues = new ArrayList<>(16);
    Map<String, IAttribute> attributes;
    String filename = null;
    boolean parsingDone = false;
    Stereotypes stereotype = Stereotypes.Undefined;
    String instrumentName = null;

    /**
     * Instantiate an InstrumentParser with a filename.
     * @param filename to load the Instrument
     */
    public InstrumentParser(@NotNull String filename){
        this.filename = filename;
    }

    public String getInstrumentName() {
        return instrumentName;
    }

    public Stereotypes getStereotype(){
        return stereotype;
    }

    public Map<String,IAttribute> getAttributes(){
        return attributes;
    }

    /**
     * Create a string formatted version of all attributes
     * @return the string
     */
    public String exportAttributes(){
        return Schema.exportAttributes(attributes);
    }

    public List<KeyValuePair> getKeyValues() {
        return keyValues;
    }

    /**
     * Reset internal states, before a new parsing
     * @return itself for convenient chaining calls
     */
    public InstrumentParser reset(){
        if(attributes != null){
            attributes.clear();
        }
        filename = null;

        if(keyValues != null){
            keyValues.clear();
        }
        stereotype = Stereotypes.Undefined;
        instrumentName = null;

        parsingDone = false;
        return this;
    }

    /**
     * Reset internal states, before a new parsing
     * @param file to use
     * @return itself for convenient chaining calls
     */
    public InstrumentParser resetFile(String file){
        reset();
        filename = file;
        return this;
    }

    /**
     * Load a schema text file and produce an internal {@code List<KeyPairValue>} that can be retrieve. <br>
     * he method will try to figure out the Stereotype and instrumentName. The detected Stereotype and InstrumentName<br>
     * can be retrieved with their respective getter methods after a successful execution.
     * @return status of the execution
     */
    protected boolean load(){
        if(filename == null){
            Logger.error(new Exception("Filename not specified, aborting."));
            return false;
        }

        Path path;
        try {
            path = Paths.get(filename);
        }catch (InvalidPathException e){
            Logger.error(e);
            return false;
        }

        // Fetch file content as List of KeyValuePair
        Optional<List<KeyValuePair>> content = FileSystemInstrumentHelper.loadSchema(path);
        if(!content.isPresent()){
            return false;
        }
        keyValues = content.get();

        //extract stereotype and instrument's name
        for(KeyValuePair kvp : keyValues){
            String key = kvp.getKey();
            if(key.equals(ATTRIBUTE_STEREOTYPE)){
                stereotype = Stereotypes.getStereotypeFromString(kvp.getValue());
            }
            if(key.equals(ATTRIBUTE_NAME)){
                instrumentName = key;
            }
        }
        return true;
    }

    /**
     * Parse the loaded schema text file and construct a Map of IAttribute that can be retrieved by the method {@code getAttributes()}.<br>
     * This Class can be used in conjunction with the InstrumentBuilder class.
     * @return true if parsing was successful, false if it failed.
     */
    public boolean parse(){

        // load filename if no KeyValuePair List is provided
        if(filename != null) {
            if(! load()) {
                return false;
            }
        }

        // init or clear attributes Map
        if(attributes == null){
            attributes = new HashMap<>(16);
        }else {
            attributes.clear();
        }

        Optional<Map<String,IAttribute>> attributesOpt = AttributeMapUtils.convert(keyValues);
        if(!attributesOpt.isPresent()){
            return false;
        }

        attributes = attributesOpt.get();

        parsingDone = AttributeMapUtils.normalizeAttributes(attributes);
        return parsingDone;
    }
}