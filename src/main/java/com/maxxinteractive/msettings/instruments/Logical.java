package com.maxxinteractive.msettings.instruments;

import com.maxxinteractive.msettings.Constants;
import com.maxxinteractive.msettings.attributes.IAttribute;
import com.maxxinteractive.msettings.attributes.values.BooleanValue;
import com.maxxinteractive.msettings.commons.Logger;
import com.maxxinteractive.msettings.stereotype.Stereotypes;
import org.jetbrains.annotations.NotNull;

import java.util.Map;
import java.util.UUID;

import static com.maxxinteractive.msettings.Constants.LOG_MESSAGE_NULL_ATTRIBUTE_SET_DEFAULT;

/**
 * <strong>Simple Stereotype Instrument used to represent a boolean value of either true or false.</strong><br><br>
 * → The version is set to 1.0, if not specified.<br>
 * → Default value is false, if not specified.<br>
 */
public class Logical extends Instrument {

	public static final Boolean DEFAULT = false;
	public static final Boolean EMPTY_VALUE = new Boolean(false);

	/**
	 * <strong>Create a Logical Instrument</strong>
	 * @param uuid unique identifier or {@code null} if auto-generate is requested.
	 * @param name name of the Instrument - must be provided. ex: Desktop.Mouse.LeftHand
	 */
	public Logical (UUID uuid, @NotNull String name) {
		this(uuid, name, DEFAULT,EMPTY_VALUE);
	}

	/**
	 * <strong>Create a Logical Instrument</strong>
	 * All values are set to false
	 * @param uuid unique identifier or {@code null} if auto-generate is requested.
	 * @param name name of the Instrument - must be provided. ex: Desktop.Mouse.LeftHand
	 * @param defaultValue for this Instrument - must be provided.
	 */
	public Logical (UUID uuid, @NotNull String name, @NotNull Boolean defaultValue) {
		this(uuid, name, defaultValue, EMPTY_VALUE);
	}

	/**
	 * <strong>Create a Logical Instrument</strong>
	 * @param uuid unique identifier or {@code null} if auto-generate is requested.
	 * @param name name of the Instrument, ex: Desktop.Mouse.LeftHand
	 * @param defaultValue for this Instrument - must be provided
	 * @param value for this Instrument - must be provided
	 */
	public Logical (UUID uuid, @NotNull String name, @NotNull Boolean defaultValue, @NotNull Boolean value) {
		super(uuid, name, Stereotypes.Logical);

		setDefault(defaultValue);

		if (value != EMPTY_VALUE) {
			setValue(value);
		}
	}

	/**
	 * <strong>Create a Logical Instrument</strong>
	 * @param attributes for this Instrument in form of {@code Map<String, IAttribute>}
	 */
	public Logical(@NotNull Map<String, IAttribute> attributes) {
		super(attributes);
		addAttribute(attributes.get(Constants.ATTRIBUTE_DEFAULT));
		addAttribute(attributes.get(Constants.ATTRIBUTE_VALUE));
	}

	/**
	 * Specify the 'default' attribute with a runtime value, a.k.a. an User Preference.
	 * @param inputValue to be set as 'default' attribute.
	 * @return itself for convenient setter calls chaining.
	 */
	public Logical setDefault(Boolean inputValue){
		if (inputValue == null) {
			inputValue=DEFAULT;
			Logger.warning(buildWarningMessage(ATTRIBUTE_DEFAULT, LOG_MESSAGE_NULL_ATTRIBUTE_SET_DEFAULT));
		}
		addAttribute(ATTRIBUTE_DEFAULT, new BooleanValue(inputValue), Stereotypes.Logical);
		return this;
	}

	/**
	 * Specify the 'value' attribute with a runtime value, a.k.a. an User Preference.
	 * @param inputValue to be set as 'vaue' attribute.
	 * @return itself for convenient setter calls chaining.
	 */
	public Logical setValue(Boolean inputValue){
		if (inputValue == null) {
			inputValue=DEFAULT;
			Logger.warning(buildWarningMessage(ATTRIBUTE_VALUE, LOG_MESSAGE_NULL_ATTRIBUTE_SET_DEFAULT));
		}
		addAttribute(ATTRIBUTE_VALUE, new BooleanValue(inputValue), Stereotypes.Logical);
		return this;
	}
}
