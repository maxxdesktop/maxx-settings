package com.maxxinteractive.msettings.instruments;

import com.maxxinteractive.msettings.Constants;
import com.maxxinteractive.msettings.attributes.IAttribute;
import com.maxxinteractive.msettings.attributes.values.IntegerValue;
import com.maxxinteractive.msettings.commons.Logger;
import com.maxxinteractive.msettings.stereotype.Stereotypes;
import org.jetbrains.annotations.NotNull;

import java.util.Map;
import java.util.UUID;

import static com.maxxinteractive.msettings.Constants.LOG_MESSAGE_NULL_ATTRIBUTE_SET_DEFAULT;

/**
 * <strong>Simple Stereotype Instrument used to represent an unsigned integer numerical value.</strong><br><br>
 * → The version is set to 1.0, if not specified.<br>
 * → Default value is 0, if not specified.<br>
 */
public class Number extends Instrument {

    public static final int DEFAULT = 0;
    public static final int EMPTY_VALUE = -1168333111;

    /**
     * <strong>Create a Number Instrument</strong>
     * @param uuid unique identifier or {@code null} if auto-generate is requested.
     * @param name name of the Instrument - must be provided. ex: Desktop.Workspace.DesktopNumber
     */
    public Number (UUID uuid, @NotNull String name) {
        this(uuid, name, DEFAULT, EMPTY_VALUE);
    }

    /**
     * <strong>Create a Number Instrument</strong>
     * @param uuid unique identifier or {@code null} if auto-generate is requested.
     * @param name name of the Instrument - must be provided. ex: Desktop.Workspace.DesktopNumber
     * @param defaultValue for this Instrument - must be provided.
     */
    public Number (UUID uuid, @NotNull String name, @NotNull Integer defaultValue) {
        this(uuid, name, defaultValue, EMPTY_VALUE);
    }

    /**
     * <strong>Create a Number Instrument</strong>
     * @param uuid unique identifier or {@code null} if auto-generate is requested.
     * @param name name of the Instrument - must be provided. ex: Desktop.Workspace.DesktopNumber
     * @param defaultValue for this Instrument - must be provided
     * @param value for this Instrument - must be provided
     */
    public Number (UUID uuid, @NotNull String name, @NotNull Integer defaultValue, @NotNull Integer value) {
        super(uuid, name, Stereotypes.Number);

        setDefault(defaultValue);

        if(value != EMPTY_VALUE){
            setValue(value);
        }
    }

    /**
     * <strong>Create a Number Instrument</strong>
     * @param attributes for this Instrument in form of {@code Map<String, IAttribute>}
     */
    public Number(@NotNull Map<String, IAttribute> attributes) {
        super(attributes);

        addAttribute(attributes.get(Constants.ATTRIBUTE_DEFAULT));
        addAttribute(attributes.get(Constants.ATTRIBUTE_VALUE));
    }

    /**
     * Specify the 'default' attribute with a runtime value, a.k.a. an User Preference.
     * @param inputValue to be set as 'default' attribute.
     * @return itself for convenient setter calls chaining.
     */
    public Number setDefault(Integer inputValue){
        if (inputValue == null) {
            inputValue=DEFAULT;
            Logger.warning(buildWarningMessage(ATTRIBUTE_VALUE, LOG_MESSAGE_NULL_ATTRIBUTE_SET_DEFAULT));
        }
        addAttribute(ATTRIBUTE_DEFAULT, new IntegerValue(inputValue), Stereotypes.Number);
        return this;
    }

    /**
     * Specify the 'value' attribute with a runtime value, a.k.a. an User Preference.
     * @param inputValue to be set as 'value' attribute.
     * @return itself for convenient setter calls chaining.
     */
    public Number setValue(Integer inputValue){
        if (inputValue == null) {
            inputValue=DEFAULT;
            Logger.warning(buildWarningMessage(ATTRIBUTE_VALUE, LOG_MESSAGE_NULL_ATTRIBUTE_SET_DEFAULT));
        }
        addAttribute(ATTRIBUTE_VALUE, new IntegerValue(inputValue), Stereotypes.Number);
        return this;
    }
}