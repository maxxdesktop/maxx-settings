package com.maxxinteractive.msettings.instruments.choices;

import com.maxxinteractive.msettings.Constants;
import com.maxxinteractive.msettings.attributes.AttributeBuilder;
import com.maxxinteractive.msettings.attributes.IAttribute;
import com.maxxinteractive.msettings.commons.Logger;
import com.maxxinteractive.msettings.schema.Schema;
import com.maxxinteractive.msettings.stereotype.Stereotypes;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import static com.maxxinteractive.msettings.Constants.ATTRIBUTE_TYPE;
import static com.maxxinteractive.msettings.Constants.VERSION_10;

/*
Catalog Schema which provides an external Choice storage mechanism.


SGIScheme.Catalog  <<

version=1.0
stereotype=Catalog
name=SGIScheme
type=Chars
option[0]=Arizona
option[1]=Bayou
option[2]=BlackAndWhite
option[3]=DarkBliss
option[4]=Gainsborough

-------------------------

WinEditor.Catalog   <<

stereotype=Catalog
name=WinEditor
type=Command
option[0]=@Desktop.WinEditor.XNEdit.Application
option[1]=@Application.WinEditor.GEdit
option[2]=@Application.WinEditor.Sublime
option[3]=@Application.WinEditor.Gvim
 */

public class Catalog extends Schema implements IGroup, IOptionContainer {

    private IOptionContainer optionContainer;
    private Stereotypes type = Stereotypes.Chars;

    /**
     * Create a Catalog - version will be set the default values of 1.0
     * The default is set to 0 and type to Chars<br>
     * @param uuid       unique identifier or null if want auto-generate one
     * @param name       name of the Schema - must be provided. ex: LeftHand
     */
    public Catalog(@NotNull UUID uuid, @NotNull String name) {
        this(uuid, name, Constants.VERSION_10, Stereotypes.Chars);
    }

    /**
     * Create a Catalog.
     * @param uuid       unique identifier or null if want auto-generate one
     * @param name       name of the Schema, ex: LeftHand
     * @param type       type of items
     * @param version    schema's version
     */
    public Catalog(@NotNull UUID uuid, @NotNull String name, @NotNull String version, @NotNull Stereotypes type) {
        super(uuid, name, Stereotypes.Catalog, version);
        this.type = type;
    }

    /**
     * <strong>Create a Catalog</strong><br>
     * @param inputAttributes for this Instrument
     */
    public Catalog (@NotNull Map<String, IAttribute> inputAttributes) {
        super(null, "Unnamed" , Stereotypes.Catalog, VERSION_10);

        // add type from the incoming Map
        IAttribute attr = inputAttributes.get(ATTRIBUTE_TYPE);
        if(attr == null){
            Logger.warning("No attribute 'type' while constructing Catalog. Defaulting type to Chars.");
            // add type=Chars as a last resort
            attr = AttributeBuilder.buildString(ATTRIBUTE_TYPE,"Chars");
        }
        addAttribute(attr);



        if(inputAttributes.get(ATTRIBUTE_NAME) != null) {
            addAttribute(inputAttributes.get(ATTRIBUTE_NAME));
        }
        if(inputAttributes.get(ATTRIBUTE_UUID) != null) {
            addAttribute(inputAttributes.get(ATTRIBUTE_UUID));
        }

        // build IOptionContainer
        optionContainer = new OptionContainer(inputAttributes);
    }

    @Override
    public void appendOptionAttributes(@NotNull Map<String,IAttribute> attributes){
        optionContainer.appendOptionAttributes(attributes);
    }
    /**
     * Add an option to Option's array
     * @param index to use in the add operation
     * @param newOption to add
     */
    @Override
    public void addOption(String index, String newOption){
        optionContainer.addOption(index,newOption);
    }

    /**
     * Get the option String representatoin at index.  First index=0, last=count-1
     * @param index as the index location within the array of options
     * @return IAttribute at index location.
     */
    @Override
    public String getOption(String index){
        return optionContainer.getOption(index);
    }

    @Override
    public List<String> getChoices() {
        return optionContainer.getChoices();
    }

    /**
    * Return the size of the Choice's options.
    * @return the size
	 */
    @Override
    public int getCount(){
        return optionContainer.getCount();
    }

    @Override
    public Map<String,String> getOptions() {
        return optionContainer.getOptions();
    }

    @Override
    public List<IAttribute> getOptionAttributes() {
        return optionContainer.getOptionAttributes();
    }

    /**
     * Serialize this Schema in key-values pairs.
     * @return serialized Schema
     */
    @Override
    public String serialize() {
        StringBuilder builder = new StringBuilder(1024);
        builder.append(super.serialize()).append("\r\n");

        // Serialize type  Attribute
        builder.append(getAttribute(ATTRIBUTE_TYPE)).append("\r\n");

        // Serialize the Option[] = value sorted by index
        List<IAttribute> attributeList = optionContainer.getOptionAttributes();

        for (IAttribute attribute : attributeList) {
            builder.append(attribute).append("\r\n");
        }
        return builder.toString().trim();
    }
}
