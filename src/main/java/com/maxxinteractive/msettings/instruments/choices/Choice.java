package com.maxxinteractive.msettings.instruments.choices;


import com.maxxinteractive.msettings.Constants;
import com.maxxinteractive.msettings.attributes.AttributeBuilder;
import com.maxxinteractive.msettings.attributes.IAttribute;
import com.maxxinteractive.msettings.attributes.values.IAttributeValue;
import com.maxxinteractive.msettings.attributes.values.IntegerValue;
import com.maxxinteractive.msettings.attributes.values.StringValue;
import com.maxxinteractive.msettings.commons.Logger;
import com.maxxinteractive.msettings.instruments.Instrument;
import com.maxxinteractive.msettings.stereotype.Stereotypes;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static com.maxxinteractive.msettings.Constants.*;

/**
 * Complex Stereotype used to represent a typed indexed container with values.<br><br>
 * In most programming languages, it’s called an array. The type defines the option's subtype.<br>
 * Each option entry is a possible value choice.<br>
 * The version is set to 1.0, if not specified.<br>
 * The default and value attributes are indexes pointing back to the Choice’s option array.<br>
 * Option index starts at 0 and default value is also 0 by default.<br>
 */
public class Choice extends Instrument implements IOptionContainer {

	public static final String DEFAULT = "0";
	public static final String EMPTY_VALUE = "-1";

	private IOptionContainer optionContainer;
	private Stereotypes type = Stereotypes.Chars; //type of option[]  default is Chars
	private Catalog catalog = null;

	/**
	 * <strong>Create a Choice Instrument</strong><br>
	 * The default is set to 0, values to 0<br>
	 * @param uuid unique identifier or null if want auto-generate one
	 * @param name name of the Instrument, ex: Desktop.Mouse.Acceleration
	 */
	public Choice (@NotNull UUID uuid, @NotNull String name) {
		this(uuid, name,  DEFAULT, EMPTY_VALUE);
	}

	/**
	 * <strong>Create a Choice Instrument</strong><br>
	 * The default is set to 0, values to 0<br>
	 * @param uuid unique identifier or null if want auto-generate one
	 * @param name name of the Instrument, ex: Desktop.Mouse.Acceleration
	 * @param defaultValue for this Choice
	 */
	public Choice (@NotNull UUID uuid, @NotNull String name, @NotNull String defaultValue) {
		this(uuid, name,  defaultValue, EMPTY_VALUE);
	}

	/**
	 * <strong>Create a Choice Instrument</strong><br>
	 * The default is set to 0 and type to Chars<br>
	 * @param uuid unique identifier or null if want auto-generate one
	 * @param name name of the Instrument, ex: Desktop.Mouse.Acceleration
	 * @param defaultValue for this Choice
	 * @param value for this Choice - must be provided
	 */
	public Choice (@NotNull UUID uuid, @NotNull String name, @NotNull String defaultValue, String value) {
		super(uuid, name, Stereotypes.Choice);

		// add type=Chars
		addAttribute(ATTRIBUTE_TYPE, new StringValue(type.name()), Stereotypes.Chars);

		// init option container
		optionContainer = new OptionContainer();

		// Default is pointing to an index of the options[]
		setDefault(defaultValue);

		if(value != EMPTY_VALUE){
			setValue(value);
		}
	}

	/**
	 * <strong>Create a Choice Instrument</strong><br>
	 * @param inputAttributes for this Instrument
	 */
	public Choice (@NotNull Map<String, IAttribute> inputAttributes) {
		super(inputAttributes);

		// init option container, with possibly appending some from pre-loaded options[]
		optionContainer = new OptionContainer(inputAttributes);

		/*
		Choice without catalog
		...
		type=Chars
		...
		Choice with catalog
		...
		type=Catalog
		catalog=WinEditor
		...
		 */

		// add type from the incoming Map
		IAttribute attr = inputAttributes.get(ATTRIBUTE_TYPE);
		if(attr == null){
			Logger.warning("No attribute 'type' while constructing Choice. Defaulting type to Chars.");
			// add type=Chars as a last resort
			attr = AttributeBuilder.buildString(ATTRIBUTE_TYPE,"Chars");
		}
		addAttribute(attr);

		// re-fetch type
		attr = getAttribute(ATTRIBUTE_TYPE);

		// with catalog
		if(attr.getValueAsString().equals("Catalog")){
			type = Stereotypes.Catalog;
			attr = inputAttributes.get(ATTRIBUTE_CATALOG);
			if(attr == null){
				Logger.warning("Choice with Catalog type but No attribute 'catalog' while constructing...");
			}else {
				Logger.debug("Choice with Catalog : " + attr.getValueAsString()+". Use loadCatalog() to inform the instance of any option[index].");
			}
			addAttribute(attr);
		}

		// default
		IAttribute attribute = inputAttributes.get(Constants.ATTRIBUTE_DEFAULT);
		setDefault(attribute.getValueAsString());

		// value
		attribute = inputAttributes.get(Constants.ATTRIBUTE_VALUE);
		if(attribute != null){
			setValue(attribute.getValueAsString());
		}
	}

	@Override
	public void appendOptionAttributes(@NotNull Map<String,IAttribute> attributes){
		optionContainer.appendOptionAttributes(attributes);
	}

	/**
	 * Flag that indicate if the default values can be updatable via CLI
	 * @return true or false
	 */
	public boolean isDefaultReadOnly() {
		return false;
	}

	/**
	 * Set the 'default' values of the Choice. The 'default' must be within 0 and the size of the options array.
	 * @param val to set.
	 * @return itself so it can be nicely chained
	 */
	public Choice setDefault(String val){
		if(optionContainer.getCount() > 0) {  // rule apply only when option[] are present
			if (!isOptionAvailable(val)) {
				String message = String.format(LOG_MESSAGE_OPTION_INDEX_NOT_FOUND_INT_ATTRIBUTE, val);
				Logger.warning(buildWarningMessage(ATTRIBUTE_DEFAULT, message));
			}
		}
		addAttribute(ATTRIBUTE_DEFAULT, new StringValue(val), Stereotypes.Choice );
		return this;
	}

	@Override
	public void setValue(@NotNull IAttributeValue value){
		setValue(value.getValueAsString());
	}

	public boolean isOptionAvailable(String index){
		return (getOption(index) != null);
	}

	/**
	 * Set the 'default' values of the Choice. The 'default' must be within 0 and the size of the options array.
	 * @param val to set.
	 * @return itself so it can be nicely chained
	 */
	public Choice setValue(String val){
		if(optionContainer.getCount() > 0) {  // rule apply only when option[] are present
			if (!isOptionAvailable(val)) {
				String message = String.format(LOG_MESSAGE_OPTION_INDEX_NOT_FOUND_INT_ATTRIBUTE, val);
				Logger.warning(buildWarningMessage(ATTRIBUTE_VALUE, message));
				return this;
			}
		}
		addAttribute(ATTRIBUTE_VALUE, new StringValue(val), Stereotypes.Choice );
		return this;
	}

	public String getCatalogName(){
		return getAttributeStringValue(ATTRIBUTE_CATALOG);
	}

	/**
	 * Load a Catalog schema and append its options[] to the Choice.
	 * @param catalog to add
	 * @return status of the operation
	 */
	public boolean loadCatalog(@NotNull Catalog catalog){
		this.catalog = catalog;
		List<IAttribute> catalogAttributes =  catalog.getOptionAttributes();
		Map<String,IAttribute> choiceAttributes = new HashMap<>(catalogAttributes.size());

		for(IAttribute attr : catalogAttributes){
			choiceAttributes.put(attr.getName(), attr);
		}
		appendOptionAttributes(choiceAttributes);

		return true;
	}

	public Catalog getCatalog(){
		return catalog;
	}

	public boolean isUsingCatalog(){
		return (type == Stereotypes.Catalog);
	}

	/**
	 * Add an option to Option's array
	 * @param index to use in the add operation
	 * @param newOption to add
	 */
	@Override
	public void addOption(@NotNull String index, @NotNull String newOption){
		optionContainer.addOption(index,newOption);
	}

	/**
	 * Get the option String representation at index.  First index=0, last=count-1
	 * @param index as the index location within the array of options
	 * @return String value at index location, or null if not found
	 */
	@Override
	public String getOption(@NotNull String index){
		String val = optionContainer.getOption(index);
		if(val==null){
			Logger.warning("Choice.getOption(...) failed, index '"+index+"' not found.");
			return "";
		}
		return val;
	}

	@Override
	public List<String> getChoices() {
		return optionContainer.getChoices();
	}

	/**
	 * Return the size of the Choice's options.
	 * @return the size
	 */
	@Override
	public int getCount(){
		return optionContainer.getCount();
	}

	@Override
	public Map<String,String> getOptions() {
		return optionContainer.getOptions();
	}

	@Override
	public List<IAttribute> getOptionAttributes() {
		return optionContainer.getOptionAttributes();
	}

	@Override
	public String resolve() {
		String value = getAttributeStringValue(ATTRIBUTE_VALUE);
		if(value==null){
			return "";
		}
		String option = getOption(value);
		if (option != null) {
			Logger.error(new Exception("failed to resolve : " + getName() + " value/default value unset."));
		}
		return option;
	}

	/**
	 * Get the Choice's options type
	 * @return the type
	 */
	public final String getType () {
		return getAttributeStringValue(ATTRIBUTE_TYPE);
	}

	/**
	 * Serialize this Schema in key-values pairs.
	 * @return serialized Schema
	 */
	@Override
	public String serialize() {

		StringBuilder builder = new StringBuilder(1024);

		builder.append(super.serialize()).append("\r\n");

		// Serialize type  Attribute
		builder.append(getAttribute(ATTRIBUTE_TYPE)).append("\r\n");

		// catalog
		IAttribute catalogAttr = attributes.get(ATTRIBUTE_CATALOG);
		if(catalogAttr != null){
			builder.append(catalogAttr).append("\r\n");
		}

		// Serialize the Option[] = value sorted by index
		List<IAttribute> attributeList = optionContainer.getOptionAttributes();
		for (IAttribute attribute : attributeList) {
			builder.append(attribute).append("\r\n");
		}

		return builder.toString().trim();
	}
}