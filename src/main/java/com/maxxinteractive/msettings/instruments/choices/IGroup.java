package com.maxxinteractive.msettings.instruments.choices;

import java.util.Map;

/**
 * Catalog feature Abstraction for Complex Choices and option resolution
 */
public interface IGroup {

    public int getCount();
    public Map<String, String> getOptions();
    public String getOption(String index);

}
