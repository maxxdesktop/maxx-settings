package com.maxxinteractive.msettings.instruments.choices;

import com.maxxinteractive.msettings.attributes.IAttribute;

import java.util.List;
import java.util.Map;

public interface IOptionContainer {

    int getCount();

    String getOption(String index);
    void addOption(String index, String newOption);
    Map<String,String> getOptions();

    List<IAttribute> getOptionAttributes();
    void appendOptionAttributes(Map<String,IAttribute> newAttributes);
    List<String> getChoices ();


}
