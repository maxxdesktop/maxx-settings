package com.maxxinteractive.msettings.instruments.choices;

import com.maxxinteractive.msettings.attributes.AttributeBuilder;
import com.maxxinteractive.msettings.attributes.IAttribute;
import com.maxxinteractive.msettings.commons.Logger;
import org.jetbrains.annotations.NotNull;

import java.util.*;

public class OptionContainer implements IOptionContainer{

    private Map<String,String> options = new HashMap<>(16);

    public OptionContainer() {
    }

    /**
     * Constructor that receives a {@code Map<String,IAttribute>}<br>
     * @param attributes containing at least option[?]=value attribute(s)
     */
    public OptionContainer(@NotNull Map<String,IAttribute> attributes) {
        appendOptionAttributes(attributes);
    }

    /**
     * Append option[index] attributes from the passed map. This method accepts any kind of IAttribute<br>
     * however, only processes option=[index].  Any existing [index] will be overwritten
     * @param attributes ingress
     */
    public void appendOptionAttributes(@NotNull Map<String,IAttribute> attributes){
        List<String> keys = new ArrayList<>(16);
        Iterator<String> iterator = attributes.keySet().iterator();

        while (iterator.hasNext()){
            String attName = iterator.next();
            if(attName.startsWith("option[")){
                String key = getKeyFromString(attName);
                String option = attributes.get(attName).getValueAsString();
                if(getOption(key) != null) {
                    Logger.warning("option["+key+"] already present '"+getOption(key)+ "', will be overwritten with : "+ option);
                }
                options.put(key, option);
            }
        }
    }

    public int getCount(){
        return options.size();
    }

    /**
     * Get the option String representation at index.  First index=0, last=count-1
     * @param index as the index location within the array of options
     * @return IAttribute at index location.
     */
    public String getOptionAt(int index){
        return options.get(index);
    }

    /**
     * Get the option String representation at index.  First index=0, last=count-1
     * @param key as the option's index key value the array of options
     * @return IAttribute at index location.
     */
    public String getOption(@NotNull String key){
        return options.get(key);
    }

    /**
     * Provide in a List of String form the choices (same as
     * @return list of choices
     */
    public List<String> getChoices (){
        return new ArrayList<String>(options.values());
    }

    /**
     * Add an option to Option's array
     * @param key to use in the add operation
     * @param newOption to add
     */
    public void addOption(String key , String newOption){
        if(options.containsKey(key)){
            Logger.warning("Can't add an already present key : " +newOption);
            return;
        }
        options.put(key, newOption );
    }

    public Map<String,String> getOptions() {
        return options;
    }

    /**
     * Provide a sorted List of IAttribute of the Catalog's option[]
     * @return list
     */
    public List<IAttribute> getOptionAttributes(){
        List<IAttribute> attributeList = new ArrayList<>(10);

        // Sort
        List<String> keyList = new ArrayList<>(options.keySet());
        Collections.sort(keyList);

        // Construct sorted list of IAttribute
        for (String attName : keyList) {
            IAttribute attribute = AttributeBuilder.buildString("option["+attName+"]", getOption(attName));
            attributeList.add(attribute);
        }
        return attributeList;
    }

    /**
     * extract index from option[index] string
     * @param option string
     * @return found key
     */
    public static String getKeyFromString(String option){
        int start = option.indexOf("[");
        int end = option.indexOf("]");
        return option.substring(start+1,end);
    }
}
