package com.maxxinteractive.msettings.instruments.complex;

import com.maxxinteractive.msettings.instruments.Instrument;
import com.maxxinteractive.msettings.stereotype.Stereotypes;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

import static com.maxxinteractive.msettings.Constants.*;

/**
 * <strong>Represent a list of Command schema names for specific application actions such as: view, edit, etc. <br>
 * The value is used for the default action when no other actions are provided.</strong><br><br>
 *
 * → Both the default and value attributes refers to the same value, which is the 'command' parameter passed to the constructor.<br>
 */
public class Application extends Instrument {



	protected static final String[] EXTENSION_ATTRIBUTES = {  };
	protected static final String[] OPTIONAL_ATTRIBUTES  = { ATTRIBUTE_OPEN_COMMAND, ATTRIBUTE_VIEW_COMMAND, ATTRIBUTE_EDIT_COMMAND, ATTRIBUTE_PRINT_COMMAND };

	/**
	 * <strong>Create a Application Instrument</strong>
	 * @param uuid unique identifier or null if want auto-generate one
	 * @param name name of the Instrument - must be provided. ex: Desktop.Editor.Nedit
	 */
	public Application (UUID uuid, @NotNull String name) {
		this(uuid, name, "", "","","");
	}

	public Application (UUID uuid, @NotNull String name, String open, String view, String edit, String print) {
		super(uuid, name, Stereotypes.Application);
	}

}
