package com.maxxinteractive.msettings.instruments.complex;

import com.maxxinteractive.msettings.ColorSpace;
import com.maxxinteractive.msettings.attributes.IAttribute;
import com.maxxinteractive.msettings.attributes.values.FloatValue;
import com.maxxinteractive.msettings.attributes.values.StringValue;
import com.maxxinteractive.msettings.instruments.Instrument;
import com.maxxinteractive.msettings.stereotype.Stereotypes;
import org.jetbrains.annotations.NotNull;

import java.util.Map;
import java.util.UUID;

import static com.maxxinteractive.msettings.Constants.ATTRIBUTE_ALPHA;
import static com.maxxinteractive.msettings.Constants.ATTRIBUTE_COLOR_SPACE;

/**
 * <strong>Represents a Color commonly used in user's preferences. BackgroundColor is such an example.<br>
 *  Color is composed of a mandatory colorSpace and optional attribute alpha. Value is populated with matching<br>
 *  colorSpace color components separated with comma.</strong><br><br>
 *
 * → Default ColorSpace is RGB255<br>
 * → Default value is {@code {255,255,255}}<br>
 * → Default alpha is 1.0<br>
 */
public class Color extends Instrument {

	public static final float[] DEFAULT_VALUE = {255,255,255};
	public static final ColorSpace DEFAULT_COLORSPACE = ColorSpace.RGB255;
	public static final float DEFAULT_ALPHA = 1.0f;

	protected static final String[] EXTENSION_ATTRIBUTES = { ATTRIBUTE_COLOR_SPACE };
	protected static final String[] OPTIONAL_ATTRIBUTES  = { ATTRIBUTE_ALPHA};

	/**
	 * <strong>Create a Color Instrument</strong>
	 * @param uuid unique identifier or null if want auto-generate one
	 * @param name name of the Instrument - must be provided. ex: Desktop.Terminal.NormalFont
	 */
	public Color (UUID uuid, @NotNull String name) {
		this(uuid, name, DEFAULT_COLORSPACE, DEFAULT_VALUE, DEFAULT_ALPHA);
	}

	/**
	 * <strong>Create a Color Instrument</strong>
	 * @param uuid unique identifier or null if want auto-generate one
	 * @param name name of the Instrument - must be provided. ex: Desktop.Terminal.NormalFont
	 * @param colorSpace of the Instrument  - must be provided.
	 * @param components color components as float array - must be provided.  ex: {@code{1.0, 1.0, 0.0}}
	 * @param alpha component of the Instrument.
	 */
	public Color (UUID uuid, @NotNull String name, @NotNull ColorSpace colorSpace, float[] components, Float alpha) {
		super(uuid,name, Stereotypes.Color);

		setDefault(arrayToString(toFloatArray(components)));
		setColorSpace( colorSpace);

		if(alpha != null){
			setAlpha(alpha);
		}
	}

	/**
	 * <strong>Create a Color Instrument</strong>
	 * @param uuid unique identifier or null if want auto-generate one
	 * @param name name of the Instrument - must be provided. ex: Desktop.Terminal.NormalFont
	 * @param colorSpace of the Instrument  - must be provided.
	 * @param components color components as int array - must be provided.  ex: {@code{255,255,555}}
	 * @param alpha component of the Instrument.
	 */
	public Color (UUID uuid, @NotNull String name, @NotNull ColorSpace colorSpace, int[] components, Float alpha) {
		super(uuid,name, Stereotypes.Color);

		setDefault(arrayToString(toIntegerArray(components)));
		setColorSpace( colorSpace);

		if(alpha != null){
			setAlpha(alpha);
		}
	}

	/**
	 * <strong>Create a Color Instrument</strong>
	 * @param attributes for this Instrument in form of {@code Map<String, IAttribute>}
	 */
	public Color(@NotNull Map<String, IAttribute> attributes) {
		super(attributes);

		addAttribute(attributes.get(ATTRIBUTE_DEFAULT));
		addAttribute(attributes.get(ATTRIBUTE_COLOR_SPACE));
		addAttribute(attributes.get(ATTRIBUTE_ALPHA));
	}

	public String[] getExtensions(){
		return EXTENSION_ATTRIBUTES;
	}

	public String[] getOptionalAttributes (){
		return OPTIONAL_ATTRIBUTES;
	}

	/**
	 * Set default color with literal.<br>
	 * Color literal ex: 255,255,255 or 0.42,1.0, 0.0
	 * @param literal the geometry as literal
	 */
	private void setDefault(String literal){
		//TODO color literal validation
		addAttribute(ATTRIBUTE_DEFAULT, new StringValue(literal), Stereotypes.Color);
	}

	/**
	 * Set value attribute
	 * Color literal ex: 255,255,255 or 0.42,1.0, 0.0
	 * @param literal the geometry as literal
	 * @return itself for convenient setter calls chaining.
	 */
	public Color setValue(String literal){
		//TODO color literal validation
		addAttribute(ATTRIBUTE_VALUE, new StringValue(literal), Stereotypes.Color);
		return this;
	}

	/**
	 * Set value attribute
	 * Color literal ex: 255,255,255 or 0.42,1.0, 0.0
	 * @param components float array of color components
	 * @return itself for convenient setter calls chaining.
	 */
	public Color setValue(float[] components){
		//TODO color literal validation
		String value = arrayToString(toFloatArray(components));
		addAttribute(ATTRIBUTE_VALUE, new StringValue(value), Stereotypes.Color);
		return this;
	}

	/**
	 * Set value attribute
	 * Color literal ex: 255,255,255 or 0.42,1.0, 0.0
	 * @param components int array of color components
	 * @return itself for convenient setter calls chaining.
	 */
	public Color setValue(int[] components){
		//TODO color literal validation
		String value = arrayToString(toIntegerArray(components));
		addAttribute(ATTRIBUTE_VALUE, new StringValue(value), Stereotypes.Color);
		return this;
	}


	/**
	 * Set the 'alpha' attribute value
	 * @param val the alpha to set
	 * @return itself for convenient setter calls chaining.
	 */
	public Color setAlpha(@NotNull Float val){
		addAttribute(ATTRIBUTE_ALPHA, new FloatValue(val), Stereotypes.Color);
		return this;
	}

	/**
	 * Return the 'alpha' attribute value for this Instrument. There is no default value.
	 * @return the alpha component for the Color or the 1.0
	 */
	public Float getAlpha () {
		return getAttributeFloatValue(ATTRIBUTE_ALPHA, DEFAULT_ALPHA);
	}

	/**
	 * Set the 'colorSpace' attribute value
	 * @param val the colorSpace to set
	 * @return itself for convenient setter calls chaining.
	 */
	public Color setColorSpace(@NotNull ColorSpace val){
		addAttribute(ATTRIBUTE_COLOR_SPACE, new StringValue(val.name()), Stereotypes.Color);
		return this;
	}

	/**
	 * Return the 'colorSpace' attribute value for this Instrument. There is no default value.
	 * @return the color space defined for the Color or the RGB255
	 */
	public ColorSpace getColorSpace() {
		String colorSpace = getAttributeStringValue(ATTRIBUTE_COLOR_SPACE, DEFAULT_COLORSPACE.name());
		return  ColorSpace.valueOf(colorSpace);
	}

//////// Internal Helpers

	private String arrayToString(Object[] values){
		StringBuilder builder = new StringBuilder(64);
		int i = values.length;
		int c = 0;
		for(Object value : values){
			builder.append(value);
			if(++c != i){
				builder.append(',');
			}
		}
		return builder.toString();
	}

	private Float[] toFloatArray(float[] values){
		Float[] result = new Float[values.length];
		for (int i = 0; i < values.length; i++) {
			result[i] = values[i];
		}

		return result;
	}

	private Integer[] toIntegerArray(int[] values){
		Integer[] result = new Integer[values.length];
		for (int i = 0; i < values.length; i++) {
			result[i] = values[i];
		}
		return result;
	}
}
