package com.maxxinteractive.msettings.instruments.complex;

import com.maxxinteractive.msettings.attributes.AttributeBuilder;
import com.maxxinteractive.msettings.attributes.IAttribute;
import com.maxxinteractive.msettings.attributes.NullNotPresentAttribute;
import com.maxxinteractive.msettings.attributes.values.StringValue;
import com.maxxinteractive.msettings.commons.Logger;
import com.maxxinteractive.msettings.instruments.Instrument;
import com.maxxinteractive.msettings.stereotype.Stereotypes;
import org.jetbrains.annotations.NotNull;

import java.util.Map;
import java.util.StringTokenizer;
import java.util.UUID;

import static com.maxxinteractive.msettings.Constants.*;

/**
 * <strong>Instrument representing an executable Command to launch for application or script.</strong><br>
 * A Command is composed of the following optional attributes: execPath, execParams,envBinaryPath, envLibraryPath and geometry.<br>
 * The attribute 'value' is generated from a concatenation of all present attributes and cannot be set directly.<br>
 * → The 'default' attribute refers to the fully qualified form of an 'executable' with included an absolute path to the executable with optional params.<br>
 * → Fully Qualified Literal form looks like this: /usr/bin/xnedit readme.md -x something
 */
public class Command extends Instrument {

	protected static final String[] EXTENSION_ATTRIBUTES = {  };
	protected static final String[] OPTIONAL_ATTRIBUTES = { ATTRIBUTE_EXEC_NAME, ATTRIBUTE_EXEC_PATH,ATTRIBUTE_EXEC_PARAMS,
			ATTRIBUTE_ENV_BINARY_PATH, ATTRIBUTE_ENV_LIBRARY_PATH, ATTRIBUTE_GEOMETRY  };

	/**
	 * <strong>Create a Command Instrument</strong>
	 * @param uuid unique identifier or null if want auto-generate one
	 * @param name name of the Instrument - must be provided. ex: Application.WinEditor.Nedit
	 * @param executable name of the executable command - must be provided.
	 */
	public Command (UUID uuid, @NotNull String name, @NotNull String executable) {
		this(uuid, name, executable, null);
	}

	/**
	 * <strong>Create a Command Instrument</strong>
	 * @param uuid unique identifier or null if want auto-generate one
	 * @param name name of the Instrument - must be provided. ex: Application.WinEditor.Nedit
	 * @param execName name of the executable command - must be provided.
	 * @param execPath absolute path of the command.
	 */
	public Command (UUID uuid, @NotNull String name, @NotNull String execName, String execPath) {
		super(uuid, name, Stereotypes.Command);

		String tmp;
		if(execPath != null) {
			tmp = execPath+"/"+execName;
		}else {
			tmp = execName;
		}
		// remove possible double //
		tmp = tmp.replace("//", "/");
		setDefault(tmp);

		// set other attributes
		parseLiteral(tmp);
	}

	/**
	 * <strong>Create a Command Instrument</strong>
	 * @param attributes for this Instrument in form of {@code Map<String, IAttribute>}
	 */
	public Command (@NotNull Map<String, IAttribute> attributes) {
		super(attributes);


		addAttribute(attributes.get(ATTRIBUTE_DEFAULT));
		addAttribute(attributes.get(ATTRIBUTE_EXEC_NAME));
		addAttribute(attributes.get(ATTRIBUTE_EXEC_PATH));
		addAttribute(attributes.get(ATTRIBUTE_EXEC_PARAMS));
		addAttribute(attributes.get(ATTRIBUTE_ENV_BINARY_PATH));
		addAttribute(attributes.get(ATTRIBUTE_ENV_LIBRARY_PATH));
		addAttribute(attributes.get(ATTRIBUTE_GEOMETRY));
	}

	public String[] getExtensions(){
		return EXTENSION_ATTRIBUTES;
	}

	public String[] getOptionalAttributes (){
		return OPTIONAL_ATTRIBUTES;
	}

	/**
	 * Reset the 'execPath, execName and execParams' attribute based on the literal.<br>
	 * This has a similar effect as a reset, but with predefined values.
	 * The attributes: geometry, envBinaryPath and envLibraryPath wont be cleared by this operation.
	 */
	private void parseLiteral(String literal){
		// 0. validate literal
		if(!validateLiteral(literal)){
			return;
		}
		// remove possible double //
		literal = literal.replace("//", "/");

		// 1. clear existing attribute
		attributes.remove(ATTRIBUTE_EXEC_NAME);
		attributes.remove(ATTRIBUTE_EXEC_PATH);
		attributes.remove(ATTRIBUTE_EXEC_PARAMS);
	//	attributes.remove(ATTRIBUTE_ENV_BINARY_PATH);
	//	attributes.remove(ATTRIBUTE_ENV_LIBRARY_PATH);
	//	attributes.remove(ATTRIBUTE_GEOMETRY);


		// 2. parse and set each present member
		//    /usr/bin/xnedit readme.md -x something
		//    [execPath][execName] [execParams]
		String path = null;
		String exec = null;
		String params = null;
		String workingCopy = literal;

		// find space that separated [left] exec from [right] (possible) params.
		int index = workingCopy.indexOf(" ");
		if(index != -1){
			params = workingCopy.substring(index).trim();
			workingCopy = workingCopy.substring(0,index).trim();
		}else{
			exec = workingCopy;
		}

		// separate path from exec
		index = -1;
		index = workingCopy.lastIndexOf("/");
		if(index != -1){
			exec = workingCopy.substring(index+1).trim();
			path = workingCopy.substring(0,index).trim();
		}
		if(path != null) {
			addAttribute(ATTRIBUTE_EXEC_PATH, new StringValue(path), Stereotypes.Typeface);
		}
		if(exec != null) {
			addAttribute(ATTRIBUTE_EXEC_NAME, new StringValue(exec), Stereotypes.Typeface);
		}
		if(path != null) {
			addAttribute(ATTRIBUTE_EXEC_PARAMS, new StringValue(params), Stereotypes.Typeface);
		}
	}

	/**
	 * Set the Command with a fully qualified executable literal.<br>
	 * This method will reset the various components of the literal.<br>
	 * The attributes: execPath, execName and execParams will be overwritten by this operation.<br>
	 * The attributes: geometry, envBinaryPath and envLibraryPath wont be cleared by this operation.<br>
	 * Accepted format:  /usr/bin/xnedit readme.md -x something<br>
	 *                  [execPath][execName][execParams]<br>
	 * @param literal form
	 * @return itself for convenient setter calls chaining.
	 */
	public Command setCommand(String literal){
		parseLiteral(literal);
		return this;
	}

	/**
	 * Validate Freetype literal
	 * @param literal input
	 * @return true is valid
	 */
	private boolean validateLiteral(String literal){

		//    /usr/bin/xnedit readme.md -x something

		//FIXME add more validation
		boolean result = true;
		StringTokenizer stk = new StringTokenizer(literal,"/");
		if(stk.countTokens() < 1){
			Logger.error(new Exception("Bad Command literal format :"+literal));
			return false;
		}


		return result;
	}
	/**
	 * Set default attribute, which is the fontName.<br>
	 * @param val as default
	 * @return itself for convenient setter calls chaining.
	 */
	protected Command setDefault(@NotNull String val){
		addAttribute(ATTRIBUTE_DEFAULT, new StringValue(val), Stereotypes.Command);
		return this;
	}

	/**
	 * Return a calculated value based on the Command current states.
	 * @return the value IAttribute
	 */
	@Override
	public IAttribute getValue() {
		StringBuilder builder = new StringBuilder(128);

		IAttribute path = attributes.get(ATTRIBUTE_EXEC_PATH);
		IAttribute exec = attributes.get(ATTRIBUTE_EXEC_NAME);
		IAttribute params = attributes.get(ATTRIBUTE_EXEC_PARAMS);

		if(path!=null){
			builder.append(path.getValue().getValueAsString()).append("/");
		}
		if(exec!=null){
			builder.append(exec.getValue().getValueAsString());
		}
		if(params!=null){
			builder.append(" ").append(params.getValue().getValueAsString());
		}
		IAttribute attribute;
		if(builder.length() == 0){
			attribute = new NullNotPresentAttribute("notPresent");
		}else {
			attribute = AttributeBuilder.buildString("value", builder.toString());
		}
		return attribute;
	}

	/**
	 * Reset the Instrument's values to its 'default' value
	 */
	@Override
	public void resetValue(){
		IAttribute defAttribute = getAttribute(ATTRIBUTE_DEFAULT);
		if(defAttribute == null){
			Logger.warning("No default value found, aborting reset().");
			return;
		}
		parseLiteral(defAttribute.getValue().getValueAsString());
	}

	/**
	 * Set the 'execName' attribute value
	 * @param val the execName to set
	 * @return itself for convenient setter calls chaining.
	 */
	public Command setExecName(@NotNull String val){
		addAttribute(ATTRIBUTE_EXEC_NAME, new StringValue(val), Stereotypes.Command);
		return this;
	}

	/**
	 * Set the 'execPath' attribute value
	 * @param val the execPath to set
	 * @return itself for convenient setter calls chaining.
	 */
	public Command setExecPath(@NotNull String val){
		addAttribute(ATTRIBUTE_EXEC_PATH, new StringValue(val), Stereotypes.Command);
		return this;
	}

	/**
	 * Set the 'execParams' attribute value
	 * @param val the execParams to set
	 * @return itself for convenient setter calls chaining.
	 */
	public Command setExecParams(@NotNull String val){
		addAttribute(ATTRIBUTE_EXEC_PARAMS, new StringValue(val), Stereotypes.Command);
		return this;
	}

	/**
	 * Set the 'envBinaryPath' attribute value
	 * @param val the envBinaryPath to set
	 * @return itself for convenient setter calls chaining.
	 */
	public Command setEnvBinaryPath(@NotNull String val){
		addAttribute(ATTRIBUTE_ENV_BINARY_PATH, new StringValue(val), Stereotypes.Command);
		return this;
	}

	/**
	 * Set the 'envLibraryPath' attribute value
	 * @param val the envLibraryPath to set
	 * @return itself for convenient setter calls chaining.
	 */
	public Command setEnvLibraryPath(@NotNull String val){
		addAttribute(ATTRIBUTE_ENV_LIBRARY_PATH, new StringValue(val), Stereotypes.Command);
		return this;
	}

	/**
	 * Set the 'geometry' attribute value
	 * @param val the geometry to set
	 * @return itself for convenient setter calls chaining.
	 */
	public Command setGeometry(@NotNull String val){
		addAttribute(ATTRIBUTE_GEOMETRY, new StringValue(val), Stereotypes.Command);
		return this;
	}

	/**
	 * Return the 'execPath' attribute value for this Instrument. There is no default value.
	 * @return the executable path for the Command or {@code null}
	 */
	public String getExecPath () {
		return getAttributeStringValue(ATTRIBUTE_EXEC_PATH, null);
	}

	/**
	 * Return the 'execName' attribute value for this Instrument. There is no default value.
	 * @return the executable name for the Command or {@code null}
	 */
	public String getExecName () {
		return getAttributeStringValue(ATTRIBUTE_EXEC_NAME, null);
	}
	/**
	 * Return the 'execParams' attribute value for this Instrument. There is no default value.
	 * @return the executable parameter(s) for the Command or {@code null}
	 */
	public String getExecParams () {
		return getAttributeStringValue(ATTRIBUTE_EXEC_PARAMS, null);
	}

	/**
	 * Return the 'envBinaryPath' attribute value for this Instrument. There is no default value.
	 * @return the environment binary path for the Command or {@code null}
	 */
	public String getEnvBinaryPath () {
		return getAttributeStringValue(ATTRIBUTE_ENV_BINARY_PATH, null);
	}

	/**
	 * Return the 'envLibraryPath' attribute value for this Instrument. There is no default value.
	 * @return the environment library path for the Command or {@code null}
	 */
	public String getEnvLibraryPath () {
		return getAttributeStringValue(ATTRIBUTE_ENV_LIBRARY_PATH, null);
	}

	/**
	 * Return the 'geometry' attribute value for this Instrument. There is no default value.
	 * @return the geometry for the Command or {@code null}
	 */
	public String getGeometry () {
		return getAttributeStringValue(ATTRIBUTE_GEOMETRY, null);
	}
}