package com.maxxinteractive.msettings.instruments.complex;

import com.maxxinteractive.msettings.attributes.IAttribute;
import com.maxxinteractive.msettings.attributes.values.StringValue;
import com.maxxinteractive.msettings.commons.Logger;
import com.maxxinteractive.msettings.instruments.Instrument;
import com.maxxinteractive.msettings.stereotype.Stereotypes;
import com.maxxinteractive.msettings.utils.Pair;
import org.jetbrains.annotations.NotNull;

import java.util.Map;
import java.util.UUID;

/**
 * <strong>Complex Stereotype used to represent a two dimensional measurement composed of width and height as float.</strong><br><br>
 * → The version is set to 1.0, if not specified.<br>
 * → The Default value if not set via the constructor is: 1.0x1.0<br>
 */
public class Dimension extends Instrument {

	public static final String DEFAULT="1.0x1.0";

	private static final float DEFAULT_WIDTH = 1.0f;
	private static final float DEFAULT_HEIGHT= 1.0f;

	private float width;
	private float height;

	/**
	 * <strong>Create a Dimension Instrument</strong><br>
	 * @param uuid unique identifier or null if want auto-generate one
	 * @param name name of the Instrument - must be provided. ex: Desktop.Pager.ActiveRectangle
	 */
	public Dimension (UUID uuid, @NotNull String name) {
		this(uuid, name, DEFAULT_WIDTH, DEFAULT_HEIGHT);
	}

	/**
	 * <strong>Create a Dimension Instrument</strong><br>
	 * @param uuid unique identifier or null if want auto-generate one
	 * @param name name of the Instrument - must be provided. ex: Desktop.Pager.ActiveRectangle
	 * @param width dimension - must be provided.
	 * @param height dimension - must be provided.
	 */
	public Dimension (UUID uuid, @NotNull String name, @NotNull Integer width, @NotNull Integer height) {
		this(uuid, name, (float)width, (float)height);
	}
	/**
	 * <strong>Create a Dimension Instrument</strong><br>
	 * @param uuid unique identifier or null if want auto-generate one
	 * @param name name of the Instrument - must be provided. ex: Desktop.Pager.ActiveRectangle
	 * @param width dimension - must be provided.
	 * @param height dimension - must be provided.
	 */
	public Dimension (UUID uuid, @NotNull String name, @NotNull Float width, @NotNull Float height) {
		super(uuid,name, Stereotypes.Dimension);

		setDefault(getFormat(width,height));
	}

	/**
	 * <strong>Create a Dimension Instrument</strong>
	 * @param attributes for this Instrument in form of {@code Map<String, IAttribute>}
	 */
	public Dimension(@NotNull Map<String, IAttribute> attributes){
		super(attributes);

		IAttribute defaultAttr = attributes.get(ATTRIBUTE_DEFAULT);
		String defaultValue = DEFAULT;
		if(defaultAttr != null) {
			defaultValue = defaultAttr.getValueAsString();
		}
		setDefault(defaultValue);
	}

	/**
	 * Set default dimension with literal.<br>
	 * Location literal ex: 123x456
	 * @param literal the dimension as literal
	 * @return itself for convenient setter calls chaining.
	 */
	private void setDefault(String literal){
		Pair<String,String> pair = getPairFromLiteral(literal);
		if(pair == null) {
			Logger.warning("Default Attribute, Invalid Dimension Literal :" + literal);
			return ;
		}
		addAttribute(ATTRIBUTE_DEFAULT, new StringValue(literal), Stereotypes.Dimension);
	}

	/**
	 * Set value attribute with internal values set by setter.
	 * @return itself for convenient setter calls chaining.
	 */
	private void setValue(){
		String val = getFormat(width, height);
		addAttribute(ATTRIBUTE_VALUE, new StringValue(val), Stereotypes.Dimension);
	}

	/**
	 * Set dimension's values from: width and height
	 * @param width  the width
	 * @param height the height
	 * @return itself for convenient setter calls chaining.
	 */
	public Dimension setDimension(float width, float height) {
		this.width = Math.abs(width);
		this.height = Math.abs(height);
		setValue();
		return this;
	}

	/**
	 * Set dimension's values from literal.<br>
	 * Location literal ex: 123x456
	 * @param literal the dimension as literal
	 * @return itself for convenient setter calls chaining.
	 */
	public Dimension setDimension(@NotNull String literal) {
		Pair<String,String> pair = getPairFromLiteral(literal);
		if(pair == null) {
			Logger.warning("Invalid Geometry Literal :" + literal);
			return this;
		}
		width = new Float(pair.getFirst());
		height = new Float(pair.getSecond());

		setValue();
		return this;
	}

	public final float getWidth () {
		return width;
	}

	public final float getHeight () {
		return height;
	}

	/**
	 * Try to construct a {@code Pair<String,String>} from a Dimension literal form.
	 * Location literal ex: 1800.0x800.0
	 * @param literal the input string
	 * @return the pair or {@code null} if unsuccessful
	 */
	public static Pair getPairFromLiteral(@NotNull String literal){
		int index = -1;
		int len =literal.length()-1;
		for(int c = 0; c < len; c++){
			if(literal.charAt(c) == 'x' ){
				index = c;
				break;
			}
		}
		if(index == -1) {
			Logger.warning("Unable to find delimiter from input-string="+literal);
			return null;
		}
		String first = literal.substring(0, index);
		String second = literal.substring(index+1);
		Pair pair = new Pair(first,second);
		return pair;
	}

	/**
	 * Reset the Instrument's values to its 'default' value
	 */
	@Override
	public void resetValue(){
		IAttribute defAttribute = getAttribute(ATTRIBUTE_DEFAULT);
		if(defAttribute == null){
			Logger.warning("No default value found, aborting reset().");
			return;
		}
		setDimension(defAttribute.getValueAsString());
	}

	/**
	 * Dimension values to literal formatter.
	 * @param ww width as float
	 * @param hh height as float
	 * @return the literal
	 */
	public static String getFormat(float ww, float hh) {
		String format = "%.1fx%.1f";
		return String.format(format, Math.abs(ww), Math.abs(hh));
	}

	/**
	 * Dimension values to literal formatter.
	 * @param ww width as int
	 * @param hh height as int
	 * @return the literal
	 */
	public static String getFormat(int ww, int hh) {
		String format = "%dx%d";
		return String.format(format, Math.abs(ww), Math.abs(hh));
	}
}
