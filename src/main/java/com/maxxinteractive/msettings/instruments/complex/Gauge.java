package com.maxxinteractive.msettings.instruments.complex;

import com.maxxinteractive.msettings.Constants;
import com.maxxinteractive.msettings.attributes.IAttribute;
import com.maxxinteractive.msettings.attributes.values.FloatValue;
import com.maxxinteractive.msettings.attributes.values.IAttributeValue;
import com.maxxinteractive.msettings.commons.Logger;
import com.maxxinteractive.msettings.instruments.Instrument;
import com.maxxinteractive.msettings.stereotype.Stereotypes;
import org.jetbrains.annotations.NotNull;

import java.util.Map;
import java.util.UUID;

import static com.maxxinteractive.msettings.Constants.*;

/**
 * <strong>Complex Stereotype used to represent a single value measurement (as of linear scalar) according to predefined
 * minimum, maximum and an incremental value as scale.</strong><br><br>
 * Mouse Sensitivity preference is using Gauge in order to control the sensitivity within a min-max.<br>
 * → The version is set to 1.0, if not specified.<br>
 * → Default value is specific to each Gauge and must be between the minimum and maximum values.<br><br>
 *
 *   However here are the failsafe values:<br>
 *    - default is 5.0<br>
 *    - values to 1.0<br>
 *    - minimum to 1.0<br>
 *    - maximum to 10.0<br>
 *    - scale to 1.0<br>
 */
public class Gauge extends Instrument {

	public static final float MINIMUM = 1.0f;
	public static final float MAXIMUM = 10.0f;
	public static final float SCALE	 = 1.0f;
	public static final float DEFAULT = 5.0f;

	protected static final String[] EXTENSION_ATTRIBUTES = { ATTRIBUTE_MINIMUM, ATTRIBUTE_MAXIMUM, ATTRIBUTE_SCALE };
	protected static final String[] OPTIONAL_ATTRIBUTES = {  };

	/**
	 * <strong>Create a Gauge Instrument</strong>
	 * @param uuid unique identifier or null if want auto-generate one
	 * @param name name of the Instrument - must be provided. ex: Desktop.Mouse.Acceleration
	 */
	public Gauge (UUID uuid, @NotNull String name) {
		this(uuid, name, DEFAULT, MINIMUM, MINIMUM, MAXIMUM);
	}

	/**
	 * <strong>Create a Gauge Instrument</strong>
	 * @param uuid unique identifier or null if want auto-generate one
	 * @param name name of the Instrument - must be provided. ex: Desktop.Mouse.Acceleration
	 * @param defaultValue for this Gauge - must be provided.
	 */
	public Gauge (UUID uuid, @NotNull String name, @NotNull Float defaultValue) {
		this(uuid, name, defaultValue, MINIMUM, MINIMUM, MAXIMUM);
	}

	/**
	 * <strong>Create a Gauge Instrument</strong>
	 * @param uuid unique identifier or null if want auto-generate one
	 * @param name name of the Instrument - must be provided. ex: Desktop.Mouse.Acceleration
	 * @param defaultValue for this Instrument - must be provided.
	 * @param value for this Gauge - must be provided.
	 */
	public Gauge (UUID uuid, @NotNull String name, @NotNull Float defaultValue, @NotNull Float value) {
		this(uuid, name, defaultValue, value, MINIMUM, MAXIMUM);
	}
	/**
	 * <strong>Create a Gauge Instrument</strong>
	 * @param uuid unique identifier or null if want auto-generate one
	 * @param name name of the Instrument, ex: Desktop.Mouse.Acceleration
	 * @param defaultValue for this Instrument - must be provided.
	 * @param value for this Instrument - must be provided.
	 * @param minimum for this Instrument - must be provided.
	 * @param maximum for this Instrument - must be provided.
	 */
	public Gauge (UUID uuid, @NotNull String name, @NotNull Float defaultValue, @NotNull Float value, @NotNull Float minimum, @NotNull Float maximum) {
		super(uuid, name, Stereotypes.Gauge);

		setMinimum(minimum);

		setMaximum(maximum);

		setDefault(defaultValue);

		setValue(value);

		setScale(SCALE);
	}

	/**
	 * <strong>Create a Gauge Instrument</strong>
	 * @param attributes for this Instrument in form of {@code Map<String, IAttribute>}
	 */
	public Gauge (@NotNull Map<String, IAttribute> attributes) {
		super(attributes);

		addAttribute(attributes.get(ATTRIBUTE_MINIMUM));
		addAttribute(attributes.get(Constants.ATTRIBUTE_MAXIMUM));
		addAttribute(attributes.get(Constants.ATTRIBUTE_DEFAULT));
		addAttribute(attributes.get(Constants.ATTRIBUTE_VALUE));
		addAttribute(attributes.get(Constants.ATTRIBUTE_SCALE));
	}

	/**
	 * Flag that indicate if the default values can be updatable via CLI
	 * @return true or false
	 */
	public boolean isDefaultReadOnly() {
		return false;
	}

	/**
	 * Provide the mandatory extension attribute names supported by this Instrument.
	 * @return the names or empty if not specified.
	 */
	public String[] getExtensions(){
		return EXTENSION_ATTRIBUTES;
	}

	/**
	 * Provide the optional attribute names supported by this Instrument
	 * @return the names or empty if not specified.
	 */
	public String[] getOptionalAttributes (){
		return OPTIONAL_ATTRIBUTES;
	}

	@Override
	public void setValue(@NotNull IAttributeValue value){
		setValue(value.getValueAsFloat());
	}

	/**
	 * Set the 'default' values of the Gauge. The 'default' must be within the predefined 'minimum' and 'maximum' values.
	 * @param val to set.
	 * @return itself so it can be nicely chained
	 */
	public Gauge setDefault(Float val){
		Float newVal = val;
		if (!isValueInRange(val)) {
			newVal = normalizeValueWithInRange(val);
			String message = String.format(LOG_MESSAGE_OUT_OF_RANGE_FLOAT_ATTRIBUTE,  val, newVal);
			Logger.warning(buildWarningMessage(ATTRIBUTE_DEFAULT, message));
		}
		addAttribute(ATTRIBUTE_DEFAULT, new FloatValue(newVal), Stereotypes.Gauge );
		return this;
	}

	/**
	 * Set the 'values' of the Gauge Instrument. The 'values' must be within the predefined 'minimum' and 'maximum' values.
	 * @param val to set
	 * @return itself so it can be nicely chained
	 */
	public Gauge setValue(Float val){
		Float newVal = val;
		if (!isValueInRange(val)) {
			newVal = normalizeValueWithInRange(val);
			String message = String.format(LOG_MESSAGE_OUT_OF_RANGE_FLOAT_ATTRIBUTE,  val, newVal);
			Logger.warning(buildWarningMessage(ATTRIBUTE_VALUE, message));
		}
		addAttribute(ATTRIBUTE_VALUE, new FloatValue(newVal), Stereotypes.Gauge );
		return this;
	}

	/**
	 * Utility to validate if the values is within the set minimum and maximum, otherwise using the standard MINIMUM and MAXIMUM
	 * @param val index to evaluate
	 * @return true if within the minimum and maximum.
	 */
	public boolean isValueInRange (Float val){
		float min = getAttributeFloatValue(ATTRIBUTE_MINIMUM, MINIMUM);
		float max = getAttributeFloatValue(ATTRIBUTE_MAXIMUM, MAXIMUM);

		if(val.floatValue() >= min && val.floatValue() <= max) {
			return true;
		}
		return false;
	}

	/**
	 * Utility to fix the values if outside of the range of allowed values. A celling() or floor() like logic is applied to the values.
	 * @param val to normalize
	 * @return the fix values is outside of range, or same if within.
	 */
	public Float normalizeValueWithInRange (Float val){
		float min = getAttributeFloatValue(ATTRIBUTE_MINIMUM, MINIMUM);
		float max = getAttributeFloatValue(ATTRIBUTE_MAXIMUM, MAXIMUM);

		if(val <  min ) {
			return min;
		}
		if(val > max ) {
			return max;
		}
		return val;
	}

	/**
	 * Specify the mandatory 'minimum' attribute with a runtime value, a.k.a. an User Preference.
	 * @param val for 'minimum' attribute. There is no default
	 * @return itself for convenient setter calls chaining.
	 */
	public Gauge setMinimum(Float val){
		addAttribute(ATTRIBUTE_MINIMUM, new FloatValue(val), Stereotypes.Gauge );
		return this;
	}

	/**
	 * Specify the mandatory 'maximum' attribute with a runtime value, a.k.a. an User Preference.
	 * @param val for 'maximum' attribute. There is no default
	 * @return itself for convenient setter calls chaining.
	 */
	public Gauge setMaximum(Float val){
		addAttribute(ATTRIBUTE_MAXIMUM, new FloatValue(val), Stereotypes.Gauge );
		return this;
	}

	/**
	 * Specify the mandatory 'scale' attribute with a runtime value, a.k.a. an User Preference.
	 * @param val for 'scale' attribute. There is no default
	 * @return itself for convenient setter calls chaining.
	 */
	public Gauge setScale(Float val){
		if(val == null){
			val = SCALE;
		}
		addAttribute(ATTRIBUTE_SCALE, new FloatValue(val), Stereotypes.Gauge );
		return this;
	}

	/**
	 * Return the 'minimum' attribute value for this Instrument. The default value is 1.0f for this attribute.
	 * @return the minimum value.
	 */
	public float getMinimum () {
		return getAttributeFloatValue(ATTRIBUTE_MINIMUM, MINIMUM);
	}

	/**
	 * Return the 'maximum' attribute value for this Instrument. The default value is 10.0f for this attribute.
	 * @return the maximum value.
	 */
	public float getMaximum() {
		return getAttributeFloatValue(ATTRIBUTE_MAXIMUM, MAXIMUM);
	}

	/**
	 * Return the 'scale' attribute value for this Instrument. The default value is 1.0f for this attribute.
	 * @return the scale value.
	 */
	public float getScale () {
		return getAttributeFloatValue(ATTRIBUTE_SCALE, SCALE);
	}
}