package com.maxxinteractive.msettings.instruments.complex;

import com.maxxinteractive.msettings.attributes.IAttribute;
import com.maxxinteractive.msettings.attributes.values.StringValue;
import com.maxxinteractive.msettings.commons.Logger;
import com.maxxinteractive.msettings.instruments.Instrument;
import com.maxxinteractive.msettings.stereotype.Stereotypes;
import com.maxxinteractive.msettings.utils.Pair;
import org.jetbrains.annotations.NotNull;

import java.util.Map;
import java.util.UUID;

/**
 * <strong>Instrument representing a geometry in 2D space and made of a Dimension and Location</strong><br>
 * → The Default value if not set via the constructor is: 1x1+0+0<br>
 */
public class Geometry extends Instrument {

	public static final String DEFAULT="1x1+0+0";

	private static final int DEFAULT_WIDTH = 1;
	private static final int DEFAULT_HEIGHT= 1;
	private static final int DEFAULT_X = 0;
	private static final int DEFAULT_Y = 0;

	private int width;
	private int height;
	private int x;
	private int y;

	/**
	 * <strong>Create a Geometry Instrument</strong><br>
	 * @param uuid unique identifier or null if want auto-generate one
	 * @param name name of the Instrument - must be provided. ex: Desktop.Overview.WindowSize
	 */
	public Geometry (UUID uuid, @NotNull String name) {
		this(uuid, name, DEFAULT_WIDTH, DEFAULT_HEIGHT, DEFAULT_X, DEFAULT_Y);
	}

	/**
	 * <strong>Create a Geometry Instrument</strong><br>
	 * @param uuid unique identifier or null if want auto-generate one
	 * @param name name of the Instrument - must be provided. ex: Desktop.Overview.WindowSize
	 * @param width default dimension - must be provided.
	 * @param height default dimension - must be provided.
	 * @param x default dimension - must be provided.
	 * @param y default dimension - must be provided.
	 */
	public Geometry (UUID uuid, @NotNull String name, @NotNull Integer width, @NotNull Integer height, @NotNull Integer x, @NotNull Integer y){
		super(uuid, name, Stereotypes.Geometry);

		setDefault( getFormat(width, height,x,y) );
	}
	/**
	 * <strong>Create a Geometry Instrument</strong>
	 * @param attributes for this Instrument in form of {@code Map<String, IAttribute>}
	 */
	public Geometry(@NotNull Map<String, IAttribute> attributes){
		super(attributes);

		IAttribute defaultAttr = attributes.get(ATTRIBUTE_DEFAULT);
		String defaultValue = DEFAULT;
		if(defaultAttr != null) {
			defaultValue =defaultAttr.getValueAsString();
		}
		setDefault(defaultValue);
	}

	/**
	 * Set default geometry with literal.<br>
	 * Location literal ex: 320x200+1000+400
	 * @param literal the geometry as literal
	 */
	private void setDefault(String literal){
		//TODO validate literal
		addAttribute(ATTRIBUTE_DEFAULT, new StringValue(literal), Stereotypes.Geometry);
	}

	/**
	 * Set value attribute with internal values set by setter.
	 * @return itself for convenient setter calls chaining.
	 */
	private void setValue(){
		String loc = Location.getFormat(x, y);
		String size = Dimension.getFormat(width, height);
		addAttribute(ATTRIBUTE_VALUE, new StringValue(size+loc), Stereotypes.Geometry);
	}

	/**
	 * Set geometry's values from literal.<br>
	 * @param width the width
	 * @param height with height
	 * @param x X
	 * @param y Y
	 * @return itself for convenient setter calls chaining.
	 */
	public Geometry setGeometry (int width, int height, int x, int y) {
		this.width = width;
		this.height = height;
		this.x = x;
		this.y = y;

		setValue();
		return this;
	}

	/**
	 * Set geometry's values from literal.<br>
	 * Location literal ex: 123x456
	 * @param literal the dimension as literal
	 * @return itself for convenient setter calls chaining.
	 */
	public Geometry setGeometry(@NotNull String literal) {
		int index = -1;
		int len =literal.length()-1;
		for(int c = 0; c < len; c++){
			if(literal.charAt(c) == '+' || literal.charAt(c) == '-' ){
				index = c;
				break;
			}
		}
		if(index == -1) {
			Logger.warning("Invalid Geometry literal :"+ literal);
			return null;
		}

		String dimension = literal.substring(0,index);
		String location = literal.substring(index);

		Pair<String,String> dimPair = Dimension.getPairFromLiteral(dimension);
		Pair<String,String> locPair = Location.getPairFromLiteral(location);
		if(dimPair == null || locPair == null){
			Logger.warning("Invalid Geometry literal :"+ literal);
			return this;
		}

		width = new Float(dimPair.getFirst()).intValue();
		height = new Float(dimPair.getSecond()).intValue();
		x = new Integer(locPair.getFirst());
		y = new Integer(locPair.getSecond());

		setValue();
		return this;
	}

	/**
	 * Reset the Instrument's values to its 'default' value
	 */
	@Override
	public void resetValue(){
		IAttribute defAttribute = getAttribute(ATTRIBUTE_DEFAULT);
		if(defAttribute == null){
			Logger.warning("No default value found, aborting reset().");
			return;
		}
		setGeometry(defAttribute.getValueAsString());
	}

	/**
	 * Get intermediary width value, for convenience purpose
	 * @return the width value
	 */
	public int getWidth () {
		return width;
	}

	/**
	 * Get intermediary height value, for convenience purpose
	 * @return the width value
	 */
	public int getHeight () {
		return height;
	}

	/**
	 * Get intermediary X value, for convenience purpose
	 * @return the width value
	 */
	public int getX () {
		return x;
	}

	/**
	 * Get intermediary Y value, for convenience purpose
	 * @return the width value
	 */
	public int getY () {
		return y;
	}

	/**
	 * Goemtry values to literal formatter.
	 * @param width as width
	 * @param height as height
	 * @param xx X as int
	 * @param yy Y as int
	 * @return the literal
	 */
	public static String getFormat(int width, int height, int xx, int yy) {
		String dim = Dimension.getFormat(width, height);
		String loc = Location.getFormat(xx, yy);
		return dim+loc;
	}
}