package com.maxxinteractive.msettings.instruments.complex;

import com.maxxinteractive.msettings.instruments.Instrument;
import com.maxxinteractive.msettings.stereotype.Stereotypes;

import java.util.UUID;

/**
 * <strong>Represents an Image user's preferences. BackgroundImage is such an example. Image is composed of a mandatory filePath <br>
 *  and the optional attribute resizeTo which can be used to apply a transformation on top of the original size. </strong><br><br>
 *
 * →
 */
public class Image extends Instrument {

	public static final String ATTRIBUTE_FILE_PATH	= "filePath";
	public static final String ATTRIBUTE_DIMENSION	= "dimension";
	public static final String ATTRIBUTE_CROP		= "crop";
	public static final String ATTRIBUTE_RESIZE_TO	= "resizeTo";

	protected static final String[] EXTENSION_ATTRIBUTES = { ATTRIBUTE_FILE_PATH };
	protected static final String[] OPTIONAL_ATTRIBUTES  = { ATTRIBUTE_DIMENSION, ATTRIBUTE_CROP, ATTRIBUTE_RESIZE_TO };

	public Image (UUID uuid, String name) {
		super(uuid, name, Stereotypes.Image);
	}
}
