package com.maxxinteractive.msettings.instruments.complex;

import com.maxxinteractive.msettings.attributes.IAttribute;
import com.maxxinteractive.msettings.attributes.values.StringValue;
import com.maxxinteractive.msettings.commons.Logger;
import com.maxxinteractive.msettings.instruments.Instrument;
import com.maxxinteractive.msettings.stereotype.Stereotypes;
import com.maxxinteractive.msettings.utils.Pair;
import org.jetbrains.annotations.NotNull;

import java.util.Map;
import java.util.UUID;

/**
 * <strong>Instrument representing a Location in 2D space</strong>
 * → The version is set to 1.0, if not specified.<br>
 * → The Default value if not set via the constructor is: +0+0<br>
 */
public class Location extends Instrument {

	public static final String DEFAULT="+0+0";

	private static final int DEFAULT_X = 0;
	private static final int DEFAULT_Y = 0;
	private static final int EMPTY = 424242138;

	private int x;
	private int y;

	/**
	 * <strong>Create a Location Instrument</strong><br>
	 * @param uuid unique identifier or null if want auto-generate one
	 * @param name name of the Instrument - must be provided. ex: Desktop.Workspace.TrashCan
	 */
	public Location (UUID uuid, @NotNull String name) {
		this(uuid, name, DEFAULT_X, DEFAULT_Y);
	}

	/**
	 * <strong>Create a Location Instrument</strong><br>
	 * @param uuid unique identifier or null if want auto-generate one
	 * @param name name of the Instrument - must be provided. ex: Desktop.Workspace.TrashCan
	 * @param dx default x location - must be provided.
	 * @param dy default y location - must be provided.
	 */
	public Location (UUID uuid, @NotNull String name, @NotNull Integer dx, @NotNull Integer dy) {
		super(uuid,name, Stereotypes.Location);
		setDefault(getFormat(dx,dy));
	}

	/**
	 * <strong>Create a Location Instrument</strong>
	 * @param attributes for this Instrument in form of {@code Map<String, IAttribute>}
	 */
	public Location(@NotNull Map<String, IAttribute> attributes){
		super(attributes);

		IAttribute defaultAttr = attributes.get(ATTRIBUTE_DEFAULT);
		String defaultValue = DEFAULT;
		if(defaultAttr != null) {
			defaultValue = defaultAttr.getValueAsString();
		}
		setDefault(defaultValue);
	}

	/**
	 * Set default location with literal form : +1800+800
	 * @param literal the dimension as literal
	 * @return itself for convenient setter calls chaining.
	 */
	private void setDefault(String literal){
		Pair<String,String> pair = getPairFromLiteral(literal);
		if(pair == null) {
			Logger.warning("Default Attribute, Invalid Location Literal :" + literal);
			return ;
		}
		addAttribute(ATTRIBUTE_DEFAULT, new StringValue(literal), Stereotypes.Location);
	}

	/**
	 * Set value attribute with internal values set by setter.
	 * @return itself for convenient setter calls chaining.
	 */
	private void setValue(){
		String val = getFormat(x,y);
		addAttribute(ATTRIBUTE_VALUE, new StringValue(val), Stereotypes.Location);
	}

	/**
	 * Set location's values from: x and y
	 * @param x  the X
	 * @param y the Y
	 * @return itself for convenient setter calls chaining.
	 */
	public Location setLocation (int x, int y) {
		this.x = x;
		this.y = y;

		setValue();
		return this;
	}

	/**
	 * Set location's values from literal.<br>
	 * Location literal ex: +1800+800, +1000-12, -112-100, -123123+12
	 * @param literal the dimension as literal
	 * @return itself for convenient setter calls chaining.
	 */
	public Location setLocation(@NotNull String literal){
		Pair<String,String> pair = getPairFromLiteral(literal);
		if(pair == null) {
			Logger.warning("Invalid Location Literal :" + literal);
			return this;
		}
		x = new Integer(pair.getFirst());
		y = new Integer(pair.getSecond());

		setValue();
		return this;
	}

	/**
	 * Reset the Instrument's values to its 'default' value
	 */
	@Override
	public void resetValue(){
		IAttribute defAttribute = getAttribute(ATTRIBUTE_DEFAULT);
		if(defAttribute == null){
			Logger.warning("No default value found, aborting reset().");
			return;
		}
		setLocation(defAttribute.getValueAsString());
	}

	public final int getX () {
		return x;
	}

	public final int getY () {
		return y;
	}

	/**
	 * Try to construct a {@code Pair<String,String>} from a Location literal.<br>
	 * Location literal ex: +1800+800, +1000-12, -112-100, -123123+12
	 * @param literal the input string
	 * @return the pair or {@code null} if unsuccessful
	 */
	public static Pair getPairFromLiteral(@NotNull String literal){
		int index = -1;
		int len =literal.length();
		int c = len-1;
		while(c >= 0 ) {
			if(literal.charAt(c) == '+' || literal.charAt(c) == '-' ){
				index = c;
				break;
			}
			c--;
		}
		if(index == -1) {
			Logger.warning("Unable to find delimiter from input-string="+literal);
			return null;
		}
		String first = literal.substring(0, index);
		String second = literal.substring(index);
		Pair pair = new Pair(first,second);
		return pair;
	}

	/**
	 * Location values to literal formatter.
	 * @param xx X as int
	 * @param yy Y as int
	 * @return the literal
	 */
	public static String getFormat(int xx, int yy) {
		String xSign = xx>=0 ? "+":"";
		String ySign = yy>=0 ? "+":"";
		String format = "%s%d%s%d";
		return String.format(format,xSign, xx, ySign, yy);
	}
}