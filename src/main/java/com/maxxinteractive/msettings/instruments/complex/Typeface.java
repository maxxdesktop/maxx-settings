package com.maxxinteractive.msettings.instruments.complex;

import com.maxxinteractive.msettings.attributes.AttributeBuilder;
import com.maxxinteractive.msettings.attributes.IAttribute;
import com.maxxinteractive.msettings.attributes.NullNotPresentAttribute;
import com.maxxinteractive.msettings.attributes.values.FloatValue;
import com.maxxinteractive.msettings.attributes.values.StringValue;
import com.maxxinteractive.msettings.commons.Logger;
import com.maxxinteractive.msettings.instruments.Instrument;
import com.maxxinteractive.msettings.stereotype.Stereotypes;
import com.maxxinteractive.msettings.utils.KeyValuePair;
import org.jetbrains.annotations.NotNull;

import java.util.Map;
import java.util.Optional;
import java.util.StringTokenizer;
import java.util.UUID;

import static com.maxxinteractive.msettings.Constants.*;

/**
 * <strong>Instrument representing a Typeface, aka Font, as an user preference .</strong><br><br>
 * TerminalFont is such an example. Typeface is composed of mandatory font name and a<br>
 * size and optional style, weight and slant attributes.<br>
 * The attribute 'value' is generated from a concatenation of all present attributes and cannot be set directly.<br>
 * → The 'default' attribute represent a fully qualified Typeface form.<br>
 * → Fully qualified form looks like this: Noto:size=10:style=bold:slant=Italic:weight=Medium
 */
public class Typeface extends Instrument {

	public static final String 	DEFAULT_FONT_NAME	= "Sans";
	public static final Float 	DEFAULT_FONT_SIZE	= 10.5f;

	protected static final String[] EXTENSION_ATTRIBUTES = { };
	protected static final String[] OPTIONAL_ATTRIBUTES  = {  ATTRIBUTE_FONT, ATTRIBUTE_SIZE,ATTRIBUTE_STYLE, ATTRIBUTE_WEIGHT, ATTRIBUTE_SLANT };


	/**
	 * <strong>Create a Typeface Instrument</strong>
	 * @param uuid unique identifier or null if want auto-generate one
	 * @param name name of the Instrument - must be provided. ex: Desktop.Terminal.NormalFont
	 */
	public Typeface (UUID uuid, @NotNull String name) {
		this(uuid, name, DEFAULT_FONT_NAME, DEFAULT_FONT_SIZE);
	}
	/**
	 * <strong>Create a Typeface Instrument</strong>
	 * @param uuid unique identifier or null if want auto-generate one
	 * @param name of the Instrument - must be provided. ex: Desktop.Terminal.NormalFont
	 * @param font of the default font name for the Instrument - must be provided.
	 * @param size of the default size for the Instrument - must be provided.
	 */
	public Typeface (UUID uuid, @NotNull String name, @NotNull String font, @NotNull Float size){
		super(uuid, name, Stereotypes.Typeface);

		setDefault(font+":size="+size);
	}

	/**
	 * <strong>Create a Typeface Instrument</strong>
	 * @param attributes for this Instrument in form of {@code Map<String, IAttribute>}
	 */
	public Typeface(@NotNull Map<String, IAttribute> attributes) {
		super(attributes);

		addAttribute(attributes.get(ATTRIBUTE_DEFAULT));
		addAttribute(attributes.get(ATTRIBUTE_FONT));
		addAttribute(attributes.get(ATTRIBUTE_SIZE));
		addAttribute(attributes.get(ATTRIBUTE_STYLE));
		addAttribute(attributes.get(ATTRIBUTE_WEIGHT));
		addAttribute(attributes.get(ATTRIBUTE_SLANT));
	}

	public String[] getExtensions(){
		return EXTENSION_ATTRIBUTES;
	}

	public String[] getOptionalAttributes (){
		return OPTIONAL_ATTRIBUTES;
	}

	/**
	 * Reset the 'value' attribute based on the literal. This will have the same effect as a reset.
	 */
	private void parseLiteral(String literal){
		// 0. validate literal
		if(!validateLiteral(literal)){
			return;
		}

		// 1. clear existing attribute
		attributes.remove(ATTRIBUTE_FONT);
		attributes.remove(ATTRIBUTE_SIZE);
		attributes.remove(ATTRIBUTE_STYLE);
		attributes.remove(ATTRIBUTE_WEIGHT);
		attributes.remove(ATTRIBUTE_SLANT);

		// 2. parse and set each present member
		// Noto:size=10:style=bold:slant=Italic:weight=Medium
		StringTokenizer stk = new StringTokenizer(literal,":");
		if(stk.countTokens() < 1){
			Logger.warning("Bad Typeface literal format :"+literal);
		}
		// Font
		String val = stk.nextToken();
		addAttribute( AttributeBuilder.buildString(ATTRIBUTE_FONT, val));

		while(stk.hasMoreElements()){
			Optional<KeyValuePair> keyPair = KeyValuePair.buildForLine(stk.nextToken());
			if(keyPair.isPresent()) {
				addAttribute(AttributeBuilder.buildString(keyPair.get().getKey(), keyPair.get().getValue()));
			}
		}
	}

	/**
	 * Set the Typeface with a fully qualified Freetype literal.<br>
	 * This method will reset the various components of the literal.
	 * Accepted format: Noto:size=10:style=bold:slant=Italic:weight=Medium
	 * @param literal form
	 * @return itself for convenient setter calls chaining.
	 */
	public Typeface setTypeface( @NotNull String literal){
		parseLiteral(literal);
		return this;
	}

	/**
	 * Validate Freetype literal
	 * @param literal input
	 * @return true is valid
	 */
	private boolean validateLiteral(String literal){
		boolean result = true;
		StringTokenizer stk = new StringTokenizer(literal,":");
		if(stk.countTokens() < 1){
			Logger.error(new Exception("Bad Typeface literal format :"+literal));
			return false;
		}
		//TODO add more validation
		String font = stk.nextToken();
		if(font.contains(":") || font.contains("=")){
			return false;
		}
		String keys = "style size weight slant";
		while(stk.hasMoreElements()){
			Optional<KeyValuePair> keyPair = KeyValuePair.buildForLine(stk.nextToken());
			if(keyPair.isPresent()) {
				String key = keyPair.get().getKey();
				if(!keys.contains(key)){
					Logger.error(new Exception("Bad Typeface literal format. Key= "+key + " is not valid"));
					return false;
				}
			}
		}
		return result;
	}

	/**
	 * Return a calculated value based on the Typeface current states.
	 * @return the value IAttribute
	 */
	@Override
	public IAttribute getValue() {
		StringBuilder builder = new StringBuilder(128);

		IAttribute val = attributes.get(ATTRIBUTE_FONT);
		if(val!=null){
			builder.append(val.getValueAsString()).append(":");
		}

		val = attributes.get(ATTRIBUTE_SIZE);
		if(val!=null){
			builder.append(val.getName()).append("=").append(val.getValueAsString());
		}
		val = attributes.get(ATTRIBUTE_STYLE);
		if(val!=null){
			builder.append(":").append(val.getName()).append("=").append(val.getValueAsString());
		}
		val = attributes.get(ATTRIBUTE_WEIGHT);
		if(val!=null){
			builder.append(":").append(val.getName()).append("=").append(val.getValueAsString());
		}
		val =attributes.get(ATTRIBUTE_SLANT);
		if(val!=null){
			builder.append(":").append(val.getName()).append("=").append(val.getValueAsString());
		}
		IAttribute attribute;
		if(builder.length() == 0){
			attribute = new NullNotPresentAttribute("notPresent");
		}else {
			attribute =AttributeBuilder.buildString("value", builder.toString());
		}
		return attribute;
	}

	/**
	 * Set the Typeface's 'default' attribute from the fully qualified form.<br>
	 * Validation will occur internally.
	 * @param value as default
	 */
	private void setDefault(@NotNull String value){
		if(validateLiteral(value)) {
			addAttribute(ATTRIBUTE_DEFAULT, new StringValue(value), Stereotypes.Typeface);
		}
	}

	/**
	 * Reset the Instrument's values to its 'default' value
	 */
	@Override
	public void resetValue(){
		IAttribute defAttribute = getAttribute(ATTRIBUTE_DEFAULT);
		if(defAttribute == null){
			Logger.warning("No default value found, aborting reset().");
			return;
		}
		parseLiteral(defAttribute.getValue().getValueAsString());
	}

	/**
	 * Specify the mandatory 'font' attribute with a runtime value, a.k.a. an User Preference.
	 * @param font as Font name. The Font name should not include any other component like style or size. Use the respective setters for that purpose.
	 * @return itself for convenient setter calls chaining.
	 */
	public Typeface setFont(@NotNull  String font){
		addAttribute(ATTRIBUTE_FONT, new StringValue(font), Stereotypes.Typeface);
		return this;
	}

	/**
	 * Return the 'font' attribute value for this Instrument.
	 * @return the font name for the Typeface
	 */
	public String getFont() {
		return getAttributeStringValue(ATTRIBUTE_FONT, null);
	}

	/**
	 * Set the 'size' attribute value for this Instrument. There is no default value.
	 * @param size set the font name for the Typeface or DEFAULT_NAME
	 * @return itself for convenient setter calls chaining.
	 */
	public Typeface setSize(@NotNull Float size){
		addAttribute(ATTRIBUTE_SIZE, new FloatValue(size), Stereotypes.Typeface);
		return this;
	}

	/**
	 * Set the 'size' attribute value for this Instrument. There is no default value.
	 * @param size set the font name for the Typeface or DEFAULT_NAME
	 * @return itself for convenient setter calls chaining.
	 */
	public Typeface setSize(@NotNull Integer size){
		addAttribute(ATTRIBUTE_SIZE, new FloatValue(size), Stereotypes.Typeface);
		return this;
	}

	/**
	 * Return the 'size' attribute value for this Instrument. There is no default value.
	 * @return the font size for the Typeface or the DEFAULT_SIZE
	 */
	public Float getSize () {
		return getAttributeFloatValue(ATTRIBUTE_SIZE, null);
	}

	/**
	 * Set the style
	 * @param style Typeface to use. Could be bold, normal or italic
	 * @return itself for convenient setter calls chaining.
	 */
	public Typeface setStyle(@NotNull  String style){
		addAttribute(ATTRIBUTE_STYLE, new StringValue(style), Stereotypes.Typeface);
		return this;
	}

	/**
	 * Return the 'style' attribute value for this Instrument. There is no default value.
	 * @return the font style for the Typeface or null
	 */
	public String getStyle () {
		return getAttributeStringValue(ATTRIBUTE_STYLE, null);
	}

	public Typeface setWeight(@NotNull  String style){
		addAttribute(ATTRIBUTE_WEIGHT, new StringValue(style), Stereotypes.Typeface);
		return this;
	}

	/**
	 * Return the 'weight' attribute value for this Instrument. There is no default value.
	 * @return the font weight for the Typeface or null
	 */
	public String getWeight () {
		return getAttributeStringValue(ATTRIBUTE_WEIGHT, null);
	}

	public Typeface setSlant(@NotNull  String style){
		addAttribute(ATTRIBUTE_SLANT, new StringValue(style), Stereotypes.Typeface);
		return this;
	}

	/**
	 * Return the 'slant' attribute value for this Instrument. There is no default value.
	 * @return the font slant for the Typeface or null
	 */
	public String getSlant () {
		return getAttributeStringValue(ATTRIBUTE_SLANT, null);
	}
}