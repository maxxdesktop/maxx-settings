package com.maxxinteractive.msettings.preferences;

import com.maxxinteractive.msettings.attributes.IAttribute;
import com.maxxinteractive.msettings.attributes.values.IAttributeValue;
import com.maxxinteractive.msettings.schema.ISchema;

public interface IUserPreference extends ISchema {

    /**
     * Return the corresponding to the Schema's 'default' Attribute
     * @return the default values IAttribute
     */
    IAttribute getDefault();

    /**
     * Return the corresponding to the Schema's 'values' Attribute
     * @return the default values IAttribute
     */
    IAttribute getValue();

    /**
     * Set the Instrument's 'value' attribute.
     * @param attribute value for the 'value' attribute
     */
    void setValue(IAttributeValue attribute);

    /**
     * Reset the Instrument's values to its 'default' values
     */
    void resetValue();

    /**
     * Convert the value into its lowest possible form.<br>
     * For example, a Choice value is the the index of its option[] array. In this case<br>
     * the resolved value would be the corresponding option[value] text form.<br>
     * @return resolved value
     */
    String resolve();

}

