package com.maxxinteractive.msettings.schema;

import com.maxxinteractive.msettings.Constants;
import com.maxxinteractive.msettings.stereotype.Stereotypes;

import java.util.UUID;

/**
 * Schema Abstraction
 */
public interface ISchema {

	public static final String ATTRIBUTE_VERSION = Constants.ATTRIBUTE_VERSION;
	public static final String ATTRIBUTE_UUID = Constants.ATTRIBUTE_UUID;
	public static final String ATTRIBUTE_STEREOTYPE = Constants.ATTRIBUTE_STEREOTYPE;
	public static final String ATTRIBUTE_NAME = Constants.ATTRIBUTE_NAME;
	public static final String ATTRIBUTE_DEFAULT = Constants.ATTRIBUTE_DEFAULT;
	public static final String ATTRIBUTE_VALUE = Constants.ATTRIBUTE_VALUE;

	UUID getUUID();

	String getVersion();

	String getName();

	Stereotypes getStereotype();

	String getFilename();

	String serialize();
}

