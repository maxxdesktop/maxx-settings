package com.maxxinteractive.msettings.schema;

import com.maxxinteractive.msettings.attributes.AttributeBuilder;
import com.maxxinteractive.msettings.attributes.IAttribute;
import com.maxxinteractive.msettings.attributes.StringAttribute;
import com.maxxinteractive.msettings.attributes.UUIDAttribute;
import com.maxxinteractive.msettings.attributes.values.IAttributeValue;
import com.maxxinteractive.msettings.attributes.values.UUIDValue;
import com.maxxinteractive.msettings.commons.Logger;
import com.maxxinteractive.msettings.stereotype.Stereotypes;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;

import static com.maxxinteractive.msettings.Constants.VERSION_10;

/**
 * @author emasson
 * <strong>MaXX Settings Schema implementation.</strong>
 */
public class Schema implements ISchema {

	protected static final String[] SCHEMA_ATTRIBUTES = { ATTRIBUTE_VERSION,
														  ATTRIBUTE_UUID,
			 											  ATTRIBUTE_STEREOTYPE,
														  ATTRIBUTE_NAME };
	protected Map<String, IAttribute> attributes;
	protected Stereotypes stereotype;

	/**
	 * Create a Schema - version will be set the default values of 1.0
	 * @param uuid unique identifier or null if want auto-generate one
	 * @param name name of the Schema - must be provided. ex: LeftHand
	 * @param stereotype Stereotype describing this Schema - must be provided. ex: Logical
	 */
	protected Schema(UUID uuid, @NotNull String name, @NotNull Stereotypes stereotype) {
		this(uuid,name, stereotype, VERSION_10);
	}

	/**
	 * Create a Schema
	 * @param uuid unique identifier or null if want auto-generate one
	 * @param name name of the Schema, ex: LeftHand
	 * @param stereotype Stereotype describing this Schema, ex: Bool
	 * @param version schema's version
	 */
	protected Schema(UUID uuid, @NotNull String name, @NotNull Stereotypes stereotype, @NotNull String version ) {
		if(uuid == null){
			uuid = UUID.randomUUID();
		}
		// init storage
		attributes = new HashMap<>(16);
		// cache stereotype for convenience
		this.stereotype = stereotype;

		attributes.put(ATTRIBUTE_UUID, new UUIDAttribute(ATTRIBUTE_UUID, uuid) );
		attributes.put(ATTRIBUTE_NAME, new StringAttribute(ATTRIBUTE_NAME, name) );
		attributes.put(ATTRIBUTE_STEREOTYPE, new StringAttribute(ATTRIBUTE_STEREOTYPE, stereotype.name()) );
		attributes.put(ATTRIBUTE_VERSION, new StringAttribute(ATTRIBUTE_VERSION, version) );
	}

	/**
	 * Generic Attribute Getter method. Will return the found IAttribute or {@code null} if not found
	 * @param attributeName name of the Attribute to fetch.
	 * @return the IAttribute or {@code null} is not present
	 */
	public IAttribute getAttribute(String attributeName) {
		return attributes.get(attributeName);
	}

	/**
	 * Return the raw UUID corresponding to the Schema's 'uuid' Attribute
	 * @return uuid Attribute's values as a {@code UUID}
	 */
	@Override
	public UUID getUUID() {
		UUID uuid = null;
		IAttribute attribute =  getAttribute(ATTRIBUTE_UUID);
		if(attribute != null){
			uuid = ((UUIDValue)attribute.getValue()).getValue();
		}
		return uuid;
	}

	/**
	 * Return the corresponding to the Schema's 'version' Attribute
	 * @return version Attribute's values as a String
	 */
	@Override
	public String getVersion() {
		return getAttributeStringValue(ATTRIBUTE_VERSION);
	}

	/**
	 * Return the corresponding to the Schema's 'name' Attribute
	 * @return name Attribute's values as a String
	 */
	@Override
	public String getName() {
		return getAttributeStringValue(ATTRIBUTE_NAME);
	}

	/**
	 * Return the corresponding to the Schema's 'stereotype'
	 * @return  Stereotype.
	 */
	@Override
	public Stereotypes getStereotype() {
		String sType = getAttributeStringValue(ATTRIBUTE_STEREOTYPE);
		if(sType != null) {
			return Stereotypes.valueOf(sType);
		}
		return Stereotypes.Undefined;
	}

	/**
	 * Add an Attribute to internal container
	 * @param attributeName name of the Attribute
	 * @param schemaStereotype Stereotype of the Schema for this Attribute
	 * @param attributeValue the values to be assigned to the Attribute
	 */
	protected void addAttribute(String attributeName, IAttributeValue attributeValue, Stereotypes schemaStereotype){
		// validate inputs
		if(attributeName == null || attributeValue == null || schemaStereotype == null) {
			Logger.warning(String.format("Failed to add Attribute '%s'. Null parameter.", attributeName));
			return;
		}
		// generate attributes
		IAttribute attr = AttributeBuilder.buildWithAttributeValue(attributeName, attributeValue, schemaStereotype);
		if(attr == null){
			Logger.warning(String.format("Failed to set Value for Attribute '%s'. Possible mismatch between the Stereotype and Value Type.", attributeName));
			return;
		}
		// add it to the container
		attributes.put(attributeName,attr);
	}

	/**
	 * Add an Attribute to internal container
	 * @param attribute the values assigned to the Attribute
	 */
	protected void addAttribute(IAttribute attribute){
		// validate inputs
		if(attribute == null) {
			//TODO fix logging to use new methods
			//System.err.println("error: Failed to add Attribute. Null value received.");
			return;
		}
		// add it to the container
		attributes.put(attribute.getName(),attribute);
	}

	/**
	 * Serialize this Schema in key-values pairs.
	 * @return serialized Schema
	 */
	@Override
	public String serialize() {
		StringBuilder builder = new StringBuilder();
		for (String schemaAttribute : SCHEMA_ATTRIBUTES) {
			builder.append(getAttribute(schemaAttribute)).append("\r\n");
		}
		return builder.toString().trim();
	}

	/**
	 * Provide the Schema filename.  Ex:  Acceleration.Gauge
	 * @return filename
	 */
	public final String getFilename() {
		String name = getName();
		int index = name.lastIndexOf(".");
		if(index > -1){
			name = name.substring(index + 1);
		}
		return name + "." + stereotype.name();
	}

	/**
	 * Convenient utility to get the  String Value of an Attribute fetched by name
	 * @param key attribute name
	 * @return attributes's values as a String - null if attributes not found
	 */
	public String getAttributeStringValue (String key){
		String result = null;
		IAttribute attribute =  getAttribute(key);
		if(attribute != null){
			result = attribute.getValueAsString();
		}
		return result;
	}

	/**
	 * Convenient utility to get the  String Value of an Attribute fetched by name
	 * @param key attributes name
	 * @param defaults fallback values if not found
	 * @return attributes's values as a String - null if attributes not found
	 */
	public String getAttributeStringValue (String key, String defaults){
		String result;
		IAttribute attribute =  getAttribute(key);
		if(attribute == null) {
			result = defaults;
			Logger.warning(String.format("Schema.getAttributeStringValue() No Match for Attribute '%s' using defaults.", key) );
		}else{
			result = attribute.getValueAsString();
		}
		return result;
	}

	/**
	 * Convenient utility to get the Integer Value of an Attribute fetched by name
	 * @param key attributes name
	 * @param defaults fallback values if not found
	 * @return attributes's values as a Integer - null if attributes not found
	 */
	public Integer getAttributeIntegerValue (String key, Integer defaults){
		Integer result;
		IAttribute attribute =  getAttribute(key);
		if(attribute == null) {
			result = defaults;
			Logger.warning(String.format("Schema.getAttributeIntegerValue() No Match for Attribute '%s' using defaults.", key) );
		}else {
			result = attribute.getValueAsInteger();
		}
		return result;
	}

	/**
	 * Convenient utility to get the Float Value of an Attribute fetched by name
	 * @param key attributes name
	 * @param defaults fallback values if not found
	 * @return attributes's values as a Float - null if attributes not found
	 */
	public Float getAttributeFloatValue (String key, Float defaults){
		Float result;
		IAttribute attribute =  getAttribute(key);
		if(attribute == null) {
			result = defaults;
		}else {
			result = attribute.getValueAsFloat();
		}
		return result;
	}

	/**
	 * Export in a comma separated list of key=value pairs of attribute
	 * @param attributes map of attributes to export
	 * @return exported attributes
	 */
	public static String exportAttributes(Map<String,IAttribute> attributes){
		StringBuilder builder = new StringBuilder(1024);
		Iterator<IAttribute> iterator = attributes.values().iterator();
		while(iterator.hasNext()) {
			IAttribute attr = iterator.next();
			builder.append(attr.toString());
			if(iterator.hasNext()){
				builder.append(",");
			}
		}
		return builder.toString();
	}
}