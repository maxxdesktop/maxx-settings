package com.maxxinteractive.msettings.schema;

import com.maxxinteractive.msettings.attributes.IAttribute;
import com.maxxinteractive.msettings.stereotype.Stereotypes;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

public class SchemaBuilder {

	private static String version;
	private static UUID uuid;
	private static Stereotypes stereotype;
	private static String name;

	private static IAttribute defaultValue;

	public SchemaBuilder() {
		reset();
	}

	public static void reset(){
		uuid=null;
		name="";
		version="1.0";
		defaultValue=null;
	}

	public SchemaBuilder generateUUID(){
		uuid = UUID.randomUUID();
		return this;
	}

	public SchemaBuilder setUUID(@NotNull String id){
		uuid = UUID.fromString(id);
		return this;
	}

	public SchemaBuilder setUUID(@NotNull UUID id){
		uuid = id;
		return this;
	}

	public SchemaBuilder setName(@NotNull String n){
		name = n;
		return this;
	}

	public SchemaBuilder setVersion(@NotNull String v){
		version = v;
		return this;
	}

	public SchemaBuilder setVersion(int v){
		version = Float.toString(v);
		return this;
	}
	public SchemaBuilder setVersion(float v){
		version = Float.toString(v);
		return this;
	}

	public SchemaBuilder setStereotype(@NotNull Stereotypes s){
		stereotype = s;
		return this;
	}

	public ISchema build(){
		ISchema schema = new Schema(uuid, name, stereotype, version);
		return schema;
	}
}