package com.maxxinteractive.msettings.stereotype;

/**
 * MaXX Settings Stereotype types
 */
public enum Stereotypes {
	Chars,
	Number,
	Decimal,
	Logical,

	Choice,
	Dimension,
	Location,
	Geometry,
	Gauge,
	Color,
	Image,
	Typeface,
	Command,
	Application,
	Catalog,
	Undefined;

	/**
	 * Safer ofValue() method from the Enum class
	 * @param name of the enum
	 * @return mapped Stereotype or Unknown.
	 */
	public static Stereotypes getStereotypeFromString(String name){
		switch (name.trim()) {
			case "Chars":
				return Stereotypes.Chars;
			case "Number":
				return Stereotypes.Number;
			case "Decimal":
				return Stereotypes.Decimal;
			case "Logical":
				return Stereotypes.Logical;
			case "Choice":
				return Stereotypes.Choice;
			case "Dimension":
				return Stereotypes.Dimension;
			case "Location":
				return Stereotypes.Location;
			case "Geometry":
				return Stereotypes.Geometry;
			case "Gauge":
				return Stereotypes.Gauge;
			case "Color":
				return Stereotypes.Color;
			case "Image":
				return Stereotypes.Image;
			case "Typeface":
				return Stereotypes.Typeface;
			case "Command":
				return Stereotypes.Command;
			case "Application":
				return Stereotypes.Application;
			case "Catalog":
				return Stereotypes.Catalog;
			default:
		}
		return  Stereotypes.Undefined;
	}

	/**
	 * Detect the Stereotype of the file loaded as a List of String
	 * @param line from schema file of input parameter
	 * @return matching Stereotype or Undefined is unsuccessful.
	 */
	public static Stereotypes extractStereotypeFromLine(String line){
		if (!line.startsWith("stereotype=")) {
			return Stereotypes.Undefined;
		}
		int index = line.indexOf("=");
		return Stereotypes.getStereotypeFromString(line.substring(index + 1));
	}
}