package com.maxxinteractive.msettings.tests;

import com.maxxinteractive.msettings.commons.Logger;
import com.maxxinteractive.msettings.instruments.InstrumentBuilder;
import com.maxxinteractive.msettings.instruments.InstrumentParser;
import com.maxxinteractive.msettings.instruments.choices.Catalog;
import com.maxxinteractive.msettings.instruments.choices.Choice;

public class CatalogTest {
    public static void main(String[] args) {

        Logger.setDebugMode(false);
        Logger.setSilentMode(false);

        CatalogTest test = new CatalogTest();

        // Load SGIScheme.Catalog
        test.test1();

        // Load Desktop.DtUtilities.WinEditor.Choice w/ Catalog type=Chars
        test.test2();


    }

    public void test1() {
        InstrumentBuilder builder;
        InstrumentParser parser;
        System.out.println("Testing ./Catalogs/SGIScheme.Catalog");
        parser = new InstrumentParser("./Catalogs/SGIScheme.Catalog");
        boolean result = parser.parse();
        System.out.println("Parsing was " + (result ? "a success" : "a failure"));
        System.out.println("attributes: " + parser.exportAttributes());
        Catalog catalog;

        if (result) {
            System.out.println("Building Catalog...");
            catalog = new Catalog(parser.getAttributes());
            System.out.println(catalog.serialize());

        }
        System.out.println("-----------------------------");
    }

    public void test2() {
        InstrumentBuilder builder;
        InstrumentParser parser;
        System.out.println("Testing ./tests/Desktop.DtUtilities.WinEditor.Choice");
        parser = new InstrumentParser("./tests/Desktop.DtUtilities.WinEditor.Choice");
        boolean result = parser.parse();
        System.out.println("Parsing was "+ (result?"a success":"a failure"));
        System.out.println ("attributes: "+parser.exportAttributes());

        if(result) {
            System.out.println("Building Choice (with Catalog) Instrument...");
            builder = new InstrumentBuilder(parser.getAttributes());
            Choice test = (Choice) builder.build().get();
            System.out.println(test.serialize());
            System.out.println(" - default="+test.getDefault().getValueAsString());
            System.out.println(" - value="+test.getValue().getValueAsString());
            System.out.println(" - type="+test.getType());
            System.out.println(" - is using Catalog="+test.isUsingCatalog());
            System.out.println(" - Catalog="+test.getCatalog().getOptionAttributes());
            System.out.println(" - Options="+test.getOptions());

        }else {
            System.exit(-1);
        }

        System.out.println("-----------------------------");
    }

}
