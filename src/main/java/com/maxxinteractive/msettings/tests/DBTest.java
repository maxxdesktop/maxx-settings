package com.maxxinteractive.msettings.tests;

import com.maxxinteractive.msettings.database.Database;
import com.maxxinteractive.msettings.database.IDatabase;
import com.maxxinteractive.msettings.utils.FileSystemHelper;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Optional;


public class DBTest {

    public static void main(String[] args) throws IOException {

        IDatabase database = Database.getInstance();
        database.createDB(false);

        database.open();

//        for(int i=0; i < 200; i++) {
//            IndexEntry entry = new IndexEntry(UUID.randomUUID(), "/aa/bb/cc/dd/", "KeepLayoutOpenInPlace.Logical");
//            database.put(entry);
//        }

        long start = System.nanoTime();
        Optional<String> content = FileSystemHelper.fastRead(Paths.get("/opt/MaXX/share/msettings/database/msettings.db") );
        long stop = System.nanoTime();
        System.out.println("Time to load everything "+ (stop-start)/1000 + " µs");

        start = System.nanoTime();
        Optional<String> record= database.findByUUID("d4375ee2-6eb2-4171-bd6f-ec9f41fa163d");
        stop = System.nanoTime();

        database.closeDB();
        System.out.format("Time to fetch %d µs [%s]\n",  (stop-start)/1000, record.get());
    }
}
