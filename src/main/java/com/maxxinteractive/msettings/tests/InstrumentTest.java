package com.maxxinteractive.msettings.tests;

import com.maxxinteractive.msettings.ColorSpace;
import com.maxxinteractive.msettings.Constants;
import com.maxxinteractive.msettings.commons.Logger;
import com.maxxinteractive.msettings.instruments.Chars;
import com.maxxinteractive.msettings.instruments.Decimal;
import com.maxxinteractive.msettings.instruments.Logical;
import com.maxxinteractive.msettings.instruments.Number;
import com.maxxinteractive.msettings.instruments.choices.Choice;
import com.maxxinteractive.msettings.instruments.complex.*;

import java.util.UUID;

public class InstrumentTest {

    public static void main(String[] args) {

        Logger.setDebugMode(false);
        Logger.setSilentMode(false);

        System.out.println("--------- InstrumentTest   Start  -------------");

        InstrumentTest test = new InstrumentTest();

        test.test10();

        System.out.println("--------- InstrumentTest   Done   -------------");
    }

    public void test(){


        Number instrument = new Number(UUID.fromString("a2bb1bc6-422f-4d43-9e19-d57e20ea4de8"),"Desktop.Mouse.Threshold");
        instrument.setDefault(2000);

        System.out.println(instrument.serialize());
        System.out.println("Fullpath="+instrument.getRelativePath());
        System.out.println("FQ Name="+instrument.getFQName());
        System.out.println("----------------------");

        Chars instrument2 = new Chars(null,"Desktop.Login.Name");
        instrument2.setDefault("guest")
                .setEncoding(Constants.ENCODING_UTF16)
                .setMaxLength(3).setValue("123456");
        System.out.println(instrument2.serialize());
        System.out.println("Fullpath="+instrument2.getRelativePath());
        System.out.println("FQ Name="+instrument2.getFQName());

        System.out.println("----------------------");
        Decimal instrument3 = new Decimal(null,"Desktop.Mouse.AccelerationFactor", 1.0f);
//        instrument3.setValue(0.75f);
        System.out.println(instrument3.serialize());
        System.out.println("Fullpath="+instrument3.getRelativePath());
        System.out.println("FQ Name="+instrument3.getFQName());

        System.out.println("----------------------");
        Logical instrument4 = new Logical(null,"Desktop.Mouse.Gesture", false, true);
//        instrument4.setValue(false);
        System.out.println(instrument4.serialize());
        System.out.println("Fullpath="+instrument4.getRelativePath());
        System.out.println("FQ Name="+instrument4.getFQName());

        System.out.println("----------------------");
        Gauge instrument5 = new Gauge(null,"Desktop.Mouse.Acceleration");
        instrument5.setMinimum(10.0f).setMaximum(100.0f).setScale(1.0f);
        instrument5.setDefault(1.0f); // will generate a warning (out of range);
        instrument5.setValue(11.0f);
        System.out.println(instrument5.serialize());
        System.out.println("Fullpath="+instrument5.getRelativePath());
        System.out.println("FQ Name="+instrument5.getFQName());
        System.out.println("----------------------");

        Choice instrument6 = new Choice(UUID.randomUUID(),"Desktop.Colors.SGIScheme" );
        instrument6.addOption("0","Patates");
        instrument6.addOption("1","Frites");
        instrument6.setDefault("0");  // will fail since lower than minimum, but code will recover.

        System.out.println(instrument6.serialize());
        System.out.println("Fullpath="+instrument6.getRelativePath());
        System.out.println("FQ Name="+instrument6.getFQName());

        System.exit(1);
        System.out.println("----------------------");
        Dimension instrument7 = new Dimension(null,"Desktop.Window.DefaultSize", 1000,400 );
        instrument7.setDimension(50,-50); // should reset to positive 50
        System.out.println(instrument7.serialize());
        System.out.println("Fullpath="+instrument7.getRelativePath());
        System.out.println("FQ Name="+instrument7.getFQName());

        System.out.println("----------------------");
        Location instrument8= new Location(null,"Desktop.Window.Origin",10,10 );
        instrument8.setLocation(20,1000);
        System.out.println(instrument8.serialize());
        System.out.println("Fullpath="+instrument8.getRelativePath());

        System.out.println("----------------------");
        Geometry instrument9 = new Geometry(null,"Desktop.Window.Workspace");
        instrument9.setGeometry(200,200,-1000,-600);
        System.out.println(instrument9.serialize());
        System.out.println("Fullpath="+instrument9.getRelativePath());

        System.out.println("----------------------");
        Command instrument10 = new Command(null,"Default.Application.Nedit", "xnedit");
        instrument10.setExecPath("/usr/bin").setExecParams("-server");
        System.out.println(instrument10.serialize());
        System.out.println("Fullpath="+instrument10.getRelativePath());

        System.out.println("----------------------");
        Typeface instrument11 = new Typeface(null,"Default.Application.Font", "Mono",12f);
        System.out.println(instrument11.serialize());
        System.out.println("Fullpath="+instrument11.getRelativePath());

        System.out.println("----------------------");
        int[] white = {255,255,255};
        float[] red = {1.0f,0.0f, 0.0f};
        Color instrument12 = new Color(null,"Default.Workspace.Accent", ColorSpace.RGB255, red, null);
        instrument12.setValue(white).setAlpha(0.5f);
        System.out.println(instrument12.serialize());
        System.out.println("Fullpath="+instrument12.getRelativePath());


        System.out.println("--------- Completed -------------");
    }

    public void test10(){
        System.out.println("----------------------");
        Command instrument10 = new Command(null,"Default.Application.Nedit", "nedit");
//        instrument10.setExecPath("/usr/bin").setExecParams("-server");
        System.out.println(instrument10.serialize());
        System.out.println("Fullpath="+instrument10.getRelativePath());
        System.out.println("default="+instrument10.getDefault().getValueAsString());
        System.out.println("value="+instrument10.getValue().getValueAsString());
        instrument10.setExecName("xnedit");
        instrument10.setExecPath("/opt/bin");
        System.out.println("value="+instrument10.getValue().getValueAsString());
    }
}
