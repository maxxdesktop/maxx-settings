package com.maxxinteractive.msettings.tests;

import com.maxxinteractive.msettings.commons.Logger;
import com.maxxinteractive.msettings.instruments.Number;
import com.maxxinteractive.msettings.instruments.*;
import com.maxxinteractive.msettings.instruments.choices.Catalog;
import com.maxxinteractive.msettings.instruments.choices.Choice;
import com.maxxinteractive.msettings.instruments.complex.*;

public class ParserTest {

    public static void main(String[] args){

        Logger.setDebugMode(true);
        Logger.setSilentMode(false);
        InstrumentBuilder builder;
        InstrumentParser parser = new InstrumentParser("./tests/Desktop.DtUtilities.WinEditor.Chars");
        boolean result = parser.parse();
        System.out.println("Parsing was "+ (result?"a success":"a failure"));
        System.out.println ("attributes: "+parser.exportAttributes());

        if(result) {
            System.out.println("Building Chars Instrument...");
            builder = new InstrumentBuilder(parser.getAttributes());
            Chars test = (Chars) builder.build().get();
            System.out.println(test.serialize());
            System.out.println(" * default="+test.getDefault().getValueAsString());
            System.out.println(" * value="+test.getValue().getValueAsString());
            System.out.println(" * encoding="+test.getEncoding());
            System.out.println(" * maxLength="+test.getMaxLength());
        }

        System.out.println("-----------------------------");

        parser.resetFile("./tests/Integer.Number");
        result = parser.parse();
        System.out.println("Parsing was "+ (result?"a success":"a failure"));
        System.out.println ("attributes: "+parser.exportAttributes());

        if(result) {
            System.out.println("Building Number Instrument...");
            builder = new InstrumentBuilder(parser.getAttributes());
            Number test = (Number) builder.build().get();
            System.out.println(test.serialize());
            test.setDefault(11);
            System.out.println(" * default="+test.getDefault().getValueAsInteger());
            test.setDefault(null);
            System.out.println(" * default="+test.getDefault().getValueAsInteger());
            System.out.println(" * value is not present="+test.getValue().isNotPresent());
            if(! test.getValue().isNotPresent()) {
                System.out.println(" * value=" + test.getValue());
                System.out.println(" * value=" + test.getValue().getValueAsInteger());
            }
        }

        System.out.println("-----------------------------");

        parser.resetFile("./tests/Floats.Decimal");
        result = parser.parse();
        System.out.println("Parsing was "+ (result?"a success":"a failure"));
        System.out.println ("attributes: "+parser.exportAttributes());

        if(result) {
            System.out.println("Building Decimal Instrument...");
            builder = new InstrumentBuilder(parser.getAttributes());
            Decimal test = (Decimal) builder.build().get();
            test.setDefault(null).setDefault(1f);
            System.out.println(test.serialize());
            System.out.println(" * default="+test.getDefault().getValueAsFloat());

            System.out.println(" * value is not present="+test.getValue().isNotPresent());
            if(! test.getValue().isNotPresent()) {
                System.out.println(" * value=" + test.getValue());
                System.out.println(" * value=" + test.getValue().getValueAsFloat());
            }
        }

        System.out.println("-----------------------------");

        parser.resetFile("./tests/Question.Logical");
        result = parser.parse();
        System.out.println("Parsing was "+ (result?"a success":"a failure"));
        System.out.println ("attributes: "+parser.exportAttributes());

        if(result) {
            System.out.println("Building Logical Instrument...");
            builder = new InstrumentBuilder(parser.getAttributes());
            Logical test = (Logical) builder.build().get();
            System.out.println(test.serialize());
            System.out.println(" * default="+test.getDefault().getValueAsBoolean());
            System.out.println(" * value="+test.getValue().getValueAsBoolean());
        }

        System.out.println("-----------------------------");

        parser.resetFile("./tests/Desktop.Mouse.Acceleration.Gauge");
        result = parser.parse();
        System.out.println("Parsing was "+ (result?"a success":"a failure"));
        System.out.println ("attributes: "+parser.exportAttributes());

        if(result) {
            System.out.println("Building Gauge Instrument...");
            builder = new InstrumentBuilder(parser.getAttributes());
            Gauge test = (Gauge) builder.build().get();
            System.out.println(test.serialize());
            System.out.println(" * default="+test.getDefault().getValueAsFloat());
            System.out.println(" * min="+test.getMinimum());
            System.out.println(" * max="+test.getMaximum());
            System.out.println(" * scale="+test.getScale());

            System.out.println(" * value is not present="+test.getValue().isNotPresent());
            if(! test.getValue().isNotPresent()) {
                System.out.println(" * value=" + test.getValue());
                System.out.println(" * value=" + test.getValue().getValueAsFloat());
            }
            System.out.println("reset...");
            test.resetValue();
            System.out.println(" * default="+test.getDefault().getValueAsFloat());
            System.out.println(" * value="+test.getValue().getValueAsFloat());
        }

        System.out.println("-----------------------------");

        parser.resetFile("./tests/Desktop.FileManager.IconSortBy.Choice");
        result = parser.parse();
        System.out.println("Parsing was "+ (result?"a success":"a failure"));
        System.out.println ("attributes: "+parser.exportAttributes());

        if(result) {
            System.out.println("Building Choice Instrument...");
            builder = new InstrumentBuilder(parser.getAttributes());
            Choice test = (Choice) builder.build().get();
            System.out.println("count="+test.getCount());
            System.out.println("catalog ?="+test.isUsingCatalog());
            System.out.println(test.serialize());
            System.out.println(" * resolve="+test.resolve());
            System.out.println(" * options="+test.getOptions());
            test.setValue("4");
            System.out.println(" * resolve="+test.resolve());

            System.out.println(" * value is not present="+test.getValue().isNotPresent());
            if(! test.getValue().isNotPresent()) {
                System.out.println(" * value=" + test.getValue());
            }

            System.out.println("reset...");

            test.resetValue();
            System.out.println(" * default="+test.getDefault().getValueAsInteger());
            System.out.println(" * value="+test.getValue().getValueAsInteger());
        }

        System.out.println("-----------------------------");

        parser.resetFile("./tests/Nedit.Command");
        result = parser.parse();
        System.out.println("Parsing was "+ (result?"a success":"a failure"));
        System.out.println ("attributes: "+parser.exportAttributes());

        if(result) {
            System.out.println("Building Command Instrument...");
            builder = new InstrumentBuilder(parser.getAttributes());
            Command test = (Command) builder.build().get();
            System.out.println(test.serialize());

            System.out.println(" * value is not present="+test.getValue().isNotPresent());
            if(! test.getValue().isNotPresent()) {
                System.out.println(" * value=" + test.getValue().getValueAsString());
            }

            System.out.println("reset...");

            test.resetValue();
            System.out.println(" * default="+test.getDefault().getValueAsString());
            System.out.println(" * value="+test.getValue().getValueAsString());
            System.out.println("using setCommand( //usr/bin/vim -s eric.txt -s 565 )...");

            test.setCommand("//usr/bin/vim -s eric.txt -s 565 ");
            System.out.println(" * value="+test.getValue().getValueAsString());
            System.out.println(" * execPath="+test.getExecPath());
            System.out.println(" * execName="+test.getExecName());
            System.out.println(" * execParam="+test.getExecParams());
        }

        System.out.println("-----------------------------");
System.exit(1);
        parser.resetFile("./tests/DesktopTrashCan.Location");
        result = parser.parse();
        System.out.println("Parsing was "+ (result?"a success":"a failure"));
        System.out.println ("attributes: "+parser.exportAttributes());

        if(result) {
            System.out.println("Building Location Instrument...");
            builder = new InstrumentBuilder(parser.getAttributes());
            Location test = (Location) builder.build().get();
            System.out.println(test.serialize());

            System.out.println(" * value is not present=" + test.getValue().isNotPresent());
            if (!test.getValue().isNotPresent()) {
                System.out.println(" * value=" + test.getValue());
                System.out.println(" * value=" + test.getValue().getValueAsString());
                System.out.println(" * X=" + test.getX());
                System.out.println(" * Y=" + test.getY());
            }

            System.out.println("reset...");

            test.resetValue();
            if (!test.getValue().isNotPresent()) {
                System.out.println(" * value=" + test.getValue().getValueAsString());
                System.out.println(" * X=" + test.getX());
                System.out.println(" * Y=" + test.getY());
            }
            test.setLocation("+2312-1231");
            System.out.println(" * value=" + test.getValue().getValueAsString());
            System.out.println(" * X=" + test.getX());
            System.out.println(" * Y=" + test.getY());
        }

        System.out.println("-----------------------------");

        parser.resetFile("./tests/ActiveRectangle.Dimension");
        result = parser.parse();
        System.out.println("Parsing was "+ (result?"a success":"a failure"));
        System.out.println ("attributes: "+parser.exportAttributes());

        if(result) {
            System.out.println("Building Dimension Instrument...");
            builder = new InstrumentBuilder(parser.getAttributes());
            Dimension test = (Dimension) builder.build().get();
            System.out.println(test.serialize());

            System.out.println(" * value is not present="+test.getValue().isNotPresent());
            if(! test.getValue().isNotPresent()) {
                System.out.println(" * value=" + test.getValue());
                System.out.println(" * value=" + test.getValue().getValueAsString());
                System.out.println(" * width=" + test.getWidth());
                System.out.println(" * height=" + test.getHeight());
            }

            System.out.println("reset...");

            test.resetValue();
            if(! test.getValue().isNotPresent()) {
                System.out.println(" * value=" + test.getValue().getValueAsString());
                System.out.println(" * width=" + test.getWidth());
                System.out.println(" * height=" + test.getHeight());
            }
            System.out.println("set... 11.0x33.0");
            test.setDimension("11.0x33.0");
            System.out.println(" * value=" + test.getValue().getValueAsString());
            System.out.println(" * width=" + test.getWidth());
            System.out.println(" * height=" + test.getHeight());
        }

        System.out.println("-----------------------------");

        parser.resetFile("./tests/Desktop.Overview.Geometry");
        result = parser.parse();
        System.out.println("Parsing was "+ (result?"a success":"a failure"));
        System.out.println ("attributes: "+parser.exportAttributes());

        if(result) {
            System.out.println("Building Geometry Instrument...");
            builder = new InstrumentBuilder(parser.getAttributes());
            Geometry test = (Geometry) builder.build().get();
            System.out.println(test.serialize());

            System.out.println(" * value is not present="+test.getValue().isNotPresent());
            if(! test.getValue().isNotPresent()) {
                System.out.println(" * value=" + test.getValue().getValueAsString());
                System.out.println(" * width=" + test.getWidth());
                System.out.println(" * height=" + test.getHeight());
                System.out.println(" * x=" + test.getX());
                System.out.println(" * y=" + test.getY());
            }

            System.out.println("reset...");

            test.resetValue();
            if(! test.getValue().isNotPresent()) {
                System.out.println(" * value=" + test.getValue().getValueAsString());
                System.out.println(" * width=" + test.getWidth());
                System.out.println(" * height=" + test.getHeight());
                System.out.println(" * x=" + test.getX());
                System.out.println(" * y=" + test.getY());
            }
            System.out.println("set... 11.0x33.0+12-12");
            test.setGeometry("11.0x33.0+12-12");
            System.out.println(" * value=" + test.getValue().getValueAsString());
            System.out.println(" * width=" + test.getWidth());
            System.out.println(" * height=" + test.getHeight());
            System.out.println(" * x=" + test.getX());
            System.out.println(" * y=" + test.getY());
        }

        System.out.println("-----------------------------");

        parser.resetFile("./tests/Terminal.Typeface");
        result = parser.parse();
        System.out.println("Parsing was "+ (result?"a success":"a failure"));
        System.out.println ("attributes: "+parser.exportAttributes());

        if(result) {
            System.out.println("Building Typeface Instrument...");
            builder = new InstrumentBuilder(parser.getAttributes());
            Typeface test = (Typeface) builder.build().get();
            System.out.println(test.serialize());

            System.out.println(" * value is not present="+test.getValue().isNotPresent());
            if(! test.getValue().isNotPresent()) {
                System.out.println(" * value=" + test.getValue().getValueAsString());

            }

            System.out.println("reset...");

            test.resetValue();
            test.setStyle("normal").setWeight("bold").setFont("Mono").setSize(11);
            if(! test.getValue().isNotPresent()) {
                System.out.println(" * value=" + test.getValue().getValueAsString());
                System.out.println(" * size=" + test.getSize());
                System.out.println(" * font=" + test.getFont());
                System.out.println(" * slant=" + test.getSlant()); // should be null
                System.out.println(" * weight=" + test.getWeight());
            }
            test.setTypeface("Mono:size=10.5:weight=bold:style=normal:slant=oblique");
            System.out.println(" * value=" + test.getValue().getValueAsString());
        }

        System.out.println("-----------------------------");

        parser.resetFile("./tests/Accent.Color");
        result = parser.parse();
        System.out.println("Parsing was "+ (result?"a success":"a failure"));
        System.out.println ("attributes: "+parser.exportAttributes());

        if(result) {
            System.out.println("Building Color Instrument...");
            builder = new InstrumentBuilder(parser.getAttributes());
            Color test = (Color) builder.build().get();
            System.out.println(test.serialize());

            System.out.println(" * value is not present="+test.getValue().isNotPresent());
            if(! test.getValue().isNotPresent()) {
                System.out.println(" * value=" + test.getValue().getValueAsString());
            }

            System.out.println("reset...");
            int[] white = {255,255,255};
            float[] red = {1.0f,0.0f, 0.0f};

            test.resetValue();
            test.setAlpha(1.0f).setValue(red);
            if(! test.getValue().isNotPresent()) {
                System.out.println(" * value=" + test.getValue().getValueAsString());
                System.out.println(" * alpha=" + test.getAlpha());
                System.out.println(" * colorSpace=" + test.getColorSpace());
            }
        }
    }
}
