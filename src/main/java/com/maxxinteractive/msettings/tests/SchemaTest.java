package com.maxxinteractive.msettings.tests;

import com.maxxinteractive.msettings.schema.ISchema;
import com.maxxinteractive.msettings.schema.SchemaBuilder;
import com.maxxinteractive.msettings.stereotype.Stereotypes;

public class SchemaTest {

	public static void main(String[] args){

		System.out.println("--------- SchemaTest-------------");
		SchemaBuilder builder = new SchemaBuilder();

		builder.setName("Threshold").generateUUID()
				.setStereotype(Stereotypes.Number);

		ISchema schema2 = builder.build();

		System.out.println("--------- serialize -------------");
		System.out.println(schema2.serialize());

		System.out.println("--------- props -------------");
		System.out.println("Filename="+schema2.getFilename());
		System.out.println("UUID="+schema2.getUUID());
		System.out.println("Name="+schema2.getName());

		System.out.println("--------- Completed -------------");
	}
}
