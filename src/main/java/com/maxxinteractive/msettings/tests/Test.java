package com.maxxinteractive.msettings.tests;

import java.util.HashMap;
import java.util.Map;

import com.maxxinteractive.msettings.instruments.Instrument;
import com.maxxinteractive.msettings.instruments.Chars;

public class Test {

	public static void main(String[] args) {

//		for(int t=0; t<args.length; t++) {
//			System.out.println("t="+t +"="+ args[t]);
//		}
		int TOTAL = 1000;
		if (args.length > 0) {
			TOTAL = Integer.parseInt(args[0]);
		}
		
		int slowest = 0;
		int fastest = 0;
		boolean first = true;
		String[] names = { "Desktop.Mouse.Acceleration",
						"Desktop.Mouse.Threshold",
						"Desktop.Settings.DisplayApplicationWarnings",
						"Desktop.DtSounds.StartupShutdownTunes",
						"Desktop.Mouse.WheelMouseScroll",
						"Desktop.Window.AutoWindowPlacement",
						"Desktop.Settings.DisplayApplicationErrors"};

		Map<String, Integer> counter = new HashMap<>(TOTAL);
		for (String name :names) {
			
			Instrument instrument = new Chars(null,name);
			
			long start = System.nanoTime();
			String path = instrument.getRelativePath();
			long stop = System.nanoTime();



			int val = (int) (stop - start);
			if (first) {
				fastest = val;
				slowest = val;
				first = false;
			} else {
				if (val < fastest) {
					fastest = val;
				} else if (val > slowest) {
					val = slowest;
				} else {
					val = 1;
				}
			}
			System.out.format("Full Path = %s time µs=%d\n", path,  (stop - start));
			if (counter.containsKey(path)) {
				counter.put(path, counter.get(path) + 1);
			} else {
				counter.put(path, 1);
			}
		}
		int count = 0;
		int col = 0;
		for (String key : counter.keySet()) {
			if (counter.get(key) > 1) {
				System.out.println(++count + ". " + key + ": " + counter.get(key));
				col++;
			}
		}
		System.out.format("Slowest=%d µs. and fastest=%d µs\n.",slowest,fastest);
		System.out.format("Total Collision %d out of %d\n",col, TOTAL);
	}
	
	public static int hashCode(char[] chars) {
		int h = 0;
        for (char c : chars)
			h = 31 * h +c;

        return h;
    }
}