package com.maxxinteractive.msettings.utils;

import com.maxxinteractive.msettings.Constants;
import com.maxxinteractive.msettings.attributes.AttributeBuilder;
import com.maxxinteractive.msettings.attributes.IAttribute;
import com.maxxinteractive.msettings.commons.Logger;
import com.maxxinteractive.msettings.instruments.Chars;
import com.maxxinteractive.msettings.instruments.Decimal;
import com.maxxinteractive.msettings.instruments.Logical;
import com.maxxinteractive.msettings.instruments.Number;
import com.maxxinteractive.msettings.instruments.choices.Choice;
import com.maxxinteractive.msettings.instruments.complex.Gauge;
import com.maxxinteractive.msettings.stereotype.Stereotypes;
import org.jetbrains.annotations.NotNull;

import java.util.*;

import static com.maxxinteractive.msettings.Constants.*;

public class AttributeMapUtils {

    private static final String VALIDATION_RULE_MISSING_ATTRIBUTE = "Validation Rule: Missing '%s' attribute.";

    /**
     * Convert a {@code List<KeyValuePair> into a Map<String,IAttribute>}<br>
     * The method will first introspect the List and detect the Stereotype.<br>
     * With the Stereotype detected, now the conversion can begging.
     * @param keyValues input list of VKP
     * @return attributes
     */
    public static Optional<Map<String, IAttribute>> convert(@NotNull List<KeyValuePair> keyValues){
        Map<String,IAttribute> attributes = new HashMap<>(16);

        Stereotypes stereotype = Stereotypes.Undefined;
        String instrumentName;

        // 1.  Detect stereotype and Instrument's name
        //extract stereotype and instrument's name
        int total =0;
        for(KeyValuePair kvp : keyValues){
            String key = kvp.getKey();
            if(key.equals(ATTRIBUTE_STEREOTYPE)){
                stereotype = Stereotypes.getStereotypeFromString(kvp.getValue());
                total++;
            }
            if(key.equals(ATTRIBUTE_NAME)){
                instrumentName = key;
                total++;
            }
            if(total == 2){
                break;
            }
        }

        if(total != 2){
            Logger.error(new Exception("Invalid List<KeyValuePair>, missing stereotype and/or name attributes."));
            return Optional.empty();
        }

        // iterate over List of Pair and generate Map<String, IAttribute>
        for (KeyValuePair keyValuePair : keyValues) {

            String name = keyValuePair.getKey();
            String value = keyValuePair.getValue();
            IAttribute attribute;

            switch (name) {
                case ATTRIBUTE_NAME:
                case ATTRIBUTE_VERSION:
                case ATTRIBUTE_STEREOTYPE:
                case ATTRIBUTE_TYPE:
                case ATTRIBUTE_ENCODING:{
                    attribute = AttributeBuilder.buildString(name, value);
                    break;
                }
                case Constants.ATTRIBUTE_UUID: {
                    attribute = AttributeBuilder.buildUUID(name, value);
                    break;
                }

                case ATTRIBUTE_MAX_LENGTH:
                {
                    attribute = AttributeBuilder.buildInteger(name, value);
                    break;
                }
                case ATTRIBUTE_SIZE:
                case ATTRIBUTE_ALPHA:
                case ATTRIBUTE_SCALE:
                case ATTRIBUTE_MINIMUM:
                case ATTRIBUTE_MAXIMUM:{
                    attribute = AttributeBuilder.buildFloat(name, value);
                    break;
                }
                case ATTRIBUTE_CATALOG: {
                    attribute = AttributeBuilder.buildString(name, value);
                    break;
                }
                case ATTRIBUTE_EXEC_NAME:
                case ATTRIBUTE_EXEC_PATH:
                case ATTRIBUTE_EXEC_PARAMS:
                case ATTRIBUTE_ENV_BINARY_PATH:
                case ATTRIBUTE_ENV_LIBRARY_PATH:
                case ATTRIBUTE_GEOMETRY:
                case ATTRIBUTE_FONT:
                case ATTRIBUTE_STYLE:
                case ATTRIBUTE_WEIGHT:
                case ATTRIBUTE_SLANT:
                case ATTRIBUTE_COLOR_SPACE:
                case ATTRIBUTE_DEFAULT:
                case ATTRIBUTE_VALUE:{
                    if(stereotype == Stereotypes.Chars || stereotype == Stereotypes.Command || stereotype == Stereotypes.Application
                            || stereotype == Stereotypes.Location || stereotype == Stereotypes.Dimension || stereotype == Stereotypes.Geometry
                            || stereotype == Stereotypes.Typeface || stereotype == Stereotypes.Color){
                        attribute = AttributeBuilder.buildString(name, value);
                    } else if(stereotype == Stereotypes.Number || stereotype == Stereotypes.Choice) {
                        attribute = AttributeBuilder.buildInteger(name, value);
                    }
                    else if(stereotype == Stereotypes.Decimal || stereotype == Stereotypes.Gauge){
                        attribute = AttributeBuilder.buildFloat(name, value);
                    }
                    else if(stereotype == Stereotypes.Logical){
                        attribute = AttributeBuilder.buildBoolean(name, value);
                    }else {
                        Logger.error(new Exception("Unsupported Stereotype '"+ stereotype  + "' with attribute ["+name+"] " ));
                        return Optional.empty();
                    }
                    break;
                }
                default: {
                    if (name.startsWith("option[")) {
                        attribute = AttributeBuilder.buildString(name, value);
                        break;
                    }
                    Logger.error(new Exception("Failed to recognize the attribute '" + name));
                    return Optional.empty();
                }
            }
            attributes.put(name, attribute);
        }
        return Optional.of(attributes);
    }

    /**
     * Apply Schema rules for each type of Stereotype
     * @param attributes to validate
     * @return status of the validation
     */
    public static boolean normalizeAttributes(@NotNull Map<String, IAttribute> attributes){

        // 1. Check for Stereotype as it is a mandatory attribute
        Stereotypes stereotype = Stereotypes.Undefined;
        IAttribute attribute = attributes.get(ATTRIBUTE_STEREOTYPE);
        if(attribute != null){
            stereotype = Stereotypes.getStereotypeFromString(attribute.getValueAsString());
        }else {
            Logger.error(new Exception("Missing stereotype, operation failed."));
            return false;
        }

        if(stereotype == Stereotypes.Undefined){
            Logger.error(new Exception("Undefined stereotype ("+ attribute.getValueAsString()+"), operation failed."));
            return false;
        }

        // 2. Check for uuid as it is a mandatory attribute
        // uuid is not required when parsing a new Instrument for creation.
        if(isMissingAttribute(attributes, ATTRIBUTE_UUID,false)){
            attributes.put(ATTRIBUTE_UUID, AttributeBuilder.buildUUID(ATTRIBUTE_UUID, UUID.randomUUID()));
        }

        // 3. Check for name as it is a mandatory attribute
        if(isMissingAttribute(attributes, ATTRIBUTE_NAME,true)){
            return false;
        }

        // 4. Extra validation based on the stereotype
        switch(stereotype) {
            case Chars: {
                // add default if missing with 'null' as value
                if(isMissingAttribute(attributes, ATTRIBUTE_DEFAULT,false)){
                    attributes.put(ATTRIBUTE_DEFAULT, AttributeBuilder.buildString(ATTRIBUTE_DEFAULT, Chars.DEFAULT));
                }
                // add encoding if missing with 'UTF-8' as value
                if(isMissingAttribute(attributes, ATTRIBUTE_ENCODING,false)){
                    attributes.put(ATTRIBUTE_ENCODING, AttributeBuilder.buildString(ATTRIBUTE_ENCODING, ENCODING_UTF8));
                }
                break;
            }
            case Number: {
                // add default if missing with '0' as value
                if(isMissingAttribute(attributes, ATTRIBUTE_DEFAULT,false)){
                    attributes.put(ATTRIBUTE_DEFAULT, AttributeBuilder.buildInteger(ATTRIBUTE_DEFAULT, Number.DEFAULT));
                }
                break;
            }
            case Decimal: {
                // add default if missing with '0.0' as value
                if(isMissingAttribute(attributes, ATTRIBUTE_DEFAULT,false)){
                    attributes.put(ATTRIBUTE_DEFAULT, AttributeBuilder.buildFloat(ATTRIBUTE_DEFAULT, Decimal.DEFAULT));
                }
                break;
            }
            case Logical: {
                // add default if missing with 'false' as value
                if(isMissingAttribute(attributes, ATTRIBUTE_DEFAULT,false)){
                    attributes.put(ATTRIBUTE_DEFAULT, AttributeBuilder.buildBoolean(ATTRIBUTE_DEFAULT, Logical.DEFAULT));
                }
                break;
            }
            case Catalog: {
                if(isMissingAttribute(attributes, ATTRIBUTE_TYPE,true)){
                    return false;
                }
                break;
            }
            case Choice: {
                // add default if missing with 'false' as value
                if(isMissingAttribute(attributes, ATTRIBUTE_DEFAULT,false)){
                    attributes.put(ATTRIBUTE_DEFAULT, AttributeBuilder.buildInteger(ATTRIBUTE_DEFAULT, Choice.DEFAULT));
                }
                if(isMissingAttribute(attributes, ATTRIBUTE_TYPE,true)){
                    return false;
                }
                break;
            }
            case Gauge: {
                IAttribute minimum = attributes.get(ATTRIBUTE_MINIMUM);
                IAttribute maximum = attributes.get(ATTRIBUTE_MAXIMUM);
                IAttribute scale = attributes.get(ATTRIBUTE_SCALE);

                boolean failed = false;
                if(minimum == null){
                    Logger.error(new Exception(String.format(VALIDATION_RULE_MISSING_ATTRIBUTE, ATTRIBUTE_MINIMUM)));
                    failed = true;
                }
                if(maximum == null){
                    Logger.error(new Exception(String.format(VALIDATION_RULE_MISSING_ATTRIBUTE, ATTRIBUTE_MAXIMUM)));
                    failed = true;
                }
                if(scale == null){
                    Logger.error(new Exception(String.format(VALIDATION_RULE_MISSING_ATTRIBUTE, ATTRIBUTE_SCALE)));
                    failed = true;
                }
                if (failed) {
                    return false;
                }

                IAttribute defaultAtt = attributes.get(ATTRIBUTE_DEFAULT);
                if(defaultAtt == null){
                    defaultAtt = AttributeBuilder.buildFloat(ATTRIBUTE_DEFAULT, Gauge.DEFAULT);
                    attributes.put(ATTRIBUTE_DEFAULT, defaultAtt);
                    Logger.warning(String.format(VALIDATION_RULE_MISSING_ATTRIBUTE, ATTRIBUTE_DEFAULT));
                }
                // Gauge validations
                float min = minimum.getValueAsFloat();
                float max = maximum.getValueAsFloat();
                float defInt = defaultAtt.getValueAsFloat();

                if(min >= max){
                    Logger.error(new Exception("Validation Rule Error: Invalid min/max range."));
                    return false;
                }
                if(defInt < min || defInt > max){
                    Logger.error(new Exception("Validation Rule Error: Default value outside min/max range."));
                    return false;
                }
                break;
            }
            case Command:{
                if(isMissingAttributes(attributes, new String[]{ATTRIBUTE_DEFAULT, ATTRIBUTE_EXEC_NAME, ATTRIBUTE_EXEC_PATH},
                        true,true)){
                    return false;
                }

                if(isMissingAttributes(attributes, new String[]{ATTRIBUTE_EXEC_NAME, ATTRIBUTE_EXEC_PATH},
                        true,true)){
                    return false;
                }
                break;
            }
            case Dimension:
            case Location:
            case Geometry:{
                if(isMissingAttribute(attributes, ATTRIBUTE_DEFAULT,true)){
                    return false;
                }
                break;
            }
            case Color:{
                if(isMissingAttributes(attributes, new String[]{ATTRIBUTE_DEFAULT, ATTRIBUTE_COLOR_SPACE},
                        true,true)){
                    return false;
                }
                break;
            }

            case Typeface: {
                if(isMissingAttributes(attributes, new String[]{ATTRIBUTE_DEFAULT/*, ATTRIBUTE_FONT, ATTRIBUTE_SIZE*/},
                        true,true)){
                    return false;
                }

                if(isMissingAttributes(attributes, new String[]{ATTRIBUTE_FONT, ATTRIBUTE_SIZE},
                        true,true)){
                    return false;
                }
                break;
            }

            case Image:
            case Application:

            default: {
            }
        }
        return true;
    }

    /**
     * Recursive validation for any missing attributes from the local {@code Map<String,IAttribute>}
     * @param attributes to validate
     * @param attributeNames list of attribute names to look for
     * @param mandatory flag that inform about mandatory attribute rule.
     * @param stopOnFirst flag that inform to either stop or continue on first occurrence of a missing attribute.
     * @return true on the first found missing attribute
     */
    public static boolean isMissingAttributes(@NotNull Map<String, IAttribute> attributes, @NotNull String[] attributeNames, boolean mandatory, boolean stopOnFirst){
        boolean status = false;
        for(String attributeName : attributeNames) {
            status = isMissingAttribute(attributes, attributeName, mandatory);
            if(stopOnFirst && status){
                break;
            }
        }
        return status;
    }

    /**
     * Validate if the attribute is missing from the local {@code Map<String,IAttribute>}
     * @param attributes to validate
     * @param attributeName attribute name to look for
     * @param mandatory flag that inform about mandatory attribute.
     * @return true if is missing
     */
    public static boolean isMissingAttribute(Map<String, IAttribute> attributes, @NotNull String attributeName, boolean mandatory){
        boolean status = false;
        if (attributes.get(attributeName) == null) {
            status = true;
            if (mandatory) {
                Logger.error("Missing mandatory attribute", new Exception(String.format(VALIDATION_RULE_MISSING_ATTRIBUTE, attributeName)));
            } else {
                Logger.debug(String.format(VALIDATION_RULE_MISSING_ATTRIBUTE, attributeName));
            }
        }
        return status;
    }
}
