package com.maxxinteractive.msettings.utils;

import com.maxxinteractive.msettings.commons.Logger;
import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.*;

/**
 * Various Filesystem level helpers using NIO2
 * @author Eric Masson
 * @version 1.0
 */
public class FileSystemHelper {



    /**
     * Load file content in the most efficient way.
     * @param path absolute file path
     * @return the content of the file or empty if it fails
     */
    public static Optional<String> fastRead(@NotNull Path path){
        List<String> list = new ArrayList<>(1);
        try {
            final BufferedReader in = new BufferedReader(
                    new InputStreamReader(new FileInputStream(path.toFile().getAbsolutePath()), StandardCharsets.UTF_8));
            String line;
            while ((line = in.readLine()) != null) {
                list.add(line);
            }
            in.close();
        } catch (final IOException e) {
            Logger.error(e);
            return Optional.empty();
        }
        StringBuilder builder = new StringBuilder(list.size());
        Iterator<String> iterator = list.listIterator();
        while (iterator.hasNext()){
            builder.append(iterator.next());
            if(iterator.hasNext()){
                builder.append("\n\r");
            }
        }
        return Optional.of(builder.toString());
    }
    /**
     * Load file content in the most efficient way.
     * @param path absolute file path
     * @return the content of the file or empty if it fails
     */
    public static Optional<String> fastRead2(@NotNull Path path){

        int totalSize = (int) path.toFile().length();
        StringBuilder builder = new StringBuilder(totalSize);
        try {
            FileReader reader = new FileReader(path.toFile());
            BufferedReader bufferedReader = new BufferedReader(reader);
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                builder.append(line).append("\n\r");
            }
            bufferedReader.close();
            reader.close();
        }
        catch (Exception ioe){
           return Optional.empty();
        }

        return Optional.of(builder.toString());
    }

    /**
     * Load file content in a List array where each line is an element.
     * @param path absolute file path
     * @return the content of the file or an empty List if no content available.
     */
    public static List<String> loadText(@NotNull Path path){
        List<String> file = new ArrayList<>(16);
        try {
            file =  Files.readAllLines(path, StandardCharsets.UTF_8);
        } catch (IOException ioe) {
            Logger.error("Failed to load file : " + path.toString(), ioe);
            return file;
        }
        return file;
    }

    /**
     * Write content to a file. Create the file if doesn't exist
     * @param dirPath absolute directory path
     * @param filename filename
     * @param content content of the file to write
     * @return the status of the operation - true if successful
     */
    public static boolean writeToFile(@NotNull String dirPath, @NotNull String filename, @NotNull String content){
    //    try {
            Logger.info(content);

         //   Files.write(Paths.get(dirPath+"/"+ filename), content.getBytes(StandardCharsets.UTF_8), StandardOpenOption.CREATE);

            Path path = Paths.get(dirPath+"/"+ filename);

            try (BufferedWriter writer = Files.newBufferedWriter(path, StandardCharsets.UTF_8)) {


                writer.append(content);


            } catch (IOException e) {
                e.printStackTrace();
            }

//        } catch (IOException | InvalidPathException ioe) {
//            Logger.error("Failed to write file.", ioe);
//            return false;
//        }
        return true;
    }

    /**
     * Append content to a file. Create the file if doesn't exist
     * @param dirPath absolute directory path
     * @param filename filename
     * @param content content of the file to write
     * @return the status of the operation - true if successful
     */
    public static boolean appendToFile(@NotNull String dirPath, @NotNull String filename, @NotNull String content){
        try {
            if(! Files.exists(Paths.get(dirPath+"/"+ filename))){
                Files.write(Paths.get(dirPath+"/"+ filename), content.getBytes(StandardCharsets.UTF_8), StandardOpenOption.CREATE);
            }else {
                Files.write(Paths.get(dirPath + "/" + filename), content.getBytes(StandardCharsets.UTF_8), StandardOpenOption.APPEND);
            }
        } catch (IOException | InvalidPathException ioe) {
            System.err.println("Failed to write file :"+ ioe.getLocalizedMessage());
            return false;
        }
        return true;
    }

    /**
     * Create any missing part of the directory path
     * @param dirPath absolute directory path
     *  @return the status of the operation - true if successful
     */
    public static boolean createDirectory(@NotNull String dirPath){
        try {
            Files.createDirectories(Paths.get(dirPath));
        } catch (IOException | NullPointerException ioe) {
            Logger.error("Failed to create directory.", ioe);
            return false;
        }
        return true;
    }

    /**
     * Recursive Directory Delete using NIO2 stack
     * @param dirPath absolute directory path
     *  @return the status of the operation - true if successful
     */
    public static boolean deleteDirectory(Path dirPath){
        boolean result = false;
        try {
            Files.walk(dirPath)
                    .sorted(Comparator.reverseOrder())
                    .map(Path::toFile)
                    .forEach(File::delete);
            result=true;
        } catch (IOException e) {
            Logger.error("Unable to delete directory:"+dirPath.toString(), e);
        }
        return result;
    }

    public static String getHome(){
        String home = System.getProperty("user.home");
        if(home == null || home.isEmpty()) {
            home = System.getenv("HOME");
        }
        return home;
    }
}
