package com.maxxinteractive.msettings.utils;

import com.maxxinteractive.msettings.commons.Logger;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.StringTokenizer;

import static com.maxxinteractive.msettings.Constants.*;

/**
 * Various Instrument Loader and Writer implementations
 * @author Eric Masson
 * @version 1.0
 */
public class FileSystemInstrumentHelper {

    private static final String VALUE = "value=";
    private static final String DEFAULT = "default=";

    /**
     * Load a System wide Instrument in raw text format
     * @param hash the calculated hash directories
     * @param schemaFile the schema file name
     * @return the content of the schema file or empty if it failed
     */
    public static Optional<String> loadRawInstrument(@NotNull String hash, @NotNull String schemaFile){
        return loadRaw(MSETTINGS_INSTRUMENTS_HOME, hash ,schemaFile);
    }

    /**
     * Load an User Preference Instrument in raw text format
     * @param hash the calculated hash directories
     * @param schemaFile the schema file name
     * @return the content of the schema file
     */
    public static Optional<String> loadRawUserPreference(@NotNull String hash, @NotNull String schemaFile){
        return loadRaw(MSETTINGS_USER_PREFERENCES_HOME, hash ,schemaFile);
    }


    private static Optional<String> loadRaw(@NotNull String home, @NotNull String hash, @NotNull String schemaFile){
        Path path;
        try {
            path = buildPath(home , hash ,schemaFile);
        }catch (InvalidPathException e){
            Logger.error(e);
            return Optional.empty();
        }
        return FileSystemHelper.fastRead(path);
    }

    /**
     * Convenient path builder
     * @param home MaXX Settings HOME - System Wide or User Pref
     * @param hash of the Instrument
     * @param schemaFile name
     * @return a Path
     * @throws InvalidPathException if path not valid
     */
    public static Path buildPath(@NotNull String home, @NotNull String hash, @NotNull String schemaFile)
            throws InvalidPathException{
        return Paths.get(home + hash + "/" + schemaFile);
    }

    /**
     * Load a System wide Instrument in a List of KeyValuePairs
     * @param hash of the Instrument
     * @param schemaFile to load
     * @return List of KeyValuePair
     */
    public static Optional<List<KeyValuePair>> loadInstrument(@NotNull String hash, @NotNull String schemaFile){
        return loadSchema(MSETTINGS_INSTRUMENTS_HOME, hash, schemaFile);
    }

    public static Optional<KeyValuePair> loadUserPreference(@NotNull String hash, @NotNull String schemaFile){
        Optional<List<KeyValuePair>> list = loadSchema(MSETTINGS_USER_PREFERENCES_HOME, hash, schemaFile);
        if(!list.isPresent() || list.get().isEmpty()){
            return Optional.empty();
        }
        // return first element
        return Optional.of(list.get().get(0));
    }

    public static Optional<List<KeyValuePair>> loadSchema(@NotNull String home, @NotNull String hash, @NotNull String schemaFile) {
        Path path;
        try {
            path = buildPath(home, hash, schemaFile);
        } catch (InvalidPathException e) {
            Logger.error("Failed to load the : " + schemaFile, e);
            return Optional.empty();
        }
        return loadSchema(path);
    }

    public static Optional<List<KeyValuePair>> loadSchema(@NotNull Path path){
        // load file
        List<String> content = FileSystemHelper.loadText(path);
        // generate List
        List<KeyValuePair> keyValuePairs = KeyValuePairUtils.processKeyValuePairs(content);
        return Optional.of(keyValuePairs);
    }

    /**
     * Write a System wide Instrument
     * @param hash the calculated hash directories
     * @param schemaFile the schema file name
     * @param content content of the schema file to write
     * @return the status of the operation - true if successful
     */
    public static boolean writeInstrument(@NotNull String hash, @NotNull String schemaFile, @NotNull String content){
        if (!FileSystemHelper.createDirectory(MSETTINGS_INSTRUMENTS_HOME+hash)){
            return false;
        }
        return FileSystemHelper.writeToFile(MSETTINGS_INSTRUMENTS_HOME+hash,schemaFile, content);
    }


    /**
     * Write an User Preference Instrument
     * @param hash the calculated hash directories
     * @param schemaFile the schema file name
     * @param value key=value pair representing the User Preference value attribute to write
     * @return the status of the operation - true if successful
     */
    public static boolean writeUserPreference(@NotNull String hash, @NotNull String schemaFile, @NotNull String value){
        String output = VALUE+value;
        boolean result = FileSystemHelper.createDirectory(MSETTINGS_USER_PREFERENCES_HOME+hash);
        if (!result){
            return false;
        }
        result = FileSystemHelper.writeToFile(MSETTINGS_USER_PREFERENCES_HOME+hash,schemaFile, output);
        return result;
    }

    public static Optional<String> getDefaultValue(@NotNull String hash, @NotNull String schemaFile){
        Optional<String> output = loadRawInstrument(hash,schemaFile);
        if(!output.isPresent()){
            return Optional.empty();
        }
        return getAttributeValueFromRawFile(output.get(),ATTRIBUTE_DEFAULT);
    }

    /**
     * Retreive attribute value from a raw Schema file
     * @param content raw multiline text file
     * @param attributeName to find
     * @return result
     */
    public static Optional<String> getAttributeValueFromRawFile(@NotNull String content, @NotNull String attributeName){
        StringTokenizer stk = new StringTokenizer(content,"\n\r");
        while(stk.hasMoreElements()){
            String line = stk.nextToken();

            if(line.startsWith(attributeName)){
                Optional<KeyValuePair> keyValuePair = KeyValuePair.buildForLine(line);
                if(keyValuePair.isPresent()) {
                    return Optional.of(keyValuePair.get().getValue());
                }
                break;
            }
        }
        return Optional.empty();
    }


    public static String getInstrumentAbsolutePath(@NotNull String hash, @NotNull String schemaFile){
        return MSETTINGS_INSTRUMENTS_HOME+hash+ File.separatorChar+ schemaFile;
    }

    public static String getUserPreferenceAbsolutePath(@NotNull String hash, @NotNull String schemaFile){
        return MSETTINGS_USER_PREFERENCES_HOME+hash+ File.separatorChar+ schemaFile;
    }
}