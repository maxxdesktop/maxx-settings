package com.maxxinteractive.msettings.utils;

import org.jetbrains.annotations.NotNull;

import java.io.File;

public class HasherUtility {

    /**
     * Generate a four level hashed directory structure based on a hash value
     * @param value - the key to hash
     * @return hashed directory
     */
    public static String generateDirectoryHash(@NotNull String value){
        return generateDirectoryHash(calculateHashCode(value));
    }

    /**
     * Generate a four level hashed directory structure based on a hash value
     * @param hash value
     * @return hashed directory
     */
    public static String generateDirectoryHash(int hash){
        int mask = 255;
        int firstDir = hash & mask;
        int secondDir = (hash >> 8) & mask;
        int thirdDir = (hash >> 8 >> 8) & mask;
        int forthDir = (hash >> 8 >> 8 >> 8) & mask;
        java.lang.String format = ".%02x.%02x.%02x.%02x";
        format = format.replace(".", File.separator);
        return  String.format(format, firstDir, secondDir, thirdDir, forthDir);
    }

    /**
     * Generate Hash code from a String. Current implementation use String.hashCode() and
     * there is an equivalent in MaXX Vue C++ Framework. Both implementation are compatible.
     * @param str string of char
     * @return the hash code
     */
    public static int calculateHashCode(String str){
        return str.hashCode();
    }
}
