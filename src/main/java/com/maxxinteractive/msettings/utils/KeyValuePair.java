package com.maxxinteractive.msettings.utils;

import com.maxxinteractive.msettings.commons.Logger;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.Objects;
import java.util.Optional;

public class KeyValuePair implements Serializable {

    private String key;
    private String value;

    public KeyValuePair(String input){
        KeyValuePair kpv = parse(input);
        if(kpv != null){
            key = kpv.getKey();
            value = kpv.getValue();
        }
    }
    public KeyValuePair(@NotNull String key, @NotNull String value) {
        this.key = key;
        this.value = value;
    }

    private static KeyValuePair parse(String line){
        int index = line.indexOf("=");
        if(index == -1){
            Logger.setSilentMode(false);
            Logger.error(new Exception("Failed parsing line '" + line
                    + "'. Format is invalid." ));
            return null;
        }
        String key = line.substring(0,index);
        String value = line.substring(index+1);
        return new KeyValuePair(key,value);
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Build an Instance of Pair from a '=' separated key value pair String
      * @param line a '=' separated key=value pair String
     * @return Pair object
     */
    public static Optional<KeyValuePair> buildForLine(@NotNull String line){
        int index = line.indexOf("=");
        if(index == -1){
            Logger.error(new Exception("Failed parsing the line '" + line
                    + "'. Format is invalid." ));
            return Optional.empty();
        }
        String key = line.substring(0,index);
        String value = line.substring(index+1);
        return Optional.of(new KeyValuePair(key,value));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        KeyValuePair keyValuePair = (KeyValuePair) o;
        return Objects.equals(key, keyValuePair.key) && Objects.equals(value, keyValuePair.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key, value);
    }

    @Override
    public String toString() {
        return "Pair{" +
                "key='" + key + '\'' +
                ", value='" + value + '\'' +
                '}';
    }

    public String serialize(){
        return key + "=" + value ;
    }
}
