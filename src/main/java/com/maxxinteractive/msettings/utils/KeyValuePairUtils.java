package com.maxxinteractive.msettings.utils;

import com.maxxinteractive.msettings.commons.Logger;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.StringTokenizer;

public class KeyValuePairUtils {

    /**
     * Build List of KVP from a multiline String such an Instrument Schema file
     * @param args the content
     * @return List of KVP or an empty list
     */
    public static List<KeyValuePair> processKeyValuePairs(@NotNull List<String> args){
        // process List of string, and add new Pair to current List<Pair> pairs
        List<KeyValuePair> keyValuePairs = new ArrayList<>(10);
        for(String line : args){
            line = line.trim();
            if (line.isEmpty()) {
                continue;
            }
            // remove possible double //
            line = line.replace("//", "/");
            Optional<KeyValuePair> keyValuePair = KeyValuePair.buildForLine(line);
            if(keyValuePair.isPresent()) {
                keyValuePairs.add(keyValuePair.get());
            }
        }
        return keyValuePairs;
    }

    /**
     * Extract Key Value Pair from comma separated String.  ex: foo=bar,hello=world
     * @param arg comma separated String
     * @return List of KVP
     */
    public static List<KeyValuePair> processKeyValuePairs(@NotNull String arg){
        // process comma separated string, and add new Pair to current List<Pair> pairs
        StringTokenizer stk = new StringTokenizer(arg.trim(),",");
        List<KeyValuePair> keyValuePairs = new ArrayList<>(16);
        while(stk.hasMoreElements()){
            Optional<KeyValuePair> keyValuePair = KeyValuePair.buildForLine( stk.nextToken());
            if(keyValuePair.isPresent()) {
                keyValuePairs.add(keyValuePair.get());
            }
        }
        return keyValuePairs;
    }


    public static Optional<String> extractKey(String line){
        int index = line.indexOf("=");
        if(index > -1){
            return  Optional.of(line.substring(0,index));
        }
        Logger.warning("Bad key/value format.");
        return Optional.empty();
    }

    public static Optional<String> extractValue(String line){
        int index = line.indexOf("=");
        if(index > -1){
            return   Optional.of(line.substring(index+1));
        }
        Logger.warning("Bad key/value format.");
        return Optional.empty();
    }

}
